import matplotlib.pyplot as plt

p0 = np.array([2.47, 1.71])

# sensitivites
dydp0 = OO._eval_SM(p0)
dydpmin = OO._eval_SM(RR["X_inner"][-1])
for i in range(len(p0)):
    plt.plot(np.abs(dydp0[:,i]), label="p0_{}".format(i+1))
    plt.plot(np.abs(dydpmin[:, i]), label="pmin_{}".format(i + 1))
plt.legend()
plt.title("Parameter sensitivities (magnitudes)")
plt.xlabel("bins")
plt.savefig("parsens_mag.pdf")

# relative sensitivities
rel_sens_0 = dydpmin/dydp0
for i in range(len(p0)):
    plt.plot(np.abs(rel_sens_0[:,i]), label="p_{}".format(i+1))
plt.legend()
plt.title("Relative parameter sensitivities (magnitudes)")
plt.xlabel("bins")
plt.hlines(1.,0,rel_sens_0.shape[0]-1)
ax = plt.gca()
ax.set_yscale("log")
plt.savefig("parsens_rel.pdf")



# plot RA
from mpl_toolkits import mplot3d
i_bin = np.argmax(np.abs(rel_sens_0[:,1]))
R = OO._RA[i_bin]
B = R._scaler.box
x = np.linspace(B[0,0],B[0,1],30)
y = np.linspace(B[1,0],B[1,1],30)
X, Y = np.meshgrid(x, y)
Z = np.zeros(X.shape)
for (i,x,y) in zip(range(X.size),np.nditer(X),np.nditer(Y)):
    Z[np.unravel_index(i, Z.shape)] = R([x,y])
fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot_surface(X, Y, Z, rstride=1, cstride=1,
                cmap='viridis', edgecolor='none')

plt.title("Rational approximation for bin #{}".format(i_bin))
plt.savefig("RA_bin#{}.pdf".format(i_bin))

# Let"s have a look at the bin-wise contributions
wmin = RR["X_outer"][-1]
#OO.setWeights(wmin)
pmin = RR["X_inner"][-1]
OO._WD_b(1, pmin, Ey=Ey[1])

WFIMmin = OO._WFIM(pmin)
WDmin = OO._WD(pmin, Ey=Ey)
print(WFIMmin - WDmin)
print(RR["eFIMm"][-1]) # should be equal

FIMs_b_min = [OO._FIM_b(i, pmin) for i in range(len(OO._binids))]
WFIMs_b_min = [OO._WFIM_b(i, pmin) for i in range(len(OO._binids))]

Ds_b = [OO._D_b(i, pmin, Ey=Ey[i]) for i in range(len(OO._binids))]
WDs_b = [OO._WD_b(i, pmin, Ey=Ey[i]) for i in range(len(OO._binids))]

FIMS_b_min_trace = np.array([np.trace(x) for x in FIMs_b_min])
Ds_b_min_trace = np.array([np.trace(x) for x in Ds_b])

T = FIMS_b_min_trace  - Ds_b_min_trace




p0 = np.array([2.47,1.71])
lbins = [len(h) for h in OO._wdict.values()]
n_b = sum(lbins)
Ey = np.zeros(n_b)
for i in range(n_b):
    if i > sum(lbins[0:3])-1:
        Ey[i] = OO._RA_sym[i].eval_f(*p0) + 0.48394232652795577
    else:
        Ey[i] = OO._RA_sym[i].eval_f(*p0)

wmin = RR["X_outer"][-1]
OO.setWeights(wmin)
pmin = RR["X_inner"][-1]

print(OO._oo_eval_direct(pmin, Ey=Ey))  # that is supposed to be the maximum
print(RR["Y_outer"][-1])

# deviation term at minimum
D = OO._WD(pmin, Ey=Ey)
# FIM at miminum
FIM = OO._WFIM(pmin)

lbd = 0.1
eFIM = FIM - lbd*D
print(eFIM)
print(RR["eFIMm"][-1]) # the two lines hould be equal


# supposed optimum
OO.setWeights([1.,1.,1.,0.])
D = OO._WD(p0,Ey=Ey)
print(D) # should be zero matrix
FIM = OO._WFIM(p0)

lbd = 0.1
eFIM = FIM - lbd*D
print(eFIM)
print(RR["eFIMm"][-1]) # the two lines hould be equal
