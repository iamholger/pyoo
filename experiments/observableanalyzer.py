import os,sys
import numpy as np
def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            y_win = _y
        elif _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"][i_win]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]


    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def printandplotstats(obsname,IO,IOCE,outdir):
    obsbins = IO.obsBins(obsname)
    obsname_ = obsname.replace('/', '_')
    coeffstats = {}
    simystats = {}
    fnvalstats = {}
    minfnval = {}
    maxfnval = {}

    datavariance = {}
    modelerrorvariance = {}
    Rb = {}
    IO.setAppStructures()
    for b in obsbins:
        bin = IO._binids[b]
        print("\n" + bin)
        bin_ = bin.replace('/', '_')
        X, Y = tools.readData(os.path.join(simdatadir, bin_))
        simystats[bin] = {
            'min': np.min(Y),
            'max': np.max(Y),
            'mean': np.average(Y),
            'median': np.median(Y),
            'sd': np.std(Y)
        }
        print("simulation values stats", simystats[bin])

        fnY = []
        for x in X:
            fnY.append(IO.calc_f_val(x, [b]))
        fnvalstats[bin] = {
            'min': np.min(fnY),
            'max': np.max(fnY),
            'mean': np.average(fnY),
            'median': np.median(fnY),
            'sd': np.std(fnY)
        }

        minfnval[bin] = apprentice.tools.extreme(IO._RA[b],100,100,True,'min')
        maxfnval[bin] = apprentice.tools.extreme(IO._RA[b],100,100,True,'max')

        Rb[bin] = IO._Y[b]
        datavariance[bin] = IO._E[b]

        found = False
        for b2 in IOCE.obsBins(obsname):
            if IOCE._binids[b2] == bin:
                found = True
                modelerrorvariance[bin] = IOCE._E[b]
            if not found:
                modelerrorvariance[bin] = 0.

        print("R_b", Rb[bin])
        print("\sigma_b (Data error)", datavariance[bin])
        print("\sigma_b (Model error)", modelerrorvariance[bin])

        pcoeff = IO._PC[b]
        coeffstats[bin] = {
            'min': np.min(pcoeff),
            'max': np.max(pcoeff),
            'mean': np.average(pcoeff),
            'median': np.median(pcoeff),
            'sd': np.std(pcoeff)
        }

        print('coeffecient values stats', coeffstats[bin])

    weights = {}
    origweights = {}
    for key in wtwt:
        wtfile = wtwt[key]
        hnames = [b.split("#")[0] for b in IO._binids]
        bnums = [int(b.split("#")[1]) for b in IO._binids]
        warr = np.array(IO.initWeights(wtfile, hnames, bnums))
        warrscaled = (warr - min(warr)) / (max(warr) - min(warr))
        weights[key] = warrscaled[obsbins[0]]
        origweights[key] = warr[obsbins[0]]

    for key in wtjson:
        wtfile = wtjson[key]
        data = readReport(wtfile)
        warr = np.array(data['wmin'])
        warrscaled = (warr - min(warr)) / (max(warr) - min(warr))
        index = data['hnames'].index(obsname)
        weights[key] = -1 if index == -1 else warrscaled[index]
        origweights[key] = -1 if index == -1 else warr[index]

    print("\nWeights (scaled to 0-1) for observable {}".format(obsname))
    for key in weights:
        print(key, weights[key])

    print("\nweights/\sigma_b")

    for bin in np.array(IO._binids)[IO.obsBins(obsname)]:
        print("\n" + bin)
        for key in weights:
            print(key, "%.2e" % (weights[key] / datavariance[bin]) ** 2,
                  "%.2e" % (weights[key] / modelerrorvariance[bin] ** 2))

    import matplotlib.pyplot as plt
    index1 = [i for i in range(0, 6 * len(simystats), 6)]
    index2 = [i for i in range(1, 6 * len(simystats) + 1, 6)]
    index3 = [i for i in range(2, 6 * len(simystats) + 2, 6)]
    index4 = [i for i in range(3, 6 * len(simystats) + 3, 6)]
    index5 = [i for i in range(4, 6 * len(simystats) + 4, 6)]
    indexxtick = [i + 2.5 for i in range(0, 6 * len(simystats), 6)]
    xticklabel = ['$b_{%d}$' % (i) for i in range(len(simystats))]
    plt.figure(figsize=(16, 14))
    plt.errorbar(index1,
                 [simystats[key]['mean'] for key in simystats],
                 [[0.] * len(simystats), [simystats[key]['sd'] for key in simystats]],
                 fmt='ob', lw=3, label="Simulation values")

    plt.errorbar(index2,
                 [fnvalstats[key]['mean'] for key in fnvalstats],
                 [[0.] * len(fnvalstats), [fnvalstats[key]['sd'] for key in fnvalstats]],
                 fmt='or', lw=3, label="Function values")

    plt.errorbar(index1,
                 [simystats[key]['mean'] for key in simystats],
                 [np.array([simystats[key]['mean'] for key in simystats])
                  - np.array([simystats[key]['min'] for key in simystats]),
                  np.array([simystats[key]['max'] for key in simystats])
                  - np.array([simystats[key]['mean'] for key in simystats])
                  ],
                 fmt='.b', ecolor='gray', lw=1)

    plt.errorbar(index2,
                 [fnvalstats[key]['mean'] for key in fnvalstats],
                 [np.array([fnvalstats[key]['mean'] for key in fnvalstats])
                  - np.array([fnvalstats[key]['min'] for key in fnvalstats]),
                  np.array([fnvalstats[key]['max'] for key in fnvalstats])
                  - np.array([fnvalstats[key]['mean'] for key in fnvalstats])
                  ],
                 fmt='.r', ecolor='gray', lw=1)

    plt.errorbar(index3,
                 [Rb[key] for key in Rb],
                 [[0.] * len(Rb), [modelerrorvariance[key] for key in simystats]],
                 fmt='om', lw=3, label="R_b & model SD")

    plt.errorbar(index4,
                 [Rb[key] for key in Rb],
                 [[0.] * len(Rb), [datavariance[key] for key in simystats]],
                 fmt='og', lw=3, label="R_b & data SD")

    minfnvalarr = np.array([minfnval[key] for key in minfnval])
    maxfnvalarr = np.array([maxfnval[key] for key in maxfnval])

    fnvalmiddle = (minfnvalarr+ maxfnvalarr)/2
    plt.errorbar(index5,
                 fnvalmiddle,
                 [fnvalmiddle-minfnvalarr,maxfnvalarr-fnvalmiddle],
                 fmt='.k', lw=3, label="Envelope")
    for k in maxfnval:
        print(k,minfnval[k],Rb[k],maxfnval[k])
    plt.xticks(indexxtick, labels=xticklabel,fontsize = 20)
    plt.yticks(fontsize=20)
    # np.array([simystats[key]['max'] for key in simystats])
    # np.amax()
    plt.yscale('symlog', linthreshy=0.01)
    plt.xlabel("Bins",fontsize=24)
    # plt.
    # plt.ylim(10**-50)
    # plt.tight_layout()
    plt.legend(fontsize=20)
    plt.title(obsname,fontsize=24)
    # plt.show()
    plt.savefig(os.path.join(outdir,"plot"+obsname_+".pdf"))
    plt.close('all')
    # mins = simystats[key]
    # import matplotlib.pyplot as plt

    # x = np.random.randn(100, 4)
    # mins = x.min(0)
    # maxes = x.max(0)
    # means = x.mean(0)
    # std = x.std(0)
    #
    # # create stacked errorbars:
    # plt.errorbar([0,3,6,9], means, std, fmt='ok', lw=3,label="SIMY")
    # plt.errorbar([1, 4, 7, 10], means, std, fmt='or', lw=3,label="Coeff")
    # plt.errorbar([0,3,6,9], means, [means - mins, maxes - means],
    #              fmt='.k', ecolor='gray', lw=1)
    # plt.errorbar([1, 4, 7, 10], means, [means - mins, maxes - means],
    #              fmt='.r', ecolor='gray', lw=1)
    # plt.xlim(-1, 8)
    # plt.xticks([0.5,3.5,6.5,9.5],labels=["bin0","bin1","bin2","bin3"])
    # plt.xlim(-1,11)
    # plt.legend()
    #
    # plt.show()

    print("\n Data error variance\n")
    tab = "Wts/Bins"
    for key in weights:
        tab +=" & $\\frac{w_{(%s)}}{\sigma_b^2}$"%(key)
        # tab += " & w/$\sigma_b$(DE) & w/$\sigma_b$(ME)"
    tab+="\\\\\\hline \n"
    tab += "w (0-1) "
    for key in weights:
        tab += "& %.2e" % (weights[key])
    tab += "\\\\\\hline\\hline \n\n"
    for bno,bin in enumerate(np.array(IO._binids)[IO.obsBins(obsname)]):
        tab += "$b_{%d}$ " % (bno)
        for key in weights:
            tab+=" & %.2e " % ((origweights[key] / datavariance[bin]) ** 2)
            # tab += " & %.2e " % ((weights[key] / modelerrorvariance[bin]) ** 2)
        tab+="\\\\\\hline \n"
    print(tab)

    print("\n Model error variance\n")
    tab = "Wts/Bins"
    for key in weights:
        tab +=" & $\\frac{w_{(%s)}}{\sigma_b^2}$"%(key)
        # tab += " & w/$\sigma_b$(DE) & w/$\sigma_b$(ME)"
    tab += "\\\\\\hline \n"
    tab+="w (0-1) "
    for key in weights:
        tab +="& %.2e"%(weights[key])
    tab += "\\\\\\hline\\hline \n\n"

    for bno, bin in enumerate(np.array(IO._binids)[IO.obsBins(obsname)]):
        tab += "$b_{%d}$ " % (bno)
        for key in weights:
            # tab += " & %.2e " % ((weights[key] / datavariance[bin]) ** 2)
            tab += " & %.2e " % ((origweights[key] / modelerrorvariance[bin]) ** 2)
        tab += "\\\\\\hline \n"
    print(tab)
            # print(key, "%.2e" % (weights[key] / datavariance[bin]) ** 2,
            #       "%.2e" % (weights[key] / modelerrorvariance[bin] ** 2))


if __name__ == "__main__":
    import argparse,apprentice

    parser = argparse.ArgumentParser(description='',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir where approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored")
    parser.add_argument('-o' "--outdir", dest="OUTDIR", type=str, default=None,
                        help="Out dir")
    parser.add_argument("--obsname", dest="OBSNAME", type=str, default=None,
                        help="Observable name")



    args = parser.parse_args()
    os.makedirs(args.OUTDIR, exist_ok=True)


    indir =  args.INDIR
    dataname = os.path.basename(indir)

    simdatadir = "../../log/SimulationData/{}".format(dataname)

    wtwt = {"nnpdf" : "../../log/orig/A14/weights_handtune"}

    wtjson={
        "meanscore" : "../../log/juliwenjing/{}/meanscore.json".format(dataname),
        "medscore" : "../../log/juliwenjing/{}/medscore.json".format(dataname),
        "portfolio": "../../log/juliwenjing/{}/portfolio_1.json".format(dataname),
        "robopt61.8": "../../log/robO/{}/new/out_mu61.8_ms0_lNone.json".format(dataname),
        "bayesian5e-2(4)": "../../log/bayesian/{}/ce/l1/out/out_dbaseWFIM_with_grad_dmA_lambda5.00e-02_iter3.json".format(dataname)
    }

    approxfile = args.INDIR + "/approximation.json"
    expdatafile = args.INDIR + "/experimental_data.json"
    weightfile = args.INDIR + "/weights"
    expdatafilece = args.INDIR + "/experimental_data_cenoise.json"
    from apprentice import tools
    IO = tools.TuningObjective(weightfile, expdatafile, approxfile, filter_hypothesis=False, filter_envelope=False)
    IOCE = tools.TuningObjective(weightfile, expdatafilece, approxfile, filter_hypothesis=False, filter_envelope=False)

    if args.OBSNAME is not None:
        printandplotstats(obsname=args.OBSNAME, IO=IO, IOCE=IOCE, outdir=args.OUTDIR)
    else:
        for hname in IO._hnames:
            printandplotstats(obsname=hname,IO=IO,IOCE=IOCE,outdir=args.OUTDIR)





































