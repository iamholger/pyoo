import numpy as np
import apprentice
import os, sys

def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json

    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def is_pareto_efficient(costs):
    """
    Find the pareto-efficient points
    :param costs: An (n_points, n_costs) array
    :return: A (n_points, ) boolean array, indicating whether each point is Pareto efficient
    """
    is_efficient = np.ones(costs.shape[0], dtype = bool)
    for i, c in enumerate(costs):
        is_efficient[i] = np.all(np.any(costs[:i]>c, axis=1)) and np.all(np.any(costs[i+1:]>c, axis=1))
    return is_efficient

def findCornerTriangulation(pareto, data, txt,logx,logy):
    def angle(B, A, C):
        """
        Find angle at A
        On the L, B is north of A and C is east of A
        B
        | \
        A - C
        """

        ab = [A[0]-B[0], A[1]-B[1]]
        ac = [A[0]-C[0], A[1]-C[1]]

        l1=np.linalg.norm(ac)
        l2=np.linalg.norm(ab)
        import math
        return math.acos(np.dot(ab,ac)/l1/l2)


    def area(B, A, C):
        """
        A,B,C ---
        On the L, B is north of A and C is east of A
        B
        | \     (area is +ve)
        A - C
        """
        # return 0.5 *( ( C[0] - A[0] )*(A[1] - B[1]) -  (A[0] - B[0])*(C[1] - A[1]) )
        return 0.5 *( (B[0]*A[1] - A[0]*B[1]) - (B[0]*C[1]-C[0]*B[1]) + (A[0]*C[1]-C[0]*A[1]))

    cte=np.cos(7./8*np.pi)
    cosmax=-2
    corner=len(pareto)-1
    # print(angle([-0.625, 1.9],[0,0],[4,0]))
    # exit(1)


    if(logx):
        xp = np.log10(pareto[:,0])
        xd = np.log10(data[:,0])
    else:
        xp = (pareto[:,0])
        xd = (data[:,0])
    if(logy):
        yp = np.log10(pareto[:,1])
        yd = np.log10(data[:,1])
    else:
        yp = (pareto[:,1])
        yd = (data[:,1])
    lpareto = np.column_stack((xp,yp))
    ldata = np.column_stack((xd,yd))

    C = lpareto[corner]
    for k in range(0,len(lpareto)-2):
        B = lpareto[k]
        for j in range(k,len(lpareto)-2):
            A = lpareto[j+1]
            _area = area(B,A,C)
            _angle = angle(B,A,C)
            # degB,pdegB,qdegB = getdegreestr(B,ldata,txt)
            # degA,pdegA,qdegA = getdegreestr(A,ldata,txt)
            # degC,pdegC,qdegC = getdegreestr(C,ldata,txt)
            # print("print all %s %s %s %f %f %f"%(degB,degA,degC, _area,_angle,_angle*(180/np.pi)))

            # if(_area > 0):
            #     degB,pdegB,qdegB = getdegreestr(B,ldata,txt)
            #     degA,pdegA,qdegA = getdegreestr(A,ldata,txt)
            #     degC,pdegC,qdegC = getdegreestr(C,ldata,txt)
            #     print("area > 0 ------>area = %.4f angls = %.4f at %s %s %s"%(_area,_angle, degB, degA, degC))


            if _angle > cte and _angle > cosmax and _area > 0:
                corner = j + 1
                cosmax = _angle
                # degB,pdegB,qdegB = getdegreestr(B,ldata,txt)
                # degA,pdegA,qdegA = getdegreestr(A,ldata,txt)
                # degC,pdegC,qdegC = getdegreestr(C,ldata,txt)
                # print("===== Found corner ====== %f %f at %s %s %s"%(_area,_angle, degB, degA, degC))
    print("In findCornerTriangulation, I got this {}".format(pareto[corner]))
    return corner

def mkPlot(data, f_out,orders=None,lx="$x$", ly="$y$", logy=True, logx=True, normalize_data=True,jsdump={},title=""):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    plt.clf()
    mpl.rc('text', usetex = False)
    mpl.rc('font', family = 'serif', size=12)
    mpl.style.use("ggplot")

    plt.figure(figsize=(8, 8))
    plt.xlabel(lx)
    plt.ylabel(ly)
    if logx: plt.xscale("log")
    if logy: plt.yscale("log")

    data=np.array(data)
    # if normalize_data: data = data / data.max(axis=0)

    b_pareto = is_pareto_efficient(data)
    _pareto = data[b_pareto] # Pareto points

    pareto = _pareto[_pareto[:,0].argsort()]# Pareto points, sorted in x
    print("===========")
    print(pareto)
    print("===========")

    c, txt   = [], []
    for num, m in enumerate(orders):
        if b_pareto[num]:
            c.append("g")
        else:
            c.append("r")

        if b_pareto[num]:
            txt.append("({})".format(m))
        else:
            txt.append("")

    cornerT = findCornerTriangulation(pareto,data,txt,logx,logy)
    # cornerT2 = findCornerTriangulation2(pareto)
    # cornerdSl = findCornerSlopesRatios(pareto)

    plt.scatter(pareto[:,0]  , pareto[:,1]  , marker = 'o', c = "silver"  ,s=100, alpha = 1.0)
    # plt.scatter(pareto[cornerdSl,0]  , pareto[cornerdSl,1]  , marker = '+', c = "peru"  ,s=777, alpha = 1.0)
    # plt.scatter(pareto[cornerT2,0]  , pareto[cornerT2,1]  , marker = 'x', c = "cyan"  ,s=444, alpha = 1.0)

    # TODO: Commented out for now
    # plt.scatter(pareto[cornerT,0]  , pareto[cornerT,1]  , marker = '*', c = "gold"  ,s=444, alpha = 1.0)

    print("Plotting")

    for num, d in enumerate(data): plt.scatter(d[0], d[1],c=c[num])
    for num, t in enumerate(txt): plt.annotate(t, (data[num][0], data[num][1]))
    plt.title(title,fontsize=8)
    plt.savefig(f_out)
    plt.tight_layout()
    plt.close('all')

    return jsdump

def dolcurve(args):
    approxfile = os.path.join(args.INDIR ,"approximation.json")
    expdatafile = os.path.join(args.INDIR, "experimental_data.json")
    weightfile = os.path.join(args.INDIR, "weights")

    from apprentice.tools import TuningObjective
    import re
    IO = TuningObjective(weightfile, expdatafile, approxfile,
                         restart_filter=100, debug=False)
    IO._nstart = 100
    IO._nrestart = 20
    residualN = []
    solutionN = []
    warr = []
    larr = []
    hnames = []
    for r in args.FILES:
        outfile = os.path.join(args.RESDIR,r)
        data = readReport(outfile)
        w = data['wmin'][0]
        hnames = data['hnames']
        warr.append(repr(["%s = %.1E" % (hnames[i],wi) for i,wi in enumerate(w)]))
        larr.append(float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME), r).group(1)))
        p = data['x']
        IO.setWeights([1.] * len(w))
        # IO.setWeights(w)
        chi2 = IO.objective(p)
        residualN.append(chi2)
        solutionN.append(sum(data['wmin'][0]))

    import pylab
    params = {
        'axes.titlesize': 6,
    }
    pylab.rcParams.update(params)
    wstr = ""
    for i, l, w in zip(range(len(args.FILES)), larr, warr):
        wstr += str(l) + ": " + w + "\n"
        # if (i in [1,3,5,7,9,11,13,15]): wstr += "\n"
    # pylab.title(wstr)
    print(residualN, solutionN)
    pylab.scatter(solutionN, residualN, color='k', linewidth=0.5, linestyle="-")
    if not os.path.exists(args.OUTDIR):
        os.makedirs(args.OUTDIR, exist_ok=True)
    outfilepareton = os.path.join(args.OUTDIR, "lcurve.pdf")
    # for r,s,mu in zip(residualN,solutionN, muoptarr):
    #     pylab.text(s,r,mu)
    xlab = "$e^Tw$"
    ylab = "$\\sum_{\\mathcal{O} \\in \\theta}  \\sum_{b \\in \\mathcal{O}} \\frac{\\left(r_b({p}) - \\mathcal{R}_b \\right)^2}{\\sigma_b^2}$"

    mkPlot([(a, b) for a, b in zip(solutionN, residualN)],
           outfilepareton, larr, ly=ylab, lx=xlab, title = "",
           logy=True, logx=False, normalize_data=False)
    print(wstr)
    pylab.tight_layout()

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Plot L curve',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir")
    parser.add_argument("-r", "--resultdir", dest="RESDIR", type=str, default=None,
                        help="Result Dir")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix")
    parser.add_argument("-t", "--resultfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Filenames of runs for different values of lambda")
    parser.add_argument("-l", "--regparamname", dest="REGPARAMNAME", type=str, default='',
                        help="Regularization param name to find in filenames and print on plot")


    args = parser.parse_args()

    print(args)

    dolcurve(args)




