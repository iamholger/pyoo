import numpy as np
import pyDOE


class SurrogateOptimiser(object):
    def __init__(self, dim, **kwargs):
        self.dim = dim
        # Parse kwargs and set default arguments
        self.phifunction  = kwargs.get("phifunction", "cubic" )
        self.debug      = True                       if "debug"        in kwargs.keys() else False
        self.printprogress = True                    if "progress"     in kwargs.keys() else False
        self.polynomial   = kwargs.get("polynomial" , "linear")
        self.maxevals     = kwargs.get("maxevals"   ,  100) # aka budget
        self.sigma_stdev  = kwargs.get("sigma_stdev",  0.2)
        self.n0           = kwargs.get("n0"         ,  50)
        if self.n0 < self.dim +1:
            self.n0 = self.dim+1
            print("Warning, design size too small, adjusting to %i"%self.n0)
        self.ncands       = kwargs.get("ncands"     ,  500)
        self.pertP        = kwargs.get("pertP"      ,  1)
        self.tolerance    = kwargs.get("tolerance"      , 0.001 * np.linalg.norm(np.ones((1, self.dim))))
        self.riskAv        = kwargs.get("riskAv"      ,  5)
        self.cycling = kwargs.get("cycling", "steps")
        self.objective = kwargs.get("objective")
        self.useObj = kwargs.get("useobjective", "trace")
        self.kwargs = kwargs

        self._xsel = []
        self._Fsel = []
        self._xbest = []
        self._Fbest = []

        self._xtrain = []
        self._ytrain = []
        self._reports = []

        self._xall = []
        self._yall = []

    def storeTrainingData(self, fname):
        """
        Dump the RBF trainig data into text file.
        Last column is the objective. All others are the
        training points.
        """
        X=np.array(self._xtrain)
        Y=self._ytrain
        T=np.zeros((X.shape[0], X.shape[1]+1))
        T[:,0:X.shape[1]]=X
        T[:,-1]=Y
        np.savetxt(fname, T)


    def mkRBF(self, fromFile=False):
        """
        Calculate an RBF. By default sampling and objective evaluation
        are done on the fly. If fromFile is given, the training
        data will be read from that file.
        """
        if fromFile:
            print("Reading training date from file '%s'"%fromFile)
            import os
            if not os.path.exists(fromFile): raise Exception("Sepcified file for RBF load '%s' does not exist!"%fromFile)
            try:
                T = np.loadtxt(fromFile)
            except Exception as e:
                raise Exception("There was a problem reading the file '%s' as numpy array"%fromFile)

            _S = T[:,0:-1]
            _Y = T[:,-1].reshape((_S.shape[0],1)) # Explicit cast into (N, 1) shape
            # For consistency fill the training data here
            self._xtrain=[x for x in _S]
            self._ytrain=[y for y in _Y.ravel()]

        else:
            rank_P = -1
            while rank_P != self.dim:
                #generate initial experimental design
                # S =       lhs(data.dim+1, samples = data.n0, criterion ="maximin") #might need to update this to get spacefilling projections
                # NOTE: not sure about +1
                S = pyDOE.lhs(self.dim, samples = self.n0, criterion ="maximin") #might need to update this to get spacefilling projections
                # NEED one more column than dimensions
                Ss=S/np.asmatrix(np.sum(S,axis = 1)).T #get all points to sum to 1
                # data.S = Ss[:, 0:data.dim]
                _S = Ss[:, 0:self.dim-1]
                P = np.concatenate((np.ones((self.n0, 1)), _S), axis = 1)
                rank_P = np.linalg.matrix_rank(P)
            # S = pyDOE.lhs(self.dim, samples = self.n0, criterion ="maximin") #might need to update this to get spacefilling projections
            # Ss=S/np.asmatrix(np.sum(S,axis = 1)).T #get all points to sum to 1
            # TODO: add Test/Warning if self.n0 too small
            # rank of S concatenated with column vector of ones should be dim+1
            # _S = Ss[:, 0:self.dim-1]
            _Y = np.zeros((self.n0,1))
            for ii in range(self.n0):
                currPoint   = np.array(_S[ii]).ravel()
                currPointIO = list(currPoint)
                currPointIO.append(1-currPoint.sum()) # This is the one with the last weight being set such that the sum of weights is 1

                report = self.objective(currPointIO)
                _Y[ii,0] = report[self.useObj]
                self._reports.append(report)

                self._xtrain.append( currPoint )
                self._ytrain.append( _Y[ii,0] )
                self._xall.append( currPoint )
                self._yall.append( _Y[ii,0] )

                if self.debug:
                    print ("RBF training, step %i complete"%(ii))

        import rbf
        self.R=rbf.RBF(self.dim-1, **self.kwargs)
        self.R.fit(_S,_Y)

    def sample_selection(self, cands):
        """
        TODO: add veto list
        """
        import numpy as np
        CandValue, dist_val = self.R.predict(cands)
        MinCandValue = np.amin(CandValue)
        MaxCandValue = np.amax(CandValue)

        if MinCandValue == MaxCandValue:
                ScaledCandValue = np.ones((CandValue.shape[0], 1))
        else:
                ScaledCandValue = (CandValue - MinCandValue) / (MaxCandValue - MinCandValue)

        normval = {}
        n_samp = 1
        if n_samp == 1:
                CandMinDist = np.asmatrix(np.amin(dist_val, axis = 0)).T
                MaxCandMinDist = np.amax(CandMinDist)
                MinCandMinDist = np.amin(CandMinDist)
                if MaxCandMinDist == MinCandMinDist:
                    ScaledCandMinDist = np.ones((CandMinDist.shape[0], 1))
                else:
                    ScaledCandMinDist = (MaxCandMinDist - CandMinDist) / (MaxCandMinDist - MinCandMinDist)

                # compute weighted score for all candidates
                CandTotalValue = self.R.rbf_weight * ScaledCandValue + (1 - self.R.rbf_weight) * ScaledCandMinDist

                # assign bad scores to candidate points that are too close to already sampled
                # points
                CandTotalValue[CandMinDist < self.tolerance] = np.inf #TODO Similar thing for veto list

                MinCandTotalValue = np.amin(CandTotalValue)
                selindex = np.argmin(CandTotalValue)
                xselected = np.array(cands[selindex, :])
                normval[0] = np.asmatrix((dist_val[:, selindex])).T
        else:
                raise myException('Error: Selection of several sample points not yet implemented.')

            if self.cycling=="steps":
                self.R.rbf_weight -= 0.25
                if self.R.rbf_weight<0:
                    self.R.rbf_weight =1
            elif self.cycling=="random":
                import numpy as np
                self.R.rbf_weight = np.random.random()

            if self.debug:
                print("RBF weight:", self.R.rbf_weight)


        return xselected, normval

    @property
    def Fbest(self):
        return self._Fbest

    @property
    def xbest(self):
        return self._xbest

    @property
    def Fsel(self):
        return self._Fsel

    @property
    def xsel(self):
        return self._xsel

    def run(self):
        sigma = self.sigma_stdev
        nFail, nSucc, nShrink = 0, 0, 0

	maxshrinkparam = 5 # maximal number of shrinkage of standard deviation for normal distribution when generating the candidate points
	failtolerance = max(5, self.dim)
	succtolerance =3
	localminflag = 0  # indicates whether or not xbest is at a local minimum
        for nit in range(self.maxevals):
            if localminflag!=0:
                print("Local minimum found, exiting")
                break
        # while (self.R._X.shape[0]-self.n0) < self.maxevals and (localminflag == 0):
            candidates = self.R.create_cands(self.dim * self.ncands, pertP=self.pertP, sigma_stdev=sigma, xlow=0, xup=1)
            xselected, dist_val = self.sample_selection(candidates)
            xselectedIO = list(xselected.ravel())
            xselectedIO.append(1-xselected.ravel().sum()) # This is the one with the last weight being set such that the sum of weights is 1

            report = self.objective(xselectedIO)
            Fselected = report[self.useObj]
            if Fselected is None:
                print("Invalid objective, skipping")
                continue
            self._reports.append(report)
            if self.debug: print(xselected, " : ", Fselected)
            if self.debug: print(self.R._X.shape[0]-self.n0, " / ", self.maxevals)

            self._xsel.append(xselected)
            self._Fsel.append(Fselected)
            self._xall.append(xselected)
            self._yall.append(Fselected)

            if Fselected < self.R.Fbest:
                if self.R.Fbest - Fselected > (1e-3)*np.fabs(self.R.Fbest):
                    nFail = 0
                    nSucc+= 1
                else:
                    nFail+= 1
                    nSucc = 0
                self.R._xbest = xselected
                self.R._Fbest = Fselected
            else:
                nFail+=1
                nSucc =0
            self._xbest.append(self.R._xbest)
            self._Fbest.append(self.R._Fbest)

            # check if algorithm is in a local minimum
            shrinkflag = 1
            if nFail >= failtolerance:
                if nShrink >= maxshrinkparam:
                    shrinkflag = 0
                nFail = 0

                if shrinkflag == 1:
                    nShrink+=1
                    sigma/=2
                else:
                    localminflag = 1

            if nSucc >= succtolerance:
                sigma = min(2 * sigma, self.sigma_stdev)
                nSucc = 0

            # update RBF
            # if self.R._X.shape[0] < self.maxevals and localminflag == 0:
            self.R.append(xselected, Fselected)
            if self.printprogress and (nit+1)%10 ==0:
                print("[%i/%i] complete"%(nit+1, self.maxevals))


    def toJson(self):
        d = {}
        d["Fbest"] = []



if __name__ == "__main__":
    import os, sys
    np.random.seed(1)
    from objectives import ProfObj
    PFX = sys.argv[5]
    P=ProfObj(sys.argv[1], ifile=sys.argv[2], datadir=sys.argv[3], wtemplate=sys.argv[4], prefix=PFX+"trace")
    SO = SurrogateOptimiser(dim=P.dimWeights, objective=P, useobjective="trace", maxevals=100)
    SO.mkRBF()
    SO.run()
    from pyoo import tools
    tools.mkPlot(SO, "SO_%strace.pdf"%PFX)

    np.random.seed(1)
    P=ProfObj(sys.argv[1], ifile=sys.argv[2], datadir=sys.argv[3], wtemplate=sys.argv[4], prefix=PFX+"det")
    SO = SurrogateOptimiser(dim=P.dimWeights, objective=P, useobjective="det", maxevals=100)
    SO.mkRBF()
    SO.run()
    tools.mkPlot(SO, "SO_%sdet.pdf"%PFX)

    np.random.seed(1)
    P=ProfObj(sys.argv[1], ifile=sys.argv[2], datadir=sys.argv[3], wtemplate=sys.argv[4], prefix=PFX +"pf")
    SO = SurrogateOptimiser(dim=P.dimWeights, objective=P, useobjective="portfolio", riskAv=4, maxevals=100)
    SO.mkRBF()
    SO.run()
    tools.mkPlot(SO, "SO_%spf.pdf"%PFX)
