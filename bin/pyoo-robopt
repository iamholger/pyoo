#!/usr/bin/env python3
from apprentice.appset import TuningObjective2

from apprentice.tools import TuningObjective
import autograd.numpy as np
import os
from timeit import default_timer as timer
import os, sys
import argparse
from timeit import default_timer as timer

class ROptCaller(TuningObjective2):
    def __init__(self, *args,scargs,**kwargs):
        # apprfile = scargs.INDIR + "/approximation.json"
        # datafile = scargs.INDIR + "/experimental_data_ro.json"
        # if not os.path.exists(datafile):
        #     datafile = scargs.INDIR + "/experimental_data.json"
        # wtfile = scargs.INDIR + "/weights"
        apprfile = scargs.APPROX
        datafile = scargs.DATA
        wtfile = scargs.WEIGHTS

        super().__init__(wtfile, datafile, apprfile,
                         scargs.ERROR,
                         filter_envelope=scargs.FILTERENVELOPE,
                         filter_hypothesis=scargs.FILTERHYPO,
                         debug=scargs.DEBUG,
                         noise_exponent=2
                         )

        odir = os.path.join(scargs.OUTDIR, "out")
        ldir = os.path.join(scargs.OUTDIR, "log")
        os.makedirs(odir, exist_ok=True)
        os.makedirs(ldir, exist_ok=True)
        self.outfp = odir + "/out_mu{}_ms{}_l{}.json".format(scargs.MU, scargs.MEANSHIFT, scargs.LAMBDA)
        self.logfp = ldir + "/log_mu{}_ms{}_l{}.json".format(scargs.MU, scargs.MEANSHIFT, scargs.LAMBDA)

        self.hnames = sorted(list(set(self._hnames)))
        def obsBins(hname):
            return [i for i, item in enumerate(self._binids) if item.startswith(hname)]
        self.OBSBINS = [obsBins(hname) for hname in self.hnames]
        self.WIDX = np.zeros(len(self._binids),dtype=np.int)
        for onum,obsbin in enumerate(self.OBSBINS):
            for o in obsbin:
                self.WIDX[o] = onum
        self.indir = scargs.OUTDIR
        self.nparam = self._SCLR.dim
        self.nweight = len(self.hnames)
        self.solveropt = scargs.SOLVER
        self.useweights = scargs.WTS
        self.useexpvals = scargs.EXPVALS
        self.mu = scargs.MU
        self.wstartpoint = None # DEPRICATED
        self.wstartpointcomment = None # DEPRICATED
        self.meanshift = scargs.MEANSHIFT
        self.regparam = scargs.LAMBDA
        self._debug = scargs.DEBUG

        self.run = self.runrobopt

        self.RBO = ROpt(RO=self)
        self.RBO.gradient = self.RBO.gradientAD
        self.RBO.jacobian = self.RBO.jacobianAD

    def runrobopt(self):
        """
        Runs robust optimization using RBO (class:ROpt)
        :return: x0: Starting points used
                 x:  argmin found
                 info: Optimization info including minimum objective
        """
        start = timer()
        (x0, w, p, t, rnorm, rnorm2, rnorm3, snorm, info) = (None,None,None,None,None,None,None,None,None)
        # Invoke Python based IPOPT
        if self.solveropt == 'cyipopt':
            if self._EAS is not None:
                raise Exception("Solver cyipopt not implemented with MC/Bin error. Please use solver option"
                      " \"-s ipopt\" to include MC/Bin error with robust optimization constraints.")
                sys.exit(1)
            # order of decision variables: w, p, t
            import ipopt
            nbin = len(self._binids)

            ll = 0. if self.useweights else 1.
            uu = 10. if self.useweights and self.mu == 0 else 1.

            lb = np.concatenate(([ll] * self.nweight, self._SCLR.box[:, 0], [0.] * nbin))
            ub = np.concatenate(([uu] * self.nweight, self._SCLR.box[:, 1], [None] * nbin))

            cl = [None] * 2 * nbin
            cu = [0.] * 2 * nbin

            if self.useweights:
                if self.mu>0:
                    cl.append(self.nweight * self.mu)
                    cu.append(None)
                else:
                    cl.append(1.)
                    cu.append(1.)

            # np.random.seed(23456744)
            np.random.seed(56786549)

            if self.useweights:
                if self.wstartpoint is None:
                    x0 = np.random.uniform(low=0., high=1., size=(self.nweight - 1,))
                    x0 = np.append(x0, 1 - np.sum(x0))
                else:
                    x0arr = self.wstartpoint.split(',')
                    x0 = np.array([float(x) for x in x0arr])
            else:
                x0 = np.array([1.] * self.nweight)
            x0 = np.concatenate((x0.tolist(),
                [np.random.uniform(low=lbv, high=ubv, size=(1,))[0] for lbv, ubv in
                 zip(lb[self.nweight:self.nweight+self.nparam], ub[self.nweight:self.nweight+self.nparam])],
                np.random.uniform(low=0., high=1., size=(nbin,)))
            )
            nlp = ipopt.problem(
                n=len(x0),
                m=len(cl),
                problem_obj=self.RBO,
                lb=lb,
                ub=ub,
                cl=cl,
                cu=cu
            )
            nlp.addOption('mu_strategy', 'adaptive')
            nlp.addOption('max_iter', 1000)
            # nlp.setProblemScaling(
            #     obj_scaling=2,
            #     x_scaling=np.ones(len(x0), dtype=np.float)
            # )
            # nlp.addOption('nlp_scaling_method', 'user-scaling')
            nlp.addOption('tol', 1e-7)
            nlp.addOption('output_file', self.logfp)
            if not self._debug:
                nlp.addOption('print_level', 1)
            nlp.addOption('file_print_level', 5)
            x, info = nlp.solve(x0)
            if self._debug:
                print("Solution of the primal variables: x=%s\n" % repr(x))
                print("Solution of the dual variables: lambda=%s\n" % repr(info['mult_g']))
                print("Objective=%s\n" % repr(info['obj_val']))

            r_w = x[:self.nweight]
            r_p =  x[self.nweight:self.nweight + self.nparam:]
            r_t = x[self.nweight + self.nparam:]
            # TODO: For now there are three rnorms. This is for testing of what works with L curve. Make this only one

            r_resnorm = 0.
            r_resnorm2 = 0.
            r_resnorm3 = 0.
            r_solnorm = 0.
            lbarrnol = [m - sd*sd for m, sd in zip(self._Y, self._E)]
            ubarrnol = [m + sd*sd for m, sd in zip(self._Y, self._E)]
            rball = self._AS.vals(r_p)
            for binno in range(len(r_t)):
                rb = rball[binno]

                """
                rn option 1
                """
                a = r_w[self.WIDX[binno]] * (rb - self.RBO.lbarr[binno]) ** 2
                b = r_w[self.WIDX[binno]] * (rb - self.RBO.ubarr[binno]) ** 2
                r_resnorm += a if a >= b else b
                """
                rn option 2
                """
                a = r_w[self.WIDX[binno]] * (rb - lbarrnol[binno]) ** 2
                b = r_w[self.WIDX[binno]] * (rb - ubarrnol[binno]) ** 2
                r_resnorm2 += a if a >= b else b

                """
                rn option 3
                """
                r_resnorm3 += r_w[self.WIDX[binno]] * (rb - self._Y[binno]) ** 2

                """
                sn option 1
                """
                r_solnorm += (r_w[self.WIDX[binno]] * rb) ** 2
            (x0, w, p, t, rnorm, rnorm2, rnorm3, snorm, info) =\
                (x0[:self.nweight + self.nparam],r_w, r_p, r_t, r_resnorm,r_resnorm2,r_resnorm3,r_solnorm, info)
        # Invoke AMPL based IPOPT through PYOMO
        elif self.solveropt == 'ipopt':
            (x0, w, p, t, rnorm, rnorm2, rnorm3, snorm, info) =  self.pyomorobustopt()

        else:
            raise Exception('{solver} solver type not supported'.format(solver=self.solveropt))

        if self._debug:
            print(rnorm, rnorm2, rnorm3, snorm)
        end = timer()
        ootime = end - start
        if self._debug:
            print("Time = {}".format(ootime))
        youtermin = info['obj_val']
        status = repr(info['status'])
        status_msg = repr(info['status_msg'])
        reports = {}
        RR = {
            "X_outer": [w.tolist()],  # parameters of outer objective per iteration
            "X_inner": [p.tolist()],
            # parameters of inner objective per iteration (i.e. tuned parameters given weights)
            "Y_outer": youtermin,  # Outer objective
            "residualnorm1": rnorm,
            "residualnorm2": rnorm2,
            "residualnorm3": rnorm3,
            "solutionnorm": snorm,
            "regparam": self.regparam,
            "Y_inner": None,  # Inner objective
            "OOstartpoint": [x0.tolist()],
            # "OOstartpointcomment": [opts.OOSTARTPOINTC], #TODO uncomment when moving to pyoo-app
            "pnames_outer": RO.hnames,  # The parameter names of the oo -- i.e. histogram names
            "pnames_inner": RO.pnames,
            "binids": RO._binids,
            "DOE_METRIC": None,
            "DOE_BASE": None,
            'log': {
                'fimtime': None,
                'ootime': ootime,
                'oostatus': [status],
                'oostatus_msg': [status_msg]
            }
        }
        reports['chain-0'] = RR
        import json
        with open(self.outfp, 'w') as f:
            json.dump(reports, f, indent=4)
        if self._debug:
            print('Solver log file is in:    {}'.format(self.logfp))
            print('Solution file written to:     {}'.format(self.outfp))

    def pyomorobustopt(self,solver='ipopt'):
        from pyomo import environ
        def objective(model):
            tsum = 0.
            for i in model.trange:
                tsum += model.tvar[i]
            return tsum

        def tconstraint(model,index,uselb):
            # Calculate r_b
            # Scale p
            if model.toscale:
                scaledp = [model.scaleterm[i]*(model.pvar[i] - model.Xmin[i]) + model.a[i] for i in model.prange]
            else:
                scaledp = [model.pvar[i] for i in model.prange]
            nterm = 0.
            dterm = 0.
            bindex = 0
            for block in model.ASstructure:
                term = 1.
                for pnum in model.prange:
                    term *= scaledp[pnum] ** block[pnum]
                nterm += term * model.ASPC[index][bindex]
                dterm += term * model.ASQC[index][bindex]
                bindex+=1
            rb = nterm/dterm

            deltarb = 0.
            if self._EAS is not None:
                nterm = 0.
                dterm = 0.
                bindex = 0
                for block in model.EASstructure:
                    term = 1.
                    for pnum in model.prange:
                        term *= scaledp[pnum] ** block[pnum]
                    nterm += term * model.EASPC[index][bindex]
                    dterm += term * model.EASQC[index][bindex]
                    bindex += 1
                deltarb = nterm / dterm


            windex = model.windexes[index]
            if uselb == 1:
                ret = (model.wvar[windex]/model.nbins[windex]) * \
                                    (rb-(model.lbarr[index]-deltarb))**2
            else: ret = (model.wvar[windex]/model.nbins[windex]) * \
                                    (rb-(model.ubarr[index]+deltarb))**2
            # ret = model.wvar[windex] * rb >=0
            return ret <= model.tvar[index]

        def pBound(model, i):
            b = (model.box[i][0], model.box[i][1])
            return b

        def etwineqconstraint(model):
            wsum = 0.
            for i in model.wrange:
                wsum += model.wvar[i]/model.nbins[i]
            return wsum >= model.nbinsrec * model.mu

        def etweqconstraint(model):
            wsum = 0.
            for i in model.wrange:
                wsum += model.wvar[i]
            return wsum == 1

        # concrete model
        model = environ.ConcreteModel()
        model.nbins = [len(o) for o in self.OBSBINS]
        model.nbinsrec = sum([1/len(o) for o in self.OBSBINS])
        model.nweight = self.nweight
        model.wrange = range(self.nweight)
        model.prange = range(self.nparam)
        model.trange = range(len(self._binids))
        model.crange = range(2)
        model.mu = self.mu
        model.lbarr = self.RBO.lbarr
        model.ubarr = self.RBO.ubarr
        model.box = self._SCLR.box.tolist()
        model.windexes = self.WIDX.tolist()
        model.wvar = environ.Var(model.wrange, bounds=(0, 1),initialize=0.5)
        model.pvar = environ.Var(model.prange, bounds=pBound)
        model.tvar = environ.Var(model.trange)
        px0 = []
        for i in model.prange:
            val = np.random.rand()*(model.box[i][1]-model.box[i][0])+model.box[i][0]
            model.pvar[i].value = val
            px0.append(val)
        wx0 = []
        for i in model.wrange:
            val = np.random.rand()
            model.wvar[i].value = val
            wx0.append(val)

        model.scaleterm = self._SCLR._scaleTerm.tolist()
        model.toscale = False
        for i in model.prange:
            if model.scaleterm[i] != 1.0:
                model.toscale = True
                break
        model.Xmin = self._SCLR._Xmin.tolist()
        model.a = self._SCLR._a.tolist()

        model.ASstructure = self._AS._structure.tolist()
        model.ASPC = self._AS._PC.tolist()
        if (self._AS._hasRationals):
            model.ASQC = self._AS._QC.tolist()
        else:
            model.ASQC = self._AS._PC.tolist()
            for num, block in enumerate(model.ASQC):
                arr = [0.] * len(block)
                arr[0] = 1.
                model.ASQC[num] = arr

        if self._EAS is not None:
            model.EASstructure = self._EAS._structure.tolist()
            model.EASPC = self._EAS._PC.tolist()
            if (self._EAS._hasRationals):
                model.EASQC = self._EAS._QC.tolist()
            else:
                model.EASQC = self._EAS._PC.tolist()
                for num, block in enumerate(model.EASQC):
                    arr = [0.] * len(block)
                    arr[0] = 1.
                    model.EASQC[num] = arr

        model.obj = environ.Objective(rule=objective, sense=1)
        model.tconstr = environ.Constraint(model.trange,model.crange, rule=tconstraint)
        if self.mu > 0 and self.mu <= 1:
            model.wconstr = environ.Constraint(rule=etwineqconstraint)
        elif self.mu>1:
            print("mu has to be >0 and <=1. mu was found to be {}\nExiting!".format(self.mu))
            exit(1)
        else:
            model.wconstr = environ.Constraint(rule=etweqconstraint)
        opt = environ.SolverFactory(solver)
        plevel=5
        if not self._debug:
            plevel = 1

        from pyutilib.services import TempfileManager
        os.makedirs('../../log/tmp',exist_ok=True)
        TempfileManager.tempdir = '../../log/tmp'
        ret = opt.solve(model, tee=True, logfile=self.logfp,keepfiles=True,options={'file_print_level':5, 'print_level':plevel})
        if self._debug:
            model.pprint()
            ret.write()
        optstatus = {'message': str(ret.solver.termination_condition), 'status': str(ret.solver.status),
                     'time': ret.solver.time, 'error_rc': ret.solver.error_rc}
        if self._debug:
            print(np.array([model.pvar[i].value for i in model.prange]))
            print(np.array([model.wvar[i].value for i in model.wrange]))
        r_w = np.array([model.wvar[i].value for i in model.wrange])
        r_p = np.array([model.pvar[i].value for i in model.prange])
        r_t = np.array([model.tvar[i].value for i in model.trange])
        x0 = np.array(wx0+px0)
        optstatus = {'message': str(ret.solver.termination_condition), 'status': str(ret.solver.status),
                     'time': ret.solver.time, 'error_rc': ret.solver.error_rc}
        info = {'obj_val': model.obj(), 'status': str(ret.solver.status), 'time': ret.solver.time,
                'error_rc': ret.solver.error_rc, 'status_msg': str(ret.solver.termination_condition)}
        return x0, r_w, r_p, r_t, 0., 0., 0., 0., info


class ROpt():
    RobObjC: ROptCaller = None
    lbarr = None
    ubarr = None

    def __init__(self, *args, RO=None, **kwargs):
        self.RobObjC = RO

        # TODO STATIC/TEMP remove getexpectedvalandvar()
        def getexpectedvalandvar():
            expvalfn = self.RobObjC.indir+"/distr_data.json"
            import json
            with open(expvalfn, 'r') as fn:
                ds = json.load(fn)
            expval = ds['eval']
            var = np.array(ds['sd'])**2

            return expval, var

        # TODO TEMP Both Required???
        if self.RobObjC.useexpvals:
            expval, eps = getexpectedvalandvar()
            self.lbarr = [expval[bno] - (self.RobObjC.regparam * eps[i]) for bno,i in enumerate(self.RobObjC.WIDX)]
            self.ubarr = [expval[bno] + (self.RobObjC.regparam * eps[i]) for bno,i in enumerate(self.RobObjC.WIDX)]
        else:
            if self.RobObjC.meanshift !=0:
                sindex = abs(self.RobObjC.meanshift)
                if sindex > 9: raise Exception("Meanshift of abs({}) > 9 not allowed".format(self.RobObjC.meanshift))
                mult = self.RobObjC.meanshift/abs(self.RobObjC.meanshift)
                nos  = 10
                varpartitioned = [np.linspace(0,np.abs(sd),nos) for sd in RO._E]

                self.lbarr = [(m + mult * vp[sindex]) - (self.RobObjC.regparam * sd) for m, vp, sd in
                              zip(RO._Y, varpartitioned, RO._E)]
                self.ubarr = [(m + mult * vp[sindex]) + (self.RobObjC.regparam * sd) for m, vp, sd in
                              zip(RO._Y, varpartitioned, RO._E)]
            else:
                self.lbarr = [m - (self.RobObjC.regparam * sd) for m, sd in zip(RO._Y, RO._E)]
                self.ubarr = [m + (self.RobObjC.regparam * sd) for m, sd in zip(RO._Y, RO._E)]

            # self.lb_inarr = [m - 0.5 * (self.RobObjC.regparam * sd) for m, sd in zip(RO._Y, RO._E)]
            # self.lb_outarr = [m - 1.5 * (self.RobObjC.regparam * sd) for m, sd in zip(RO._Y, RO._E)]
            #
            # self.ub_outarr = [m + 0.5 * (self.RobObjC.regparam * sd) for m, sd in zip(RO._Y, RO._E)]
            # self.ub_inarr = [m + 1.5 * (self.RobObjC.regparam * sd) for m, sd in zip(RO._Y, RO._E)]

            # self.lb_inarr = [v - 0.01 * (sd) for v, sd in zip(self.lbarr, RO._E)]
            # self.lb_outarr = [v + 0.01 * (sd) for v, sd in zip(self.lbarr, RO._E)]
            #
            # self.ub_outarr = [v + 0.01 * (sd) for v, sd in zip(self.ubarr, RO._E)]
            # self.ub_inarr = [v - 0.01 * (sd) for v, sd in zip(self.ubarr, RO._E)]


    # TODO: Commented out for now... Required??? Doing iterative RO?
    # def overwriteconstrbounds(self, newY=None):
    #     self.lbarr = [m - (self.RobObjC.regparam * sd*sd) for m, sd in zip(newY, RO._E)]
    #     self.ubarr = [m + (self.RobObjC.regparam * sd*sd) for m, sd in zip(newY, RO._E)]

    def objective(self, x):
        # order of decision variables: w, p, t
        if self.RobObjC._debug:
            print(x[:self.RobObjC.nweight], x[self.RobObjC.nweight:self.RobObjC.nweight+self.RobObjC.nparam])
        sys.stdout.flush()
        t = x[self.RobObjC.nweight + self.RobObjC.nparam:]
        return sum(t)

    def gradientAD(self, x):
        from autograd import elementwise_grad
        egrad = elementwise_grad(self.objective)
        vegrad = np.array(egrad(x))
        # print('AD', x, vegrad)
        return np.array(vegrad)

    def constraints(self, x):
        # order of decision variables: w, p, t
        w = x[:self.RobObjC.nweight]
        p = x[self.RobObjC.nweight:self.RobObjC.nweight+self.RobObjC.nparam:]
        t = x[self.RobObjC.nweight + self.RobObjC.nparam:]
        cons = []
        rb = self.RobObjC._AS.vals(p)
        for binno in range(len(t)):
            cons.append(w[self.RobObjC.WIDX[binno]] * (rb[binno] - self.lbarr[binno]) ** 2 - t[binno])
            cons.append(w[self.RobObjC.WIDX[binno]] * (rb[binno] - self.ubarr[binno]) ** 2 - t[binno])

            # if not self.RobObjC.useexpvals:
            #     # cons.append(w[self.RobObjC.WIDX[binno]] * (rb - self.lb_inarr[binno]) ** 2 - t[binno])
            #     cons.append(w[self.RobObjC.WIDX[binno]] * (rb - self.lb_outarr[binno]) ** 2 - t[binno])
            #
            #     # cons.append(w[self.RobObjC.WIDX[binno]] * (rb - self.ub_inarr[binno]) ** 2 - t[binno])
            #     cons.append(w[self.RobObjC.WIDX[binno]] * (rb - self.ub_outarr[binno]) ** 2 - t[binno])
        if self.RobObjC.useweights:
            cons.append(sum(w))
        return np.array(cons)

    def jacobianAD(self, x):
        from autograd import jacobian
        jac = jacobian(self.constraints)
        vjac = jac(x)
        return np.array(vjac)

class SaneFormatter(argparse.RawTextHelpFormatter,
                    argparse.ArgumentDefaultsHelpFormatter):
    pass
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Robust Optimization for Model Fitting',
                                     formatter_class=SaneFormatter)

    # parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
    #                     help="In dir where approximations (as approximation.json), data "
    #                          "(as experimental_data.json) and weight file (as weights) are stored "
    #                          "for building ideal parameters")
    parser.add_argument("-a", "--approx", dest="APPROX", type=str, default=None,
                        help="Filename where MC central value approximations is stored")
    parser.add_argument("-d", "--expdata", dest="DATA", type=str, default=None,
                        help="Filename where experimental data is stored")
    parser.add_argument("-w", "--weights", dest="WEIGHTS", type=str, default=None,
                        help="Filename where weights is stored")
    parser.add_argument("-e", "--error", dest="ERROR", type=str, default=None,
                        help="Filename of the MC error stddev approximations is stored")

    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")

    parser.add_argument("-s", "--solver", dest="SOLVER", type=str, default='ipopt',
                        help="Solver to use. Use \"ipopt\" for AMPL IPOPT solver that runs "
                             "with Pyomo. Use \"cyipopt\" for solver with python wrapper around IPOPT.")

    parser.add_argument("--useweights", dest="WTS", default=False, action="store_true",
                        help="Use weights in the robust optimization formulation")
    parser.add_argument("--useexpvals", dest="EXPVALS", default=False, action="store_true",
                        help="Use Expected values and standard deviations (SD) from the bin surrograte models (r_b) "
                             "instead of using the data mean (_Y) and SD (_E) ")

    parser.add_argument('-m',"--mu", dest="MU", type=float, default=1.,
                        help="\mu \in [0,1] Mu value for use in constraint $e^T w \ge \mu * N_{observables}$")

    parser.add_argument("--meanshift", dest="MEANSHIFT", type=int, default=0,
                        help="Shift mean by in the interval -SD(-_E)...MEAN(_Y)...SD(_E). Should be an integer between "
                             "-9 and 9. If 0, mean is not shifted, -9 means that the mean is shifted to -SD in"
                             "the interval, and 9 means that the mean is shifted to SD in the interval. "
                             "SD itself is not changed")

    parser.add_argument("--filterhypo", dest="FILTERHYPO", default=False, action="store_true",
                        help="Do hypothesis filtering")
    parser.add_argument("--filterenvelope", dest="FILTERENVELOPE", default=False, action="store_true",
                        help="Do Envelope filtering")

    parser.add_argument("-l", "--lambda", dest="LAMBDA", type=float, default=1.,
                        help="Regularization parameter over the width of the uncertainty set interval "
                             "-SD(-_E)...MEAN(_Y)...SD(_E). Used as Y + \lambda * _E and Y - \lambda * _E.")

    parser.add_argument("-v", "--debug", dest="DEBUG", action="store_true", default=False,
                        help="Turn on some debug messages")
    args = parser.parse_args()
    if args.DEBUG:
        print(args)

    RO = ROptCaller(scargs=args)
    RO.run()
