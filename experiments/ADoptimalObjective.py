#! /usr/bin/env python
import numpy as np
import apprentice

class OEDObjective():

    def __init__(self, *args, TuningObjective2,params,**kwargs):
        self.TO = TuningObjective2
        self.PMAP = params
        self.EMG = self.TO._AS.grads(self.PMAP)
        self.F_e = {}
        self.B_e = {}
        self.hnames = sorted(list(set(self.TO._hnames)))
        def obsBins(hname):
            return [i for i, item in enumerate(self.TO._binids) if item.startswith(hname)]
        self.OBSBINS = [obsBins(hname) for hname in self.hnames]

        self.GAMMANOISEINVMAT_e = {}
        for hno, hname in enumerate(self.hnames):
            obsbins = self.OBSBINS[hno]
            err = np.array([e for e in self.TO._E[obsbins]])
            self.GAMMANOISEINVMAT_e[hname] = np.zeros((len(err), len(err)))
            np.fill_diagonal(self.GAMMANOISEINVMAT_e[hname], err)

            if self.TO._EAS is not None:
                err2 = self.TO._EAS.vals(params, sel=obsbins)**2
                GAMMANOISEINVMAT_e2 = np.zeros((len(err2), len(err2)))
                np.fill_diagonal(GAMMANOISEINVMAT_e2, err)
                self.GAMMANOISEINVMAT_e[hname] += GAMMANOISEINVMAT_e2

            self.GAMMANOISEINVMAT_e[hname] = np.linalg.inv(np.transpose(self.GAMMANOISEINVMAT_e[hname]))
        for hno, h in enumerate(self.hnames):
            obsbins = self.OBSBINS[hno]
            self.F_e[h] = [self.EMG[bnum].tolist() for bnum in obsbins]
            B = np.matmul(self.GAMMANOISEINVMAT_e[h],self.F_e[h])
            self.B_e[h] = np.matmul(np.transpose(B),B)

    def calculateAoptimalObjective(self,w):
        return self.__objective(w,"A")

    def calculateDoptimalObjective(self,w):
        return self.__objective(w,"D")

    def calculatelogDoptimalObjective(self,w):
        return self.__objective(w,"logD")

    def __doe_metric(self,M,doe_type):
        import autograd.numpy as np
        if doe_type == "A":
            return np.trace(M)
        elif doe_type == "D":
            return np.linalg.det(M)
        elif doe_type == "logD":
            return np.log(np.linalg.det(M))
        else:
            raise Exception("Specified metric type not known.")

    def __objective(self,x,doe_type):
        w = np.array(x)
        obj = self.__doe_metric(self.getposteriorcovmatrix(w),doe_type)
        return obj

    def getposteriorcovmatrix(self,x):
        w = np.array(x)
        A = w[0] * self.B_e[self.hnames[0]]
        for hno,hname in enumerate(self.hnames):
            if hno == 0: continue
            A += w[hno] * self.B_e[hname]
        return np.linalg.inv(A)

if __name__ == "__main__":
    import os
    INDIR = "../data/A14"
    apprfile = os.path.join(INDIR, "approximation.json")
    datafile = os.path.join(INDIR, "experimental_data.json")
    wtfile = os.path.join(INDIR, "weights")
    errapprfile = os.path.join(INDIR, "errapproximation.json")

    TO = apprentice.appset.TuningObjective2(
        wtfile, datafile, apprfile,
        f_errors=errapprfile,
        noise_exponent=2
    )

    oedo = OEDObjective(TuningObjective2=TO,params=[1,1,1,0.5,1,0.5,1,1,1,1])
    # print(oedo.calculateAoptimalObjective())
    # print(oedo.calculateDoptimalObjective([1] * len(oedo.hnames)))
    # print(oedo.calculatelogDoptimalObjective([1] * len(oedo.hnames)))