import argparse,json
import numpy as np

import pyoo.outer_objective as oobj
class SaneFormatter(argparse.RawTextHelpFormatter,
                    argparse.ArgumentDefaultsHelpFormatter):
    pass

def plotpdiff(args,priorparam,outdirarg):
    apprfile = os.path.join(args.INDIR, "approximation.json")
    datafile = os.path.join(args.INDIR, "experimental_data.json")
    wtfile = os.path.join(args.INDIR, "weights")

    if args.CALIBERROR:
        datafile = os.path.join(args.INDIR, "experimental_data_cenoise.json")

    OO = oobj.OuterObjective(wtfile, datafile, apprfile,
                             filter_envelope=False,
                             filter_hypothesis=False,
                             metric=args.DOEMETRIC,
                             metric_base=args.DOEBASE,
                             restart_filter=100,
                             debug=False,
                             OO_uses_sym_cache=True,
                             noise_exponent=2)
    if args.MSE:
        OO._E2 = np.array([1.] * len(OO._E2))
    OBSBINS = [OO.obsBins(hname) for hname in OO._hnames]
    LAMBDA = args.LAMBDA
    stopcond = args.STOPCOND

    outdir = os.path.join(outdirarg, "out")
    if not os.path.exists(outdir): os.makedirs(outdir, exist_ok=True)

    outfp = outdir + "/out_dbase{}_dm{}_lambda{:.2e}".format(args.DOEBASE, args.DOEMETRIC, LAMBDA)
    prev = None
    X = []
    Yw1 = []
    Ywnot1 = []
    SSstr = []

    with open(priorparam, 'r') as f:
        data = json.load(f)

    pcurr = np.array(data['chain-0']['X_inner'][0])
    wcurr = np.array(data['chain-0']['X_outer'][0])
    pnamesinner = data['chain-0']['pnames_inner']
    for pno, pname in enumerate(pnamesinner):
        X.append([])
    for pno, pname in enumerate(pnamesinner):
        X[pno].append(pcurr[pno])
    SSstr.append("pr")

    OO.setWeights(wcurr)
    e2notw1 = OO.obswiseObjective(pcurr)
    for hno, hname in enumerate(OO.hnames):
        e2notw1[hno] /= len(OBSBINS[hno])
    Ywnot1.append(sum(e2notw1))

    OO.setWeights([1.] * len(wcurr))
    e2w1 = OO.obswiseObjective(pcurr)
    for hno, hname in enumerate(OO.hnames):
        e2w1[hno] /= len(OBSBINS[hno])
    Yw1.append(sum(e2w1))
    lastwcurr = None
    for i in range(100):
        print("Processing iter, {}".format(i))
        fn = outfp+"_iter{}.json".format(i)
        if not os.path.exists(fn):
            break
        with open(fn , 'r') as f:
            data = json.load(f)
        pcurr = np.array(data['chain-0']['X_inner'][0])
        wcurr = np.array(data['chain-0']['X_outer'][0])
        pnamesinner = data['chain-0']['pnames_inner']
        if i !=0:
            next = pcurr
            print(sum((next - prev) ** 2), stopcond)
            if sum((next-prev)**2) <= stopcond:
                break
        prev = pcurr

        for pno,pname in enumerate(pnamesinner):
            X[pno].append(pcurr[pno])

        OO.setWeights(wcurr)
        e2notw1 = OO.obswiseObjective(pcurr)
        for hno, hname in enumerate(OO.hnames):
            e2notw1[hno] /= len(OBSBINS[hno])
        Ywnot1.append(sum(e2notw1))

        OO.setWeights([1.] * len(wcurr))
        e2w1 = OO.obswiseObjective(pcurr)
        for hno,hname in enumerate(OO.hnames):
            e2w1[hno] /= len(OBSBINS[hno])
        Yw1.append(sum(e2w1))
        SSstr.append(repr(i + 1))
        lastwcurr = wcurr


    import matplotlib.pyplot as plt
    import matplotlib as mpl
    plt.clf()
    mpl.rc('text', usetex=False)
    mpl.rc('font', family='serif', size=12)
    mpl.style.use("ggplot")
    fig, axs = plt.subplots(2, 5, sharey=True)
    fig.set_size_inches(20.5, 10.5)
    X = np.array(X)
    jj=-1
    for pno,pname in enumerate(pnamesinner):
        ax = axs[int(pno/5)][pno%5]

        # plt.plot(X[pno, :], Ywnot1, marker='.')
        ax.plot(X[pno, :], Yw1, marker='.')
        ax.set_yscale('log')
        # plt.subplots_adjust(right=1.3)
        # plt.xscale('log')
        ax.set_xlabel(pname)
        # ii=0
        # SSstr[4] = "..."
        for x, y, s in zip(X[pno, :], Yw1, SSstr):
            ax.text(x, y, s, fontsize=12)
            # if ii ==4:
            #     break
            # ii+=1
        if pno %5==0:
            ax.set_ylabel("$\chi^2_{\Gamma_{noise}^{-1}} (w = \\vec{1})$")

    vartypestr = ""
    if args.MSE:
        vartypestr = "mse"
    elif args.CALIBERROR:
        vartypestr = "model-error-variance"
    else:
        vartypestr = "data-variance"
    od = os.path.join(outdirarg,"plots")
    os.makedirs(od,exist_ok=True)
    priorname = os.path.basename(priorparam).split('.')[0]
    ofn = os.path.join(od,"plot_dbase{}_dm{}_lambda{:.2e}_{}_{}.pdf".format(args.DOEBASE, args.DOEMETRIC, LAMBDA,priorname,vartypestr))
    # plt.show()
    plt.savefig(ofn)

    W2 = np.zeros(len(OO._E2))
    for hno, hname in enumerate(OO.hnames):
        W2[OBSBINS[hno]] = lastwcurr[hno]
    ofn2 = os.path.join(od, "plot_wbysigma_dbase{}_dm{}_lambda{:.2e}_{}_{}.pdf".format(args.DOEBASE, args.DOEMETRIC, LAMBDA,priorname,vartypestr))
    plotwbysigma(OO._E2,W2,ofn2)
    return SSstr,Yw1




def plotwbysigma(E2,W2,ofn):
    import pylab
    pylab.clf()
    pylab.rc('text', usetex=False)
    pylab.rc('font', family='serif', size=12)
    pylab.style.use("ggplot")
    Y = W2*E2
    pylab.scatter(range(len(W2)), Y, marker='.', c="r", s=20, alpha=1.0)

    # plt.plot(range(len(W2)), W2 * E2)
    pylab.ylim(min(Y)/10,max(Y)*10)
    pylab.yscale('log')
    pylab.plot([0,len(W2)],[1,1], c='black')
    pylab.savefig(ofn)
    pylab.clf()

def plotpriorcomparisonplot(args):
    X = {}
    Y = {}

    for prior,outdir in zip(args.PARR,args.OARR):
        prname = os.path.basename(prior).split('.')[0]
        SSstr,Yw1 = plotpdiff(args,prior,outdir)
        X[prname] = SSstr
        Y[prname] = Yw1

    import matplotlib.pyplot as plt
    import matplotlib as mpl
    plt.clf()
    mpl.rc('text', usetex=False)
    mpl.rc('font', family='serif', size=12)
    mpl.style.use("ggplot")
    plt.close('all')
    xlab = []
    for prior, outdir in zip(args.PARR, args.OARR):
        prname = os.path.basename(prior).split('.')[0]
        plt.plot(np.arange(len(X[prname])),Y[prname],label=prname)
        if len(xlab) < len(SSstr):
            xlab = SSstr
    plt.xticks(np.arange(len(xlab)),xlab)
    plt.legend()
    plt.yscale('log')
    od = os.path.join(args.OARR[0], "plots")
    LAMBDA = args.LAMBDA
    vartypestr = ""
    if args.MSE:
        vartypestr = "mse"
    elif args.CALIBERROR:
        vartypestr = "model-error-variance"
    else:
        vartypestr = "data-variance"
    ofn = os.path.join(od,
                        "plot_combined_iteration_progress_dbase{}_dm{}_lambda{:.2e}_{}.pdf".format(args.DOEBASE, args.DOEMETRIC, LAMBDA,vartypestr))
    plt.savefig(ofn)
    plt.close('all')



    plt.show()



if __name__ == "__main__":
    import os


    parser = argparse.ArgumentParser(description='pdiff plots',
                                     formatter_class=SaneFormatter)
    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")

    parser.add_argument("-d", "--doemetric", dest="DOEMETRIC", type=str, default="A",
                        help="DOE Metric. Options are A, D logD")
    parser.add_argument("-b", "--doebase", dest="DOEBASE", type=str, default="WFIM_with_grad",
                        help="DOE Metric Base. Options are WFIM_with_grad, FIM, WFIM, H")

    parser.add_argument("--usecaliberrors", dest="CALIBERROR", default=False, action="store_true",
                        help="Use caliberation errors instead of observation errors")
    parser.add_argument("--domse", dest="MSE", default=False, action="store_true",
                        help="Use MSE in performance")

    parser.add_argument('--stopcond', dest="STOPCOND", default=10**-6, type=float,
                        help="Stoping condition metric")

    parser.add_argument("-p", "--prior", dest="PRIORPARAM", type=str, default=None,
                        help="Prior parameter file path")

    parser.add_argument('-l','--lambda', dest="LAMBDA", type=float, default=1.,
                        help="Lambda parameter for regularization")

    parser.add_argument('--parr', dest="PARR", default=[], nargs='+',
                        help="Filenames of prior param comparison plots to plot."
                             " Use any other parameter except for -p and -o. If the size of this"
                             " array >0, then -p and -o option will be ignored.")
    parser.add_argument('--oarr', dest="OARR", default=[], nargs='+',
                        help="Output directoried connresponding to prior param in --parr"
                             " where the output out folder is stored. "
                             " Plot will be stored in first output folder's plot directory"
                             " Use any other parameter except for -p and -o. If the size of this"
                             " array >0, then -p and -o option will be ignored.")


    args = parser.parse_args()
    print(args)
    if len(args.PARR)>0 and len(args.OARR)>0:
        plotpriorcomparisonplot(args)
    else:
        plotpdiff(args,args.PRIORPARAM,args.OUTDIR)
