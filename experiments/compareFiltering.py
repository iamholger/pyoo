import os,sys
import apprentice
def checkexists(args):
    k = 0
    resultdir = args.RESDIR
    for t in args.FILES:
        if t == 'd': continue
        e = os.path.exists(os.path.join(resultdir,t))
        print(e, os.path.join(resultdir,t))
        if not e: k = 1
    if k: sys.exit(1)

def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def checkTypes(args):
    import re
    typecleanarr = []

    for fnum,r in enumerate(args.FILES):
        if "gradO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME[fnum]), r).group(1))
            if mu == 0:
                # typecleanarr.append('FIM without bound')
                typecleanarr.append('FIM no bound')
            else:
                typecleanarr.append('FIM, $\{}={}$'.format(args.REGPARAMNAME[fnum],mu))
        elif "robO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(args.REGPARAMNAME[fnum]), r).group(1))
            typecleanarr.append('RO, $\{}={}$'.format(args.REGPARAMNAME[fnum], mu))
        else:
            findex = -1
            otherapproaches = ['portfolio', 'medscore', 'meanscore', # Juli/Wenjing's approaches
                               'CTEQ', 'MSTW', 'NNPDF', 'HERA'] # Approaches in A14 paper
            for othernum,other in enumerate(otherapproaches):
                if other in r:
                    findex= othernum
                    break
            if findex == -1: raise Exception("Unknown approach {}".format(r))
            typecleanarr.append('{}'.format(otherapproaches[findex]))


    # p0 = [2.47,1.71]
    # p1 = [3.0,1.2]
    # if "4gw2p_1wp0_1wp1_1wp0_1wp1" in indir:
    #     parr = [p0,p1,p0,p1]
    #     parrstr = "$p_0~p_1~p_0~p_1$"
    # elif '4gw2p_1wp0_1wp1_2wp0' in indir:
    #     parr = [p0, p1, p0, p0]
    #     parrstr = "$p_0~p_1~p_0~p_0$"
    # elif '4gw2p_2wp0_2wp1' in indir:
    #     parr = [p0, p0, p1, p1]
    #     parrstr = "$p_0~p_0~p_1~p_1$"
    # elif '4gw2p_3wp0_1wp1' in indir:
    #     parr = [p0, p0, p0, p1]
    #     parrstr = "$p_0~p_0~p_0~p_1$"
    # else:
    #     parr = [p0,p0,p0,p0]
    #     parrstr = "$p_0~p_0~p_0~p_0$"

    return typecleanarr

def calcchi2(y, d,e):
    if e == 0:
        return (y-d) ** 2
    else:
        return (y-d) ** 2 / (e ** 2)

def getchi2(indir):
    approxfile = indir + "/approximation.json"
    expdatafile = indir + "/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = indir + "/experimental_data.json"
    binids, RA = apprentice.tools.readApprox(approxfile)
    dim = RA[0].dim
    dd = apprentice.tools.readExpData(expdatafile, binids)
    hnames = sorted(list(set([b.split("#")[0] for b in binids])))

    chi2data = {}
    chi2all = {}
    for tno, type in enumerate(args.FILES):
        chi2data[type] = {}
        chi2all[type] = []
        for hno, hn in enumerate(hnames):
            _usedbins = [b for b in binids if hn in b]
            i_used = [binids.index(b) for b in _usedbins]
            D = [dd[b][0] for b in _usedbins]
            DY = [dd[b][1] for b in _usedbins]

            data = readReport(os.path.join(args.RESDIR, type))
            Y = [RA[i].predict(data["x"]) for i in i_used]
            chi2 = []
            for i,b  in enumerate(_usedbins):
                val = calcchi2(Y[i], D[i], DY[i])
                chi2data[type][b] = val
                chi2.append(val)
            chi2all[type].append(sum(chi2))
    return chi2data,chi2all

def getcepsdata(origfolder, typefolder,chi2data,chi2all):
    weightfile = typefolder + "/weights"
    approxfile = typefolder + "/approximation.json"
    fullweightfile = origfolder + "/weights"

    matchers = apprentice.weights.read_pointmatchers(weightfile)
    weights = [(m, wstr) for m, wstr in matchers.items()]

    matchersall = apprentice.weights.read_pointmatchers(fullweightfile)
    weightsall = [(m, wstr) for m, wstr in matchersall.items()]

    binids, RA = apprentice.tools.readApprox(approxfile)

    approxfileall = origfolder + "/approximation.json"
    binidsall, RAall = apprentice.tools.readApprox(approxfileall)

    cepsdatakept = {}
    cepsdataremoved = {}
    cepsdataall = {}
    chi2summaryall = {}
    chi2summarykept = {}
    chi2summaryremoved = {}

    for tno1, type1 in enumerate(args.FILES):
        chi2data1 = chi2data[type1]
        c1 = 0.
        weightsintype = []
        count = 0.
        for w in weights:
            weightsintype.append(w[0].path.pattern)
            c1 += sum([chi2data1[c] for c in chi2data1.keys() if w[0].match_path(c) and c in binids])
            count += sum([1 for c in chi2data1.keys() if w[0].match_path(c) and c in binids])
        chi2summarykept[type1] = c1/count
        # print(count)

        chi2summaryall[type1] = sum(chi2all[type1])/len(binidsall)
        # print(len(binidsall))

        count = 0.
        count += sum([1 for c in chi2data1.keys() if c not in binids])
        chi2summaryremoved[type1] = sum([chi2data1[c] for c in chi2data1.keys() if c not in binids])/count
        # print(count)

        for tno2, type2 in enumerate(args.FILES):
            if tno1 <= tno2: continue
            chi2data2 = chi2data[type2]
            c1count = 0
            c2count = 0
            weightsintype = []
            for w in weights:
                weightsintype.append(w[0].path.pattern)
                c1 = sum([chi2data1[c] for c in chi2data1.keys() if w[0].match_path(c) and c in binids])
                c2 = sum([chi2data2[c] for c in chi2data2.keys() if w[0].match_path(c) and c in binids])
                if c1 > c2:
                    c2count += 1
                elif c2 > c1:
                    c1count += 1
            cepsdatakept[type1 + "_" + type2] = c1count
            cepsdatakept[type2 + "_" + type1] = c2count
            print("{}\t\t{}\t\t{}\t\t{}".format(os.path.basename(type1), os.path.basename(type2), c1count, c2count))

            c1count = 0
            c2count = 0
            for wt in weightsall:
                w = wt[0].path.pattern
                if w in weightsintype: continue
                c1 = sum([chi2data1[c] for c in chi2data1.keys() if w in c and c not in binids])
                c2 = sum([chi2data2[c] for c in chi2data2.keys() if w in c and c not in binids])
                if c1 > c2:
                    c2count += 1
                elif c2 > c1:
                    c1count += 1
            cepsdataremoved[type1 + "_" + type2] = c1count
            cepsdataremoved[type2 + "_" + type1] = c2count
            print("{}\t\t{}\t\t{}\t\t{}".format(os.path.basename(type1), os.path.basename(type2), c1count, c2count))

            chi2dataall1 = chi2all[type1]
            chi2dataall2 = chi2all[type2]
            c1count = 0
            c2count = 0
            for c1,c2 in zip(chi2dataall1,chi2dataall2):
                if c1>c2:
                    c2count+=1
                elif c2>c1:
                    c1count += 1
            cepsdataall[type1 + "_" + type2] = c1count
            cepsdataall[type2 + "_" + type1] = c2count
            print("{}\t\t{}\t\t{}\t\t{}".format(os.path.basename(type1), os.path.basename(type2), c1count, c2count))
    print(chi2summarykept,chi2summaryall)
    print("\n\n###########################\n\n")


    print(chi2summarykept)
    print(chi2summaryremoved)
    print(chi2summaryall)
    return cepsdatakept,cepsdataremoved,cepsdataall,chi2summarykept,chi2summaryremoved,chi2summaryall

def tablofyresults(args):
    typecleanarr = checkTypes(args)
    chi2data,chi2all = getchi2(args.OINDIR)
    cepsdatakept, cepsdataremoved, cepsdataall, chi2summarykept, chi2summaryremoved, chi2summaryall = getcepsdata(
            args.OINDIR, args.INDIR, chi2data, chi2all)
    tabstr = "\\multirow{%d}{*}"%(len(args.FILES)-1)
    if args.OFILEPREFIX == "hypothesis":
        tabstr += "{Hypothesis}"
    elif args.OFILEPREFIX == "outlier":
        tabstr += "{Outlier}"
    else:
        print("ERROR: Ofile prefix required using options -x or --ofileprefix and the value should be in {hypothesis,outlier}. Current value is %s"%(args.OFILEPREFIX))
        sys.exit(1)
    btype = args.FILES[-1]
    for tno,type in enumerate(args.FILES):
        if tno == len(args.FILES)-1: break
        tabstr += "&%s"%(typecleanarr[tno])
        wrtotal = cepsdataremoved[type + "_" + btype] + cepsdataremoved[btype + "_" + type]
        wktotal = cepsdatakept[type + "_" + btype] + cepsdatakept[btype + "_" + type]
        alltotal = cepsdataall[type + "_" + btype] + cepsdataall[btype + "_" + type]
        tabstr += "\n&\\multicolumn{1}{|c|}{%.1f}" \
                  "&\\multicolumn{1}{|c|}{%.1f}" \
                  "&\\multicolumn{1}{|c|}{%.1f}\n"%(cepsdataremoved[type + "_" + btype]/wrtotal *100,
                                                cepsdatakept[type + "_" + btype]/wktotal *100,
                                                cepsdataall[type + "_" + btype]/alltotal *100)
        tabstr += "&\\multicolumn{1}{|c|}{%.1f}" \
                  "&\\multicolumn{1}{|c|}{%.1f}" \
                  "&\\multicolumn{1}{|c|}{%.1f}" % (chi2summaryremoved[type],
                                                  chi2summarykept[type],
                                                  chi2summaryall[type])
        if tno < len(args.FILES)-2: tabstr+="\\\\\\cline{2-8}\n"
        else:tabstr+="\\\\\\hline\n"
    print("\n\n###########################\n\n")
    print(tabstr)
    print("\n\n###########################\n\n")






    import math



if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='com[are filtering schemes',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir with filtered data")
    parser.add_argument("-z", "--origindir", dest="OINDIR", type=str, default=None,
                        help="In dir with all data")
    parser.add_argument("-r", "--resultdir", dest="RESDIR", type=str, default=None,
                        help="Result Dir")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix")
    parser.add_argument("-t", "--resultfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Result files in which output is stored and/or d (for true parameter)")
    parser.add_argument("--onlycheck", dest="CHECK", default=False, action="store_true",
                        help="Only check for existence of runs")
    parser.add_argument("--onlytable", dest="TABLE", default=False, action="store_true",
                        help="Only Table. Don't plot")
    parser.add_argument("-l", "--regparamname", dest="REGPARAMNAME", default=[], nargs='+',
                        help="Regularization param name per file to find in filenames")

    args = parser.parse_args()

    print(args)
    if len(args.REGPARAMNAME) == 0:
        args.REGPARAMNAME = ['mu'] * len(args.FILES)

    checkexists(args)
    if args.CHECK:  sys.exit(0)
    else: tablofyresults(args)






