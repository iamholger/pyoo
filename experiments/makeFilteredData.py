import sys,os
import numpy as np
def makehypothesisfiltereddata(indir,outdir):
    import json
    with open(indir + "/keepids.json", 'r') as f:
        keepids = json.load(f)

    with open(indir + "/approximation.json", 'r') as f:
        apprx = json.load(f)

    with open(indir + "/errapproximation.json", 'r') as f:
        errapprx = json.load(f)

    with open(indir + "/experimental_data.json", 'r') as f:
        expdata = json.load(f)

    newapprx = {}
    for k in apprx:
        if k in keepids:
            newapprx[k] = apprx[k]

    newerrapprx = {}
    for k in errapprx:
        if k in keepids:
            newerrapprx[k] = errapprx[k]

    newdata = {}
    for k in expdata:
        if k in keepids:
            newdata[k] = expdata[k]

    wstr = ""
    hnames = [b.split("#")[0] for b in keepids]
    uniquehnames = np.unique(hnames)
    for hn in uniquehnames:
        wstr += "{}\t1\n".format(hn)

    os.makedirs(outdir, exist_ok=True)
    with open(outdir + "/approximation.json", 'w') as f:
        json.dump(newapprx, f, indent=4)

    with open(outdir + "/errapproximation.json", 'w') as f:
        json.dump(newerrapprx, f, indent=4)

    with open(outdir + "/experimental_data.json", 'w') as f:
        json.dump(newdata, f, indent=4)

    with open(outdir + "/weights", 'w') as f:
        f.write(wstr)

def makeoutlierfiltereddata(indir):
    import json
    with open(indir + "/approximation.json", 'r') as f:
        apprx = json.load(f)

    with open(indir + "/errapproximation.json", 'r') as f:
        errapprx = json.load(f)

    with open(indir + "/experimental_data.json", 'r') as f:
        expdata = json.load(f)

    import apprentice
    matchers = apprentice.weights.read_pointmatchers(indir + "/weights")
    weights = [(m, wstr) for m, wstr in matchers.items()]
    newapprx = {}
    newerrapprx = {}
    newdata = {}
    for w in weights:
        for k in apprx.keys():
            if w[0].match_path(k):
                newapprx[k] = apprx[k]
                newdata[k] = expdata[k]
                newerrapprx[k] = errapprx[k]
    with open(indir + "/approximation.json", 'w') as f:
        json.dump(newapprx, f, indent=4)

    with open(indir + "/errapproximation.json", 'w') as f:
        json.dump(newerrapprx, f, indent=4)

    with open(indir + "/experimental_data.json", 'w') as f:
        json.dump(newdata, f, indent=4)

def findhypofilteredoutinorder(indir):

    apprfile = indir + "/approximation.json"
    datafile = indir + "/experimental_data.json"
    wtfile = indir + "/weights"
    import json
    with open(os.path.join(indir,"keepids.json"), 'r') as f:
        keepidds = json.load(f)
    basedir = os.path.basename(indir)
    from apprentice.tools import TuningObjective
    IO = TuningObjective(wtfile, datafile, apprfile, filter_envelope=False,
                         filter_hypothesis=False)

    fobins = {}
    for bno,bin in enumerate(IO._binids):
        if not bin in keepidds:
            hname = bin.split("#")[0]
            with open(os.path.join("../../log/optParamsPerObs", basedir, hname.replace('/', '_')+".json"), 'r') as f:
                data = json.load(f)
            chi2 = IO.obswiseObjective(x=data['chain-0']['X_inner'][0],unbiased=True,binids=[[bno]])
            fobins[bin]=chi2


    from collections import OrderedDict
    fobinssorted = OrderedDict(sorted(fobins.items(),reverse=True, key=lambda x: x[1]))
    folderin = "/Users/mkrishnamoorthy/Box/PhysicsData/HEP_Fermi/A14"
    folderout = "/Users/mkrishnamoorthy/Box/PhysicsData/HEP_Fermi/A14_filtered_out_subset"
    import shutil
    index = 0
    for bno,bin in enumerate(fobinssorted):
        if bno %20==0:
            index +=1
        dir = os.path.join(folderout,repr(index))
        os.makedirs(dir, exist_ok=True)
        name = bin.replace("/","_")
        shutil.copyfile(os.path.join(folderin,name),
                        os.path.join(dir,name))

    binids = [key for key in fobinssorted]
    chi2 = [fobinssorted[key][0] for key in fobinssorted]

    jsout = {'binids':binids,'chi^2':chi2}
    with open(os.path.join(folderout,"chi2sortedbins.json"), 'w') as f:
        json.dump(jsout,f,indent=4)






if __name__ == "__main__":
    indir = sys.argv[1]
    outdir = sys.argv[2]
    if outdir == "outlier":
        makeoutlierfiltereddata(indir)
    elif outdir == "findhypofilteredoutinorder":
        findhypofilteredoutinorder(indir)
    else:
        makehypothesisfiltereddata(indir,outdir)




