#! /usr/bin/env python
import numpy as np
import os,sys

def getCalibrationErrorCovariance(args):
    apprfile = os.path.join(args.INDIR, "approximation.json")
    datafile = os.path.join(args.INDIR, "experimental_data.json")
    wtfile = os.path.join(args.INDIR, "weights")

    from apprentice.tools import TuningObjective
    from apprentice import tools
    IO = TuningObjective(wtfile, datafile, apprfile,filter_envelope=False,
                             filter_hypothesis=False)

    CE_COV = {}
    newdata = {}
    for hname in IO._hnames:
        obsbin = IO.obsBins(hname)
        simdata = {}
        for bno,b in enumerate(obsbin):
            binid = IO._binids[b]
            simfile = os.path.join(args.SIMDIR,binid.replace('/','_'))
            if not os.path.exists(simfile):
                print("Simulation file not found: {}".format(simfile))
                sys.exit(1)
            X, Y = tools.readData(simfile)
            if bno == 0:
                simdata["X"] = X
                simdata["Y"] = np.zeros((len(Y),len(obsbin)),dtype=np.float)
            for yno,y in enumerate(Y):
                simdata['Y'][yno][bno] = y

        MAT = np.zeros((len(simdata["X"]),len(obsbin)),dtype=np.float)
        for xno, p in enumerate(simdata["X"]):
            for bno, b in enumerate(obsbin):
                fval = IO.calc_f_val(x=p, sel=[b])
                # caliberror = np.abs(fval - simdata["Y"][xno])
                caliberror = fval - simdata["Y"][xno][bno]
                MAT[xno][bno] = caliberror
        cov =  np.cov(np.array(MAT).T)
        CE_COV[hname] = cov.tolist()
        for bno, b in enumerate(obsbin):
            newdata[IO._binids[b]] = [IO._Y[b],np.sqrt(cov[bno][bno])]

    import json
    with open(datafile,'r') as f:
        olddata = json.load(f)
    for key in olddata:
        if key not in newdata:
            newdata[key] = olddata[key]

    newdatafile = os.path.join(args.INDIR, "experimental_data_cenoise.json")

    with open(newdatafile,'w') as f:
        json.dump(newdata,f)

    os.makedirs(args.OUTDIR, exist_ok=True)
    covmatfile = os.path.join(args.OUTDIR, "caliberrorcovariance.json")
    with open(covmatfile,'w') as f:
        json.dump(CE_COV,f)

def plotcovariance(args):
    apprfile = os.path.join(args.INDIR, "approximation.json")
    oedatafile = os.path.join(args.INDIR, "experimental_data.json")
    cedatafile = os.path.join(args.INDIR, "experimental_data_cenoise.json")
    wtfile = os.path.join(args.INDIR, "weights")

    from apprentice.tools import TuningObjective

    import json
    with open(args.COVFILE,'r') as f:
        covmats = json.load(f)
    if args.RESULTFILE is not None:
        with open(args.RESULTFILE, 'r') as f:
            rdata = json.load(f)
        wts = rdata['chain-0']["X_outer"][0]
        print(rdata['chain-0']["Y_outer"])
        snorm = sum(np.array(wts)**2)
        print(snorm)
        print(rdata['chain-0']["Y_outer"] + rdata['chain-0']['lambda'] * snorm)
        if args.ONLYSTATS:
            exit(0)
    else:
        wts = None
    import matplotlib.pyplot as plt
    import matplotlib.cm as cm

    IO = TuningObjective(wtfile, cedatafile, apprfile, filter_envelope=False,
                         filter_hypothesis=False)
    outdir = os.path.join(args.OUTDIR, "COVMAT", 'caliberror')
    os.makedirs(outdir, exist_ok=True)
    for hno, hname in enumerate(IO._hnames):
        mat = covmats[hname]

        # When only diagonal matrix is to be plotted
        full=False
        A = []
        for i in range(len(mat)):
            A.append(mat[i][i])
        newmat = np.zeros((len(A),len(A)))
        np.fill_diagonal(newmat, A)
        cmap = cm.get_cmap('gray_r', 10)
        plt.imshow(newmat, interpolation='nearest',
                              # cmap=cm.Greys_r)
                              cmap=cmap)
        if full:
            plt.imshow(mat, interpolation='nearest',
                       # cmap=cm.Greys_r)
                       cmap=cm.viridis)
        title = hname
        title += " ({:.2e})".format(wts[hno]) if wts is not None else ""

        plt.title(title)
        plt.colorbar()
        plt.savefig(os.path.join(outdir, hname.replace('/', '_') + ".pdf"))
        plt.close('all')

    # Create a single plot with calib error vs observation error
    outdir = os.path.join(args.OUTDIR, "COMPARE")
    os.makedirs(outdir, exist_ok=True)
    with open(oedatafile, 'r') as f:
        obserrordata = json.load(f)
    with open(cedatafile, 'r') as f:
        caliberrordata = json.load(f)
    for hname in IO._hnames:
        obsbin = IO.obsBins(hname)
        caliberrorY = []
        obserrorY = []
        for b in obsbin:
            binid = IO._binids[b]
            caliberrorY.append(caliberrordata[binid][1]**2)
            obserrorY.append(obserrordata[binid][1]**2)
        X = range(len(obsbin))
        plt.ylim((10**-15, 10**3))
        plt.plot(X, caliberrorY, label="Caliberation Error Variance")
        plt.plot(X, obserrorY, label="Observation Error Variance")
        plt.yscale('log')
        plt.legend()
        plt.title(hname)
        plt.savefig(os.path.join(outdir, hname.replace('/', '_') + ".pdf"))
        plt.close('all')




if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Calibration Error Covariance',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir")
    parser.add_argument("-s", "--simdatadir", dest="SIMDIR", type=str, default=None,
                        help="Simulation data dir")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")

    parser.add_argument('-c', "--covariancefile", dest="COVFILE", type=str, default=None,
                        help="Covariance file")
    parser.add_argument('-r', "--resultfile", dest="RESULTFILE", type=str, default=None,
                        help="Result file")
    parser.add_argument("--onlyplot", dest="ONLYPLOT", default=False, action="store_true",
                        help="Only plot covariance. Use options -i, -c, -r and -o")
    parser.add_argument("--onlystats", dest="ONLYSTATS", default=False, action="store_true",
                        help="Only print stats")



    args = parser.parse_args()
    print(args)
    if args.ONLYPLOT:
        plotcovariance(args)
    else:
        getCalibrationErrorCovariance(args)





