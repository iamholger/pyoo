import numpy as np
def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json

    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def is_pareto_efficient(costs):
    """
    Find the pareto-efficient points
    :param costs: An (n_points, n_costs) array
    :return: A (n_points, ) boolean array, indicating whether each point is Pareto efficient
    """
    is_efficient = np.ones(costs.shape[0], dtype = bool)
    for i, c in enumerate(costs):
        is_efficient[i] = np.all(np.any(costs[:i]>c, axis=1)) and np.all(np.any(costs[i+1:]>c, axis=1))
    return is_efficient

def findCornerTriangulation(pareto, data, txt,logx,logy):
    def angle(B, A, C):
        """
        Find angle at A
        On the L, B is north of A and C is east of A
        B
        | \
        A - C
        """

        ab = [A[0]-B[0], A[1]-B[1]]
        ac = [A[0]-C[0], A[1]-C[1]]

        l1=np.linalg.norm(ac)
        l2=np.linalg.norm(ab)
        import math
        return math.acos(np.dot(ab,ac)/l1/l2)


    def area(B, A, C):
        """
        A,B,C ---
        On the L, B is north of A and C is east of A
        B
        | \     (area is +ve)
        A - C
        """
        # return 0.5 *( ( C[0] - A[0] )*(A[1] - B[1]) -  (A[0] - B[0])*(C[1] - A[1]) )
        return 0.5 *( (B[0]*A[1] - A[0]*B[1]) - (B[0]*C[1]-C[0]*B[1]) + (A[0]*C[1]-C[0]*A[1]))

    cte=np.cos(7./8*np.pi)
    cosmax=-2
    corner=len(pareto)-1
    # print(angle([-0.625, 1.9],[0,0],[4,0]))
    # exit(1)


    if(logx):
        xp = np.log10(pareto[:,0])
        xd = np.log10(data[:,0])
    else:
        xp = (pareto[:,0])
        xd = (data[:,0])
    if(logy):
        yp = np.log10(pareto[:,1])
        yd = np.log10(data[:,1])
    else:
        yp = (pareto[:,1])
        yd = (data[:,1])
    lpareto = np.column_stack((xp,yp))
    ldata = np.column_stack((xd,yd))

    C = lpareto[corner]
    for k in range(0,len(lpareto)-2):
        B = lpareto[k]
        for j in range(k,len(lpareto)-2):
            A = lpareto[j+1]
            _area = area(B,A,C)
            _angle = angle(B,A,C)
            # degB,pdegB,qdegB = getdegreestr(B,ldata,txt)
            # degA,pdegA,qdegA = getdegreestr(A,ldata,txt)
            # degC,pdegC,qdegC = getdegreestr(C,ldata,txt)
            # print("print all %s %s %s %f %f %f"%(degB,degA,degC, _area,_angle,_angle*(180/np.pi)))

            # if(_area > 0):
            #     degB,pdegB,qdegB = getdegreestr(B,ldata,txt)
            #     degA,pdegA,qdegA = getdegreestr(A,ldata,txt)
            #     degC,pdegC,qdegC = getdegreestr(C,ldata,txt)
            #     print("area > 0 ------>area = %.4f angls = %.4f at %s %s %s"%(_area,_angle, degB, degA, degC))


            if _angle > cte and _angle > cosmax and _area > 0:
                corner = j + 1
                cosmax = _angle
                # degB,pdegB,qdegB = getdegreestr(B,ldata,txt)
                # degA,pdegA,qdegA = getdegreestr(A,ldata,txt)
                # degC,pdegC,qdegC = getdegreestr(C,ldata,txt)
                # print("===== Found corner ====== %f %f at %s %s %s"%(_area,_angle, degB, degA, degC))
    print("In findCornerTriangulation, I got this {}".format(pareto[corner]))
    return corner

def mkPlot(data, outfn, orders=None,lx="$x$", ly="$y$", logy=True, logx=True, normalize_data=True,jsdump={},title=""):
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    plt.clf()
    mpl.rc('text', usetex = False)
    mpl.rc('font', family = 'serif', size=12)
    mpl.style.use("ggplot")

    plt.figure(figsize=(8, 8))
    plt.xlabel(lx)
    plt.ylabel(ly)


    data=np.array(data)
    # if normalize_data: data = data / data.max(axis=0)

    b_pareto = is_pareto_efficient(data)
    _pareto = data[b_pareto] # Pareto points

    pareto = _pareto[_pareto[:,0].argsort()]# Pareto points, sorted in x
    print("===========")
    print(pareto)
    print("===========")
    c, txt   = [], []
    paretoorders = []
    for num, m in enumerate(orders):
        if b_pareto[num]:
            c.append("g")
        else:
            c.append("r")

        if b_pareto[num]:
            txt.append("({})".format(m))
            paretoorders.append(m)
        else:
            txt.append("")

    print("===========")
    for i in range(len(pareto)):
        print("{} {} {}".format(pareto[i][0], pareto[i][1],paretoorders[i]))
    print("===========")
    # cornerT = findCornerTriangulation(pareto,data,txt,logx,logy)
    # cornerT2 = findCornerTriangulation2(pareto)
    # cornerdSl = findCornerSlopesRatios(pareto)
    #
    # plt.ylim(min(data[:, 1]) , max(data[:, 1]) )
    if logx: plt.xscale("log")
    if logy: plt.yscale("log")
    # plt.xlim(10**-15,10**-14)
    # plt.ylim(10**2, 10**3)

    plt.scatter(pareto[:,0]  , pareto[:,1]  , marker = 'o', c = "silver"  ,s=100, alpha = 1.0)
    # plt.scatter(pareto[cornerdSl,0]  , pareto[cornerdSl,1]  , marker = '+', c = "peru"  ,s=777, alpha = 1.0)


    for num, d in enumerate(pareto): plt.scatter(d[0], d[1],c='g')
    # for num, t in enumerate(txt): plt.annotate(t, (data[num][0], data[num][1]))
    for num, t in enumerate(paretoorders): plt.annotate(t, (pareto[num,0]  , pareto[num,1]))

    # plt.scatter(pareto[cornerT, 0], pareto[cornerT, 1], marker='*', c="gold", s=444, alpha=1.0)
    # print("The corner point is at {}".format(orders[cornerT]))
    # plt.show()
    plt.savefig(outfn)
    plt.close('all')

def dolcurve(args):
    import numpy as np
    XX = {} #residual norm
    YY = {} #solution norm
    SS = []
    for tnum,t in enumerate(args.FILES):
        import json
        with open (t,"r") as f:
            data = json.load(f)
        youter = data['chain-0']['Y_outer']
        xouter = np.array(data['chain-0']['X_outer'][0])
        ll = data['chain-0']['lambda']
        sn = sum(xouter)
        # rn = (youter - ll * sn)
        sn = data['chain-0']['solutionnorm']
        rn = data['chain-0']['residualnorm']
        YY[ll] = sn
        XX[ll] = rn
        SS.append(ll)
    import matplotlib as mpl
    SS = np.sort(SS)
    SSstr = [repr(s) for s in SS]
    # SS = SS[::-1]
    X = [XX[key] for key in SS]
    Y = [YY[key] for key in SS]
    import matplotlib.pyplot as plt
    plt.plot(X,Y)

    print(np.c_[SS,X,Y])
    print(X,Y)
    xlab = "$\psi(w) = $Tr $(\Gamma_{post}(w))$"
    ylab = "$e^Tw$"
    plt.xlabel(xlab)
    plt.ylabel("$e^Tw$")
    plt.yscale('log')
    plt.xscale('log')
    plt.ylim(0, 420)
    # plt.xlim(-0.000005, 0.0015)
    for x,y,s in zip(X,Y,SSstr):
        plt.text(x, y, s, fontsize=12)
    import os
    os.makedirs(args.OUTDIR,exist_ok=True)
    # plt.show()
    # plt.savefig(os.path.join(args.OUTDIR,args.OFILEPREFIX+"_lcurve.pdf"))

    plt.close('all')
    outfilepareton = os.path.join(args.OUTDIR,args.OFILEPREFIX+"_pareto_lcurve.pdf")
    mkPlot([(a, b) for a, b in zip(X, Y)], outfilepareton,
           SSstr, ly=ylab, lx=xlab, title="",
           logy=False, logx=False, normalize_data=False)
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Plot L curve',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix")
    parser.add_argument("-r", "--resultfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Filenames of runs for different values of lambda")


    args = parser.parse_args()

    print(args)

    dolcurve(args)




