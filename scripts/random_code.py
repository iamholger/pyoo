import autograd as ag
import time

Hag = ag.hessian(OO.objective)
W = np.sqrt(OO._W2)
print(Hag(np.ones(3)))
print(OO._H(W,np.ones(3)))

points = [np.random.rand(3) for _ in range(10)]

t = time.time()
for p in points:
    Hag(p)
print(time.time()-t)
t = time.time()
for p in points:
    OO._H(W,p)
print(time.time()-t)
