import os
import numpy as np
if __name__ == "__main__":
    folder = "../data/A14"

    apprfile = os.path.join(folder,"approximation.json")
    datafile = os.path.join(folder,"experimental_data.json")
    wtfile = os.path.join(folder,"weights")

    import pyoo.outer_objective as oobj
    OO = oobj.OuterObjective(wtfile, datafile, apprfile, filter_envelope=False,
                                 filter_hypothesis=False,metric_base='WFIM_with_grad',
                                 restart_filter=100, debug=False, OO_uses_sym_cache=True)

    p = [1,1,1,0.5,1,1,1,0.5,1,1]
    allw = [1.]*len(OO.hnames)
    for hno, h in enumerate(OO.hnames):
        obsbins = OO.obsBins(h)
        w = allw[hno]
        FIM = sum(OO._WFIM_bi(i, p, [1.]) for i in obsbins)

        F = [OO.EMG[bnum].tolist() for bnum in obsbins]
        F = np.array(F)
        # print(F)

        C = np.array(1 / OO._E2[obsbins])
        D = np.zeros((len(C),len(C)))
        np.fill_diagonal(D, C)

        W = np.zeros((len(C),len(C)))
        np.fill_diagonal(W, w)
        FIM2 = np.matmul(np.matmul(np.matmul(np.matrix.transpose(F),W),np.linalg.inv(D)),F)
        print("{} : {}".format(h, np.allclose(FIM,FIM2)))

        # np.matmul(np.matmul(weights * np.matrix.transpose(SM), np.linalg.inv(C)), SM)





