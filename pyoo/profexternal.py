



class ProfObj(object):
    """
    Base class for Professor things.
    """
    def __init__(self, execPath, **kwargs):
        self._proftune =  execPath
        self._ncalls = 0
        self._clargs = []
        self._debug      = True                       if "debug"     in kwargs.keys() else False
        self._ifile      = kwargs["ifile"]            if "ifile"     in kwargs.keys() else None
        self._scan       = kwargs["scan"]            if "scan"     in kwargs.keys() else 10
        self._datadir    = kwargs["datadir"]          if "datadir"   in kwargs.keys() else None
        self._wtemplate  = kwargs["wtemplate"]        if "wtemplate" in kwargs.keys() else None
        self._prefix     = kwargs["prefix"]           if "prefix"    in kwargs.keys() else "workDir"
        self._riskAv     = float(kwargs["riskAv"])    if "riskAv"    in kwargs.keys() else 5.
        self._cov = None
        self._meancont = None

    def setP(self, P):
        self._params=P

    @property
    def P(self):
        return self._params

    @property
    def debug(self):
        return self._debug

    @property
    def nCalls(self):
        return self._ncalls

    @property
    def cov(self):
        return self._cov

    @property
    def invcov(self):
        import numpy as np
        return np.linalg.inv(self._cov)

    @property
    def invcovtrace(self):
        import numpy as np
        return np.trace(self.invcov)

    @property
    def invcovdet(self):
        import numpy as np
        return np.linalg.det(self.invcov)

    @property
    def meancont(self):
        temp = dict(self._meancont)
        return [temp[k] for k in sorted(temp.keys())]

    @property
    def riskAv(self):
        return self._riskAv

    @property
    def portfolio(self):
        cts = self.meancont
        import numpy as np
        m = np.mean(cts)
	s = np.var(cts)
	ll = self.riskAv # parameter describing risk aversion
	return m + ll*s  # only mean, only std, dependence on ...

    def mkOutdir(self, mkdir=False):
        import os
        out = os.path.join(self._prefix, "step-%s"%str(self.nCalls).zfill(4))
        if mkdir:
            if not os.path.exists(out): os.makedirs(out)
        return out

    @property
    def dimWeights(self):
        """
        Read in weights file and determine number of non-zero weights
        """
        _d = 0
        with open(self._wtemplate, "r") as f:
            for line in f:
                l=line.strip()
                if len(l) == 0 or l.startswith("#"): continue
                _split = l.split()
                if len(_split)>1:
                    if float(_split[1]) == 0: continue
                _d+=1
        return _d

    @property
    def mkWeightFile(self):
        """
        Read in a valid weight file, sort, fill and write to outputdir.
        We ignore commented out lines, empty lines and observables with zero weight.

        The items are sorted alphabetically and a weight file is written into
        the respective output directory using the current params as weights.
        """
        temp = []
        with open(self._wtemplate, "r") as f:
            for line in f:
                l=line.strip()
                if len(l) == 0 or l.startswith("#"): continue
                _split = l.split()
                if len(_split)>1:
                    if float(_split[1]) == 0: continue
                temp.append( _split[0] )
        if len(temp)!=len(self.P):
            raise Exception("Error: the number of observables in weight file does not match number of parameters, exiting!!!")

        temp.sort()

        import os
        wout = os.path.join(self.mkOutdir(), "weights")
        with open(wout, "w") as f:
            for num, obs in enumerate(temp):
                f.write("%s\t%f\n"%(obs, self.P[num]))
        return wout



    @property
    def mkCmd(self):
        """
        Create a command line array suitable for subprocess.
        """
        cmd = [self._proftune, "--norm-weights"]
        cmd.append(self._ifile)
        cmd.extend(["-d", self._datadir])
        cmd.extend(["-o", self.mkOutdir(mkdir=True)])
        wfile = self.mkWeightFile
        cmd.extend(["-w", wfile])
        cmd.extend(["--scan-n", str(self._scan)])
        if self._debug:
            print(" ".join(cmd))
        return cmd

    def readProfResult(self, resfile=None):
        def getParamCov(TXT):
            """
               Read the covariance matrix from the lines, return as numpy array
               N.b. copied from professor/minimize.py
            """
            START = TXT.index("# Covariance matrix:") + 2
            dim = len(TXT[START].strip().split()) - 2
            END = START+dim
            COV_raw = TXT[START:END]
            COV_txt = [COV_raw[d].split()[2:2+dim] for d in xrange(dim)]

            # Go through line by line and find fixed params
            fixed=[]
            for num, c in enumerate(COV_txt):
                if all([x=='---' for x in c]):
                    fixed.append(num)

            # This is the reduced cov matrix
            COV_l = []
            for num, c in enumerate(COV_txt):
                if not num in fixed:
                    cline = [float(x) for num2, x in enumerate(c) if not num2 in fixed]
                    COV_l.append(cline)

            # Cast it into a numpy array
            from numpy import zeros
            COV_p = zeros((dim-len(fixed), dim-len(fixed)))
            for i in xrange(dim-len(fixed)):
                for j in xrange(dim-len(fixed)):
                    COV_p[i][j] = COV_l[i][j]
            return COV_p

        def getMeanContrib(TXT):
            """
               Get the mean unbiased contribution of each obs to chi2
            """
            START = TXT.index("# Mean contribution:") + 2
            CTB_raw = TXT[START:]
            CTB = []
            for c in CTB_raw:
                _split = c.split()
                CTB.append((_split[1], float(_split[2])))
            return CTB


        import os
        if resfile is None:
            rfile = os.path.join(self.mkOutdir(), "results.txt")
        else:
            rfile = os.path.join(resfile)
        with open(rfile) as f:
            L = [l.strip() for l in f]

        _COV = getParamCov(L)
        _CTB = getMeanContrib(L)
        self._cov = _COV
        self._meancont = _CTB


    def run(self):
        import subprocess
        process = subprocess.Popen(" ".join(self.mkCmd), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        output = process.communicate()
        rc = process.returncode
        # Return code handling
        if rc != 0:
            raise Exception("\n\nprof2-tune command returned an error:\n\n %s\n\nSTDOUT was: %s"%(output[1], output[0]))
        self.readProfResult()

        self._ncalls+=1

    @property
    def report(self):
        """
        Return a dictionary of the evaluated objectives
        """
        obj = {
                "trace"   : self.invcovtrace,
                "det"     : self.invcovdet,
                "meancont": self.meancont,
                "portfolio": self.portfolio
                }
        return obj

    def __call__(self, P):
        self.setP(P)
        rep = {
                "trace"   :  None,
                "det"     :  None,
                "meancont":  None,
                "portfolio": None
                }
        try:
            self.run()
            rep=self.report
        except:
            # I know this is ugly
            print("Try-excepot on __call__ of ProfObj")
            pass


        return rep

if __name__=="__main__":
    import os, sys
    P=ProfObj(sys.argv[1], ifile=sys.argv[2], datadir=sys.argv[3], wtemplate=sys.argv[4])
    try:
        r = P([0.2,0.2,0.2,0.4])
        print "det(C^-1):",  r["det"]
        print "tr(C^-1):",   r["trace"]
        print "mean errors:", r["meancont"]
    except Exception, e:
        print("An error has occured:\n")
        print e

    sys.exit(0)
