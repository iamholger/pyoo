#!/usr/bin/env python

import numpy as np
import pyoo as oo
import apprentice as app

def obsBins(self, hname):
    return [i for i, item in enumerate(self._binids) if item.startswith(hname)]

def meanScore(self, p, SEL, NB):
    if self._EAS is not None:
        inv_err2 = 1./ (   self._EAS.vals(p)**2 + 1./self._E2  )
    else:
        inv_err2 = self._E2
    D = self._AS.vals(p) - self._Y
    obswise_gof = np.empty(len(SEL), dtype=np.float)
    for onum, sel in enumerate(SEL):
        obswise_gof[onum] = np.sum(D[sel]*D[sel] * inv_err2[sel] - np.log(inv_err2[sel]))
    obswise_gof /= NB
    return np.sum(obswise_gof)

def meanScoreGradient(self, p, SEL, NB):
    if self._EAS is not None:
        inv_err2 = 1./ (   self._EAS.vals(p)**2 + 1./self._E2  )
        apperr = self._EAS.vals(p)
    else:
        inv_err2 = self._E2
        apperr = np.zeros_like(inv_err2)
    D = self._AS.vals(p) - self._Y

    valgrads = self._AS.grads(p)
    if self._EAS is not None:
        errgrads = self._EAS.grads(p)
    else:
        errgrads = np.zeros_like(valgrads)

    # Derivative of  gradient w.r.t. observable weight
    mixmax = np.zeros((len(SEL), len(p)))
    for num, sel in enumerate(SEL):
        # mixmax[num] = 2*self.gradient(p, sel=sel)/np.sqrt(self._W2[sel][0]) # all weighs are the same
        mixmax[num] = self.gradient(p, sel=sel)/self._W2[sel][0] # all weighs are the same


    seltot = [i for sublist in SEL for i in sublist]

     # Concatenate sels here
    HESS = self.hessian(p, sel=seltot)
    from numpy import linalg
    HINV = linalg.inv(HESS)

    # Observable wise gradient
    dSOdp = np.zeros_like(p)
    for num, sel in enumerate(SEL):
        dSOdp += np.sum(2*D[sel] * inv_err2[sel] * valgrads[sel].T, axis=1)
        dSOdp -= np.sum( D[sel] * D[sel] * inv_err2[sel] * inv_err2[sel] * 2 * apperr[sel] * errgrads[sel].T, axis=1)
        dSOdp += np.sum(inv_err2[sel] * 2 * apperr[sel] * errgrads[sel].T, axis=1)
        dSOdp /= NB[num]

    return  -1* dSOdp @ HINV @ mixmax.T

def ooeval(self, w, hnames, SEL, NB, nstart=1, nrestart=1, algorithm="tnc", tol=1e-6, nocheck=True):
    from collections import OrderedDict
    wd = OrderedDict()
    for num, hn in enumerate(hnames): wd[hn]=w[num]
    self.setWeights(wd)
    res = self.minimizeMPI(nstart, nrestart, method=algorithm, tol=tol, saddlePointCheck=False)
    phat = res.x
    chi2 = self.objective(res.x, unbiased=True)
    ndf = self.ndf


    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    # print("[{}] {}".format(rank, res.x))
    import sys
    sys.stdout.flush()
    # s="#"
    # for num, hn in enumerate(hnames): s+= " W{}".format(num+1)
    # for num, p  in enumerate(phat):   s+= " P{}".format(num+1)
    # s+= " InnerObjective OuterObjective\n"
    s=""
    for x in w:     s+= "{},".format(x)
    for x in phat: s+= "{},".format(x)
    s+= "{},".format(chi2/ndf)
    # print("Objective value at best fit point: %.2f (%.2f without weights)"%(res.fun, chi2))
    # print("Degrees of freedom: {}".format(ndf))
    # print("phi2/ndf: %.3f"%(chi2/ndf))

    # print("Inner obj: {}  sumW: {}".format(res.fun, sum(w)))
    # print(self.objective.printParams(phat))
    obj, grad = meanScore(self, phat, SEL, NB), meanScoreGradient(self, phat, SEL, NB)
    s+="{}\n".format(obj)
    import sys
    # sys.stderr.write(s)
    # print(s)
    return obj, grad

def runLineScan(self, nstart=1, nrestart=1, startPoint=None, algorithm="tnc", tol=1e-6, nocheck=True, seed=1234):
    np.random.seed(seed)

    hnames = sorted(list(set(self._hnames)))
    SEL = [obsBins(self, hn) for hn in hnames]
    NB = np.array([len(sel) for sel in SEL])

    X = np.linspace(1e-6,1-1e-6,25)
    Y = 1-X

    import sys

    for x, y in zip(X,Y):
        w=[x,y]
        res = ooeval(self, w, hnames, SEL, NB, nstart=nstart, nrestart=nrestart,tol=tol, algorithm=algorithm,nocheck=nocheck)
        sys.stderr.write("{} {}\n".format(x, res[0]))

def runSLSQP(self, nstart=1, nrestart=1, startPoint=None, algorithm="tnc", tol=1e-6, otol=1e-3, nocheck=True, seed=1234):

    hnames = sorted(list(set(self._hnames)))
    SEL = [obsBins(self, hn) for hn in hnames]
    NB = np.array([len(sel) for sel in SEL])

    box = np.zeros((len(SEL), 2))
    box[:,1]=1
    box[:,0]=1e-6

    cons = np.empty(0, "object")
    cons = np.append(cons, {'type': 'eq',   'fun':lambda x: sum(x)-1.})

    # x0=np.ones(len(SEL))
    # x0/=len(SEL)
    np.random.seed(seed)
    X0 = np.random.dirichlet(np.ones(len(SEL)), 1)
    # X0 = [[0.05, 0.95]]
    FVAL=1e99
    retres=None

    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    from scipy import optimize
    for x0 in X0:
        if rank==0: print("[{}] start at {} (seed={})".format(rank, x0, seed))
        res = optimize.minimize(lambda w:ooeval(self, w, hnames, SEL, NB, nstart=nstart, nrestart=nrestart,tol=tol, algorithm=algorithm,nocheck=nocheck), x0, tol=otol, bounds=box, constraints=cons, jac=True, method="SLSQP", options={"disp":True if rank==0 else False, 'iprint':2})
        if res.fun < FVAL:
            FVAL=res.fun
            retres=res


    # s="#"
    # for num, hn in enumerate(hnames): s+= " W{}".format(num+1)
    # for num, p  in enumerate(self.pnames):   s+= " P{}".format(num+1)
    # s+= " InnerObjective OuterObjective\n"
    # import sys
    # sys.stderr.write(s)
    # res = optimize.minimize(lambda w:ooeval(self, w, hnames, SEL, NB, nstart=nstart, nrestart=nrestart,tol=tol, algorithm=algorithm,nocheck=nocheck, seed=seed), x0, bounds=box, constraints=cons, method="SLSQP", options={"disp":True, 'iprint':3})
    if rank==0: print("[{}] Best res at {} with {}".format(rank, res.x, res.fun))
    import sys
    sys.stdout.flush()
    return retres


if __name__ == "__main__":
    import optparse, os, sys

    op = optparse.OptionParser(usage=__doc__)
    op.add_option("-v", "--debug", dest="DEBUG", action="store_true", default=False,
                  help="Turn on some debug messages")
    op.add_option("-q", "--quiet", dest="QUIET", action="store_true", default=False,
                  help="Turn off messages")
    op.add_option("-o", "--output", dest="OUTPUT",default="optimresults.json",
                  help="Output file (Default: %default)")
    op.add_option("--seed", dest="SEED", type=int, default=1234,
                  help="The base random seed (Default: %default)")
    op.add_option("-D", "--design", dest="DESIGNSIZE", type=int, default=-1,
                  help="The size of the initial design, by default alll available data is used (Default: %default)")
    op.add_option("-n", "--niterations", dest="NITERATIONS", default=100, type=int,
                  help="The number of iterations (Default: %default)")
    op.add_option("-c", "--cycling", dest="CYCLING", default="steps", type=str,
                  help="Cycling of rbf weight --- steps|random (Default: %default)")
    op.add_option("-a", "--algorithm", dest="ALG", default="tnc", help="The minimisation algrithm tnc, ncg, lbfgsb, trust (default: %default)")
    op.add_option("--tol", dest="TOL", default=1e-6, type=float, help="Tolerance for scipy optimize minimize (default: %default)")
    op.add_option("--0tol", dest="OTOL", default=1e-3, type=float, help="Tolerance for slsqp (default: %default)")
    op.add_option("--no-check", dest="NOCHECK", default=False, action="store_true", help="Don't check for sadlepoints (default: %default)")
    op.add_option("-e", "--errorapprox", dest="ERRAPP", default=None, help="Approximations of bin uncertainties (default: %default)")
    op.add_option("-s", "--survey", dest="SURVEY", default=1000, type=int, help="Size of survey when determining start point (default: %default)")
    op.add_option("-r", "--restart", dest="RESTART", default=1, type=int, help="Minimiser restarts (default: %default)")
    opts, args = op.parse_args()


    WFILE = args[0]
    DATA  = args[1]
    APP   = args[2]
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    import time, datetime
    if rank==0:
        print("Initialise inner optimisation data on rank 0")
        sys.stdout.flush()
        t0=time.time()
        GOF = app.appset.TuningObjective2(WFILE, DATA, APP, f_errors=opts.ERRAPP, debug=opts.DEBUG)
        t1=time.time()
        print("Init took %.2f s"%(t1-t0))
        sys.stdout.flush()
    else:
        GOF = None

    t0=time.time()
    GOF = comm.bcast(GOF, root=0)
    t1=time.time()
    if rank ==0 :  print("Broadcast took %.2f s"%(t1-t0))
    sys.stdout.flush()

    t0=time.time()
    t0stamp = datetime.datetime.fromtimestamp(t0)
    if rank ==0 :  print("Start outer optimisation at {}".format(t0stamp.strftime('%Y-%m-%d %H:%M:%S')))
    sys.stdout.flush()
    res = runSLSQP(GOF, opts.SURVEY, opts.RESTART, startPoint=None, tol=opts.TOL, algorithm=opts.ALG, nocheck=not opts.NOCHECK, seed=opts.SEED, otol=opts.OTOL)
    # runLineScan(GOF, opts.SURVEY, opts.RESTART, startPoint=None, tol=opts.TOL, algorithm=opts.ALG, nocheck=not opts.NOCHECK, seed=opts.SEED+rank)
    # exit(1)

    t1=time.time()
    if rank ==0 : print("Run on rank 0 took %.2f s"%(t1-t0))
    sys.stdout.flush()

    if rank==0:
        import json

        hnames = sorted(list(set(GOF._hnames)))
        rd, wo = {}, {}
        for hn, w in zip(hnames, res.x):
            wo[hn] = w
        rd["x"] = wo
        rd["y"] = res.fun
        with open(opts.OUTPUT, "w") as f:
            json.dump(rd, f, indent=4)

    exit(0)

    # if rank==0:
        # hnames = sorted(list(set(GOF._hnames)))
        # pnames = ["P%i"%i for i in range(GOF.dim)]
        # binids = GOF._binids,
    # else:
        # hnames, pnames, binids = None, None, None

    # hnames = comm.bcast(hnames, root=0)
    # pnames = comm.bcast(pnames, root=0)
    # binids = comm.bcast(binids, root=0)

    data   = [p.tolist() for p in SO._xall]
    RR = {
        "X_outer": data,  # parameters of outer objective per iteration
        "X_inner": [x.tolist() for x in SO._innerXMin],
        # parameters of inner objective per iteration (i.e. tuned parameters given weights)
        "Y_outer": SO._yall,  # Outer objective
        "Y_inner": [x.tolist() for x in SO._innerYMin],
        # "n0": len(SO._ytrain),  # The trainingsize
        # "pnames_outer": hnames,  # The parameter names of the oo -- i.e. histogram names
        # "pnames_inner": pnames,
        # "binids": binids,
    }


    allres = comm.gather(RR, root=0)
    if rank==0:
        allData={}
        for num, r in enumerate(allres):
            allData["chain-{}".format(num)] = r

        import json
        with open(opts.OUTPUT, "w") as f:
            json.dump(allData, f, indent=4)
        print("Output written to {}".format(opts.OUTPUT))
    comm.barrier()
    sys.exit(0)
