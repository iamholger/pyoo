#! /usr/bin/env python

"""\
%prog -d <refdir> [<ipolfiles>=ipol.dat ...] [-r <runsdir>=<refdir>/../mc] [opts]

Use the interpolations stored in <ipolfile> to find optimised parameters with
the reference histograms found in the <refdir> as the optimisation target.

The <runsdir> is used to calculate the maximum error value seen for each bin,
to regularise interpolated errors which could otherwise blow up, leading to
an unrepresentative small chi2 (and hence fit result) outside the sampled ranges.


WEIGHT FILE SYNTAX:

The weight file syntax is derived from YODA path syntax, and allows selecting bin
ranges either by physical value or by bin number, e.g.

  /path/parts/to/histo            weight
  /path/parts/to/histo#n          weight
  /path/parts/to/histo@x          weight
  /path/parts/to/histo@xmin:xmax  weight
  /path/parts/to/histo#nmin:nmax  weight

Blank lines and lines starting with a # symbol will be ignored.

The bin indices used with the # syntax start at 0, and the end index in a
range is non-inclusive. In the range form, if xmin/nmin or xmax/nmax is left
blank, it defaults to the accepting all bins from the start of the histogram,
or all bins to the end of the histogram respectively.

TODO:
 * Include correlations in the tuning and resampling.
 * Handle run combination file/string (write a hash of the run list into the ipol filename?)
"""
def timeit(method):
    import time
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print '%r  %2.2f ms' % \
                  (method.__name__, (te - ts) * 1000)
        return result
    return timed

import professor2 as prof
import numpy as np

class ProfOOTune(object):
    def __init__(self, ifile, datadir, wtemplate, debug=False, nscan=100, strategy=2, risk=1):

        # Read in ipolhistos once
        self._IHISTOS, self._METADATA = prof.read_ipoldata(ifile)
        # Read in data histos once and throw away those without ipol equivalent
        self._DHISTOS =  {k:v for k, v in prof.read_all_histos(datadir).iteritems() if k in self._IHISTOS.keys()}

        self._wtemplate = wtemplate
        self.riskAv=risk

        self.STRATEGY=strategy
        # NOTE: there is more cleaning happening when setWeights is called

        self.setMeta()
        self._nscan=nscan

        globname = "gof"
        fname="self.trivialGoF"
        extraargs=[]
        fargs = ["A%03i" % i for i in xrange(len(self._PNAMES))]
        self.funcdef = "def {gname}({fargs}):\n    x= lambda {fargs}: {fname}([{fargs}], {extrargs})".format(gname=globname, fargs=", ".join(fargs), fname=fname, extrargs=", ".join(extraargs))
        self.funcdef+="\n    return x({fargs})".format(fargs=", ".join(fargs))

    def setMeta(self):
        self._PNAMES = self._METADATA["ParamNames"].split()
        if not self._PNAMES:
            self._PNAMES = ["A%03i" % i for i in xrange(int(self._METADATA["Dimension"]))]
        self._PMIN   = [float(x) for x in self._METADATA["MinParamVals"].split()]
        self._PMAX   = [float(x) for x in self._METADATA["MaxParamVals"].split()]

    def setWeights(self, wdict):
        ## Find objects available in both ipol and ref data (and in matchers if not None)
        matchers = {prof.PointMatcher(k) : float(v) for k, v in wdict.iteritems()}
        available = []
        for ihn in sorted(self._IHISTOS.keys()):
            ## Set default bin weights
            for ib in self._IHISTOS[ihn].bins:
                ib.w = 1.0
            ## Find matches
            pathmatch_matchers = [(m,wstr) for m,wstr in matchers.iteritems() if m.match_path(ihn)]
            ## Ditch histos not listed in the weight file
            if not pathmatch_matchers:
                del self._IHISTOS[ihn]
                del self._DHISTOS[ihn]
                continue
            ## Attach fit weights to the ibins, setting to zero if there's no position match
            for ib in self._IHISTOS[ihn].bins:
                posmatch_matchers = [(m,wstr) for (m,wstr) in pathmatch_matchers if m.match_pos(ib)]
                ib.w = float(posmatch_matchers[-1][1]) if posmatch_matchers else 0

    def normWeights(self):
        sumW = sum([I.w for I in self._IBINS])
        for I in self._IBINS:
            I.w/=sumW

    def setFitData(self):
        self._IBINS, self._DBINS = [], []
        for hn, ih in self._IHISTOS.iteritems():
            for nb in range(len(ih.bins)):
                if ih.bins[nb].w > 0 and self._DHISTOS[hn].bins[nb].err >0:
                    self._IBINS.append(ih.bins[nb])
                    self._DBINS.append(self._DHISTOS[hn].bins[nb])
        # self.normWeights()


    def trivialGoF(self, params):
        """
        Very straightforward goodness-of-fit measure
        """
        # NOTE slightly deviate from professor's own logic by using linearly entering weights here instead of squared
        return sum([ I.w*(I.val(params) - D.val)**2/(D.err**2 + I.err(params, emax=None)**2)   for I, D in zip(self._IBINS, self._DBINS)])
        # return sum([ I.w**2*(I.val(params) - D.val)**2/(D.err**2 + I.err(params, emax=None)**2)   for I, D in zip(self._IBINS, self._DBINS)])

    def trivialUnitGoF(self, params):
        return sum([        (I.val(params) - D.val)**2/(D.err**2 + I.err(params, emax=None)**2)   for I, D in zip(self._IBINS, self._DBINS)])

    def ObsGoF(self, params, ibins, dbins):
        return sum([        (I.val(params) - D.val)**2/(D.err**2 + I.err(params, emax=None)**2)   for I, D in zip(ibins, dbins)])/len(ibins)

    def mkStartPoint(self):
        pstart = [(self._PMIN[i] + self._PMAX[i])/2. for i in xrange(len(self._PMIN))]

        if self._nscan > 0:
            startSampler= prof.NDSampler(zip(self._PMIN, self._PMAX), sampler="uniform", seed=1234)
            startPoints = [startSampler() for _ in range(self._nscan)]
            testVals = [self.trivialGoF(x) for x in startPoints]
            winner = startPoints[testVals.index(min(testVals))]

            # ## This sets the start point
            # print "Using startpoint that yieldes fval %e:"%min(testVals)
            for i, aname in enumerate(self._PNAMES):
                pstart[i] = winner[i]
                # print "%s = %f"%(aname, pstart[i])

        return pstart


    def ihistos(self, P):
        return {k : v.toDataHisto(P) for k, v in self._IHISTOS.iteritems() }


    # @timeit
    def tune(self, wdict):
        self.setWeights(wdict)
        self.setFitData()

        pstart=self.mkStartPoint()

        exec self.funcdef in locals()
        from iminuit import Minuit
        FARG=prof.setupMinuitFitarg(self._PNAMES, pstart, self._PMIN, self._PMAX, {}, {}, False, verbose=False)
        minuit = Minuit(gof, errordef=1, print_level=0, forced_parameters=self._PNAMES, **FARG)
        minuit.strategy = self.STRATEGY

        minuit.migrad()

        result = [minuit.values[p] for p in self._PNAMES]
        self._minP = result

        self.mkReport(minuit)

    @property
    def invcov(self):
        import numpy as np
        return np.linalg.inv(self._cov)

    @property
    def invcovtrace(self):
        import numpy as np
        return np.trace(self.invcov)

    @property
    def invcovdet(self):
        import numpy as np
        return np.linalg.det(self.invcov)

    @property
    def portfolio(self):
        cts = self.meancont.values()
        import numpy as np
        m = np.mean(cts)
        s = np.var(cts)
        ll =  self.riskAv # parameter describing risk aversion
        return m + ll*s  # only mean, only std, dependence on ...

    def mkReport(self, M):
        """
        Take a minuit object and return relevant stuff.
        """

        # Covariance matrix cast into a numpy array
        from numpy import zeros
        dim = len(self._PNAMES)
        self._cov = zeros((dim, dim))
        for i, pi in enumerate(self._PNAMES):
            for j, pj in enumerate(self._PNAMES):
                self._cov[i][j] = M.covariance[(pi, pj)]

        unitgof  = self.trivialUnitGoF(self._minP)
        self.meancont = {hn : self.ObsGoF(self._minP, self._IHISTOS[hn].bins, self._DHISTOS[hn].bins) for hn in sorted(self._IHISTOS.keys()) }

        self._report = {
                "PNAMES": self._PNAMES,
                "minp": self._minP,
                "cov": self._cov,
                "unitgof": unitgof,
                "MEANCONT": self.meancont,
                "trace"   : self.invcovtrace,
                "det"     : self.invcovdet,
                "meancont": self.meancont,
                "portfolio": self.portfolio
                }

    @property
    def report(self): return self._report
    # @property
    # def report(self):
        # """
        # Return a dictionary of the evaluated objectives
        # """
        # obj = {
                # "trace"   : self.invcovtrace,
                # "det"     : self.invcovdet,
                # "meancont": self.meancont,
                # "portfolio": self.portfolio
                # }
        # return obj

    def __call__(self, P):
        # from IPython import embed
        # embed()
        W=self._wtemplate
        for num, p in enumerate(P): W[W.keys()[num]] = p
        # for num, p in enumera
        rep = {
                "trace"   :  None,
                "det"     :  None,
                "meancont":  None,
                "portfolio": None
                }
        try:
            self.tune(W)
            rep=self.report
        except Exception as e:
            # I know this is ugly
            print("Try-except on __call__ of ProfObj:")
            print(e)
            pass


        return rep


    def __repr__(self):
        s="Prof OO --- %i iobs, %i dobs"%(len(self._IHISTOS.keys()), len(self._DHISTOS.keys()))
        return s


if __name__ == "__main__":
    import optparse, os, sys
    op = optparse.OptionParser(usage=__doc__)
    op.add_option("-d", "--datadir", dest="DATADIR", default=None, help="The data directory")
    op.add_option("-o", "--outdir", dest="OUTDIR", default="tunes", help="Prefix for outputs (default: %default)")
    op.add_option("--norm-weights", dest="NORMW", action="store_true", default=False, help="Divide obserbable weight by number of regular bins")
    op.add_option("-v", "--debug", dest="DEBUG", action="store_true", default=False, help="Turn on some debug messages")
    op.add_option("-q", "--quiet", dest="QUIET", action="store_true", default=False, help="Turn off messages")
    op.add_option("-s", "--strategy", dest="STRATEGY",  default=2, type=int, help="Set Minuit strategy [0 fast, 1 standard, 2 slow]")
    op.add_option("-n", dest="SCANNP", default=None, type=int, help="Number of test points find a good migrad start point (default: %default)")
    opts, args = op.parse_args()

    import sys
    PO = ProfOOTune(args[0], opts.DATADIR, nscan=opts.SCANNP)
    print "Reminder: figure out about sumw vs sumw2"

    import time
    start_time = time.time()
    for i in xrange(10):
        W2={"/PDG_HADRON_MULTIPLICITIES_RATIOS/d47-x01-y03":9,"/PDG_HADRON_MULTIPLICITIES_RATIOS/d25-x01-y02":4, "/PDG_HADRON_MULTIPLICITIES_RATIOS/d10-x01-y01":3}
        PO.tune(W2)
        if i%10==0: print "Done with %i/%i"%(i,10)
    print("Boom done after %s seconds" % (time.time() - start_time))
    # from IPython import embed
    # embed()



