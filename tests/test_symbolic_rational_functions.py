import pytest

from time import time

import numpy as np
import sympy
import autograd as ag
#from .context import pyoo
from .context import apprentice

from pyoo import outer_objective as oo
from pyoo import rational_functions as rf
from apprentice.rationalapproximation import RationalApproximation

def test_monomials_conversion():
    """
    Conversion of monomials and coefficients to/from polynomial coefficient matrix.
    """
    monoms = np.array([(1, 2), (4, 1), (0, 2)])
    coeffs = [4,-2,9.5]

    C = rf.monoms2C(monoms, coeffs)

    # construct C
    C_com = np.zeros((5,3))
    for (m,c) in zip(monoms,coeffs):
        C_com[m] = c


    assert (C == C_com).any()

    # and back
    m, c = rf.C2monoms(C)

    assert (m == monoms).any()
    coeffs.sort()
    assert (np.sort(c) == coeffs).any()


def test_RF_from_monomials():
    """
    Rational function from monomials information.
    """

    # instantiation
    pcoeffs = [-1, 3., 10.]
    pmonomials = np.array([(1, 2), (4, 1), (0, 2)])

    qcoeffs = [-2.3, 4.9]
    qmonomials = np.array([(1, 1), (3, 0)])

    f = rf.SymbolicRationalFunction.from_monomials(pcoeffs, qcoeffs, pmonomials, qmonomials, grad=True)
    print(f)

    assert len(f._f.free_symbols) == 2 # dimension

    # numerical evaluation of rational function
    pc = np.zeros((5, 3), dtype=np.float)
    for i in range(len(pcoeffs)):
        pc[pmonomials[i][0], pmonomials[i][1]] = pcoeffs[i]
    qc = np.zeros((4, 2), dtype=np.float)
    for i in range(len(qcoeffs)):
        qc[qmonomials[i][0], qmonomials[i][1]] = qcoeffs[i]
    x0 = 1.9
    y0 = 4.7
    p0 = np.polynomial.polynomial.polyval2d(x0, y0, pc)
    q0 = np.polynomial.polynomial.polyval2d(x0, y0, qc)

    assert abs(p0/q0-f._eval_f(x0,y0)) < 1e-8 # the numerical value is something around 28

    # numerical evaluation of gradient
    c_dpdx = np.polynomial.polynomial.polyder(pc, axis=0)
    c_dpdy = np.polynomial.polynomial.polyder(pc, axis=1)
    c_dqdx = np.polynomial.polynomial.polyder(qc, axis=0)
    c_dqdy = np.polynomial.polynomial.polyder(qc, axis=1)

    dpdx0 = np.polynomial.polynomial.polyval2d(x0, y0, c_dpdx)
    dpdy0 = np.polynomial.polynomial.polyval2d(x0, y0, c_dpdy)
    dqdx0 = np.polynomial.polynomial.polyval2d(x0, y0, c_dqdx)
    dqdy0 = np.polynomial.polynomial.polyval2d(x0, y0, c_dqdy)

    g1 = (dpdx0 * q0 - p0 * dqdx0) / q0 ** 2
    g2 = (dpdy0 * q0 - p0 * dqdy0) / q0 ** 2

    print(np.linalg.norm(np.array([g1,g2])))
    print("-----")
    print(f.eval_grad(x0, y0))
    assert np.linalg.norm(np.array([g1,g2]) - f.eval_grad(x0, y0)) < 1e-10


def test_eval_f():
    """
    Test evaluation of rational function.
    :return:
    """

    pmonoms = np.array([(1, 2), (4, 1), (0, 2)])
    pcoeffs = [4, -2, 9.5]
    Cp = rf.monoms2C(pmonoms, pcoeffs)

    qmonoms = np.array([(3, 1), (0, 7)])
    qcoeffs = [1.9, -0.009]
    Cq = rf.monoms2C(qmonoms, qcoeffs)


    RA = rf.SymbolicRationalFunction.from_monomials(pcoeffs, qcoeffs, pmonoms, qmonoms)
    points = [np.linspace(-1000, 1000, num=100), np.linspace(-9, 9, num=100)]

    sol_RA = []
    sol_pol = []
    for i in np.arange(100):
        p = (points[0][i], points[1][i])
        sol_RA.append(RA.eval_f(*p))
        P = np.polynomial.polynomial.polyval2d(*p, Cp)
        Q = np.polynomial.polynomial.polyval2d(*p, Cq)
        sol_pol.append(P/Q)

    assert np.linalg.norm(np.array(sol_RA) - np.array(sol_pol)) < 1e-10

def test_eval_f_from_RA():
    RA = RationalApproximation(fname='tests/test_rational_approximation.json')
    r = rf.SymbolicRationalFunction.from_RA(RA)

    dim = RA.dim

    points = [np.random.rand(dim) for _ in np.arange(20)]

    sol_RA = []
    sol_r = []
    for p in points:
        sol_RA.append(RA(p))
        sol_r.append(r.eval_f(*p))

    assert np.linalg.norm(np.array(sol_RA) - np.array(sol_r)) < 1e-10

def test_gradient():
    """
    Test gradient functions of the SymbolicRationalFunction() object.
    :return:
    """
    x, y, z = sympy.symbols("x,y,z")
    w = [y,x,z]
    P = sympy.Poly(x ** 5 * y ** 2 * z, y, x, z, domain='ZZ')
    Q = sympy.Poly(5*x*z**3 + 9)
    f = P/Q
    df = [sympy.diff(f, wi) for wi in w]

    RF = rf.SymbolicRationalFunction(P,Q, grad=True, grad_calc="poly")

    # very difficult to compare expressions if they are mathematically the same (there is the simplify() function, which often gives good results, but does not work always); thus numerical comparison
    eval_df = sympy.lambdify(w,df, "numpy")

    for i in np.arange(5):
        x_sample = np.random.rand(3)
        f1 = np.array(eval_df(*x_sample))
        f2 = np.array(RF.eval_grad(*x_sample))
        np.linalg.norm(f1 - f2) < 1e-10

def test_gradient_num(): # using autograd
    pmonoms = np.array([(1, 2), (4, 1), (0, 2)])
    pcoeffs = [4, -2, 9.5]
    qmonoms = np.array([(3, 1), (0, 7)])
    qcoeffs = [1.9, -0.009]

    RA = rf.SymbolicRationalFunction.from_monomials(pcoeffs, qcoeffs, pmonoms, qmonoms, grad=True)
    grad_AD = ag.elementwise_grad(RA.eval_f, (0,1))
    points = [np.linspace(-1000, 1000, num=100), np.linspace(-9, 9, num=100)]

    for i in np.arange(100):
        p = (points[0][i], points[1][i])
        sol_RA = RA.eval_grad(*p)
        sol_autograd = grad_AD(*p)
        assert np.linalg.norm(np.array(sol_RA) - np.array(sol_autograd)) < 1e-10

def test_eval_gradient_from_RA():
    """
    Test gradient of symbolic rational functions, when rational function is created from rational approximation file, i.e. we are now in a scaled world.
    :return:
    """
    RA = RationalApproximation(fname='tests/test_rational_approximation.json')
    r = rf.SymbolicRationalFunction.from_RA(RA, grad=True)

    dim = RA.dim

    points = [np.random.rand(dim) for _ in np.arange(20)]

    grad_AD = ag.elementwise_grad(r.eval_f, tuple(range(dim)))
    sol_AD = []
    sol_r = []
    for p in points:
        sol_AD.append(np.array(grad_AD(*p)))
        sol_r.append(r.eval_grad(*p))

    assert np.linalg.norm(np.array(sol_AD) - np.array(sol_r)) < 1e-10


def test_eval_hessian_from_monomials():
    """
        Test gradient of symbolic rational functions, when rational function is created from monomials, i.e. we are in the non-scaled world (see test_eval_hessian_from_RA for the scaled case).
        :return:
    """
    pmonoms = np.array([(1, 2), (4, 1), (0, 2)])
    pcoeffs = [4, -2, 9.5]
    qmonoms = np.array([(3, 1), (0, 7)])
    qcoeffs = [1.9, -0.009]

    # easier case
    #pmonoms = np.array([(1, 2), (0, 2)])
    #pcoeffs = [4, -2]
    #qmonoms = np.array([(3, 1)])
    #qcoeffs = [1.]


    RA = rf.SymbolicRationalFunction.from_monomials(pcoeffs, qcoeffs, pmonoms, qmonoms, grad=True, hessian=True)
    H = RA._eval_hessian
    H_AD = ag.hessian(lambda z: RA.eval_f(*z))
    points = [np.linspace(-1000, 1000, num=100), np.linspace(-9, 9, num=100)]

    for i in np.arange(100):
        p = (points[0][i], points[1][i])
        sol_RA = H(p)
        sol_autograd = H_AD(np.array(p))
        assert np.linalg.norm(np.array(sol_RA) - np.array(sol_autograd)) < 1e-10



def test_eval_hessian_from_RA():
    """
    Test hessian of symbolic rational functions, when rational function is created from rational approximation file, i.e. we are now in a scaled world.
    :return:
    """
    RA = RationalApproximation(fname='tests/test_rational_approximation.json')
    r = rf.SymbolicRationalFunction.from_RA(RA, grad=True, hessian=True)

    dim = RA.dim

    points = [np.random.rand(dim) for _ in np.arange(20)]

    H_ag = ag.hessian(lambda z: r.eval_f(*z)) # autograd needs a vector, not multiple input arguments!
    H = r._eval_hessian

    sol_AD = []
    sol_r = []

    t = time()
    for p in points:
        sol_AD.append(H_ag(p))
    print(time()-t)
    t = time()
    for p in points:
        sol_r.append(H(p))
    print(time() - t)

    assert np.linalg.norm(np.array(sol_AD) - np.array(sol_r)) < 1e-10