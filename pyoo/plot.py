import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms


# The following is taken from the matplotlib source code and modified in order to use the covariance matrix directly
def confidence_ellipse(x0, y0, cov, ax, n_std=3.0, facecolor='none', edgecolor='red', **kwargs):

    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
        width=ell_radius_x * 2,
        height=ell_radius_y * 2,
        facecolor=facecolor,
        edgecolor=edgecolor,
        **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = x0

    # calculating the stdandard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = y0

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)


def plot_io_surf(f, approx_fp, plotlib="matplotlib", out=None): # we cannot put this in the bin folder as a python command as we need the objective function, which cannot be parsed, as we cannot neither dump it with json nor pickle it with the pickle module (see pickle.dumps() function and this comment: https://stackoverflow.com/questions/36994839/i-can-pickle-local-objects-if-i-use-a-derived-class; the function itself is not pickled, only names, and local objects cannot be pickled anyways)
    """
    Plot surface of inner objective function.
    :param f: Two-argument (i.e. 2D-) objective function to plot.
    :param results_fp: File path to results json file.
    :param approx_fp: File path to approximation json file.
    :param plotlib: Plotting library to use.
    :param out: Specify out file path if plot is to be saved.
    :return:
    """
    import apprentice
    from inspect import signature
    sig = signature(f)
    if len(sig.parameters) != 2:
        raise ValueError("Function needs two input arguments.")

    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt

    binids, RA = apprentice.tools.readApprox(approx_fp)
    # parameter bounds from approximation file (same for all bins!), which is given to the rational approximation objects
    p_L = RA[0]._scaler._Xmin
    p_U = RA[0]._scaler._Xmax
    p_names = RA[0]._scaler.pnames

    x = np.linspace(p_L[0], p_U[0], 30)
    y = np.linspace(p_L[1], p_U[1], 30)

    X, Y = np.meshgrid(x, y)

    Z = np.zeros(X.shape)
    for (i, x, y) in zip(range(X.size), np.nditer(X), np.nditer(Y)):
        Z[np.unravel_index(i, Z.shape)] = f(x, y)
    # alternative: Z = F(X,Y) # does not always seem to work because of broadcasting step

    # minimum and max
    i_max = np.unravel_index(np.argmax(Z), Z.shape)
    i_min = np.unravel_index(np.argmin(Z), Z.shape)
    P_min = (X[i_min],Y[i_min],Z[i_min])
    P_max = (X[i_max],Y[i_max],Z[i_max])


    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.set_xlabel(p_names[0])
    ax.set_ylabel(p_names[1])
    ax.set_zlabel("Value")
    ax.set_title("Inner objective function")

    ax.scatter(*P_min, c="red")
    ax.scatter(*P_max, c="red")
    textstr = '\n'.join((
        r'$P_\mathrm{min}=(%.2f,%.2f,%.2f)$' % (P_min[0],P_min[1],P_min[2]),
        r'$P_\mathrm{max}=(%.2f,%.2f,%.2f)$' % (P_max[0],P_max[1],P_max[2])))
    ax.text(0.05, 0.95, 0.95, textstr)


    if plotlib == "plotly":
        import plotly.plotly as py
        import plotly.graph_objs as go
        import plotly.tools as tls
        plotly_fig = tls.mpl_to_plotly(fig)
        trace1 = go.Surface(z=Z, colorscale='Viridis')
        py.plot([trace1], filename="IO_pseudodata")
    elif plotlib == "matplotlib":
        ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap='viridis', edgecolor='none')
    else:
        raise ValueError("Plotting library {} not known.".format(plotlib))

    if out is not None:
        fig.savefig(out)
    return fig