import os,sys
import numpy as np
def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            y_win = _y
        elif _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def buildexacthessian(args):
    approxfile = args.APPROX
    expdatafile = args.EXPDATA
    weightfile = args.WEIGHTS
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERRAPPROX,
                          filter_hypothesis=False, filter_envelope=False)
    IO._W2 = np.array([1.] * len(IO._W2))
    data = readReport(args.PARAM)
    x = data['x']

    ds = {
            'parameters':x,
            'hessian':IO.hessian(x).tolist(),
          }
    import json
    with open(args.OUTFILE, 'w') as f:
        json.dump(ds, f, indent=4)


# python exactHessian.py -a ../data/A14/approximation.json -e ../data/A14/errapproximation.json -d ../data/A14/exprimental_data.json -w ../data/A14/weights  -p ../../log/robO/A14/new/out/out_mu0.751_ms0_l1.0.json -o /tmp/hessian.json
if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Exact Hessian',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-a", "--approx", dest="APPROX", type=str, default=None,
                        help="Approximation JSON file")
    parser.add_argument("-e", "--errapprox", dest="ERRAPPROX", type=str, default=None,
                        help="Error Approximation JSON file")
    parser.add_argument("-d", "--expdata", dest="EXPDATA", type=str, default=None,
                        help="Experimental data JSON file")
    parser.add_argument("-w", "--weights", dest="WEIGHTS", type=str, default=None,
                        help="Weight file")

    parser.add_argument("-p", "--paramfile", dest="PARAM", type=str, default=None,
                        help="Parameter result JSON file")

    parser.add_argument("-o", "--outfile", dest="OUTFILE", type=str, default=None,
                        help="Output file name (JSON)")

    args = parser.parse_args()

    buildexacthessian(args)
