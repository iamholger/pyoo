import argparse,os,sys
from apprentice.appset import TuningObjective2


def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D = json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win = ""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            break
        elif _y < y_win:
            c_win = chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pmap = D[c_win]["PMAP"][i_win] if "PMAP" in D[c_win] and D[c_win]["PMAP"] is not None else 0.
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x": pmin, 'pmap': pmap, "binids": binids, "pnames": pnames, "hnames": hnames,
            'wmin': wmin, 'youter': D[c_win]["Y_outer"]}

def calcfval(scargs):
    apprfile = scargs.INDIR + "/approximation.json"
    datafile = scargs.INDIR + "/experimental_data_ro.json"
    if not os.path.exists(datafile):
        datafile = scargs.INDIR + "/experimental_data.json"
    wtfile = scargs.INDIR + "/weights"
    TO2 = TuningObjective2(wtfile, datafile, apprfile,
                     scargs.ERROR,
                     debug=scargs.DEBUG,
                     noise_exponent=2
                     )
    lbarr = [m - (sd) for m, sd in zip(TO2._Y, TO2._E)]
    ubarr = [m + (sd) for m, sd in zip(TO2._Y, TO2._E)]

    hnames = sorted(list(set(TO2._hnames)))
    def obsBins(hname):
        return [i for i, item in enumerate(TO2._binids) if item.startswith(hname)]
    OBSBINS = [obsBins(hname) for hname in hnames]

    ROresultds = readReport(scargs.ROBRES)
    wrobopt = ROresultds['wmin'][0]
    for pfile in scargs.PARAMFILES:
        paramds = readReport(pfile)
        param = paramds['x']
        vals = TO2._AS.vals(param)
        if TO2._EAS is not None:
            evals = TO2._EAS.vals(param)
        else:
            evals = [0.]*len(TO2._binids)
        tb = []
        for hno, hname in enumerate(hnames):
            obsbins = OBSBINS[hno]
            for bin in obsbins:
                left = wrobopt[hno]*(vals[bin] - (lbarr[bin] - evals[bin]))**2
                right = wrobopt[hno] * (vals[bin] - (ubarr[bin] + evals[bin])) ** 2
                tb.append(max(left,right))
        print(sum(tb),os.path.basename(pfile))

class SaneFormatter(argparse.RawTextHelpFormatter,
                    argparse.ArgumentDefaultsHelpFormatter):
    pass
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Robust Optimization Function Value Computation',
                                     formatter_class=SaneFormatter)

    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir where approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored "
                             "for building ideal parameters")
    parser.add_argument("-e", "--error", dest="ERROR", type=str, default=None,
                        help="Filename of the MC error stddev approximations is stored")

    parser.add_argument("-r", "--robOresult", dest="ROBRES", type=str, default=None,
                        help="Filename where the robust optmization result is stored")
    parser.add_argument("-p", "--parameters", dest="PARAMFILES", type=str, default=[], nargs='+',
                        help="Filenames of test parameters")

    parser.add_argument("-v", "--debug", dest="DEBUG", action="store_true", default=False,
                        help="Turn on some debug messages")
    args = parser.parse_args()
    if args.DEBUG:
        print(args)
    calcfval(scargs=args)