import numpy as np
import apprentice
import os, sys
from ADoptimalObjective import OEDObjective


def getA14categories10():
    nobs = [200, 59, 9, 8, 20, 36, 4, 8, 20, 42]
    names_lab = [
        "Track Jet properties",
        "Jet shapes",
        "Dijet decorr",
        "Multijets",
        "p_T^z",
        "Substructure",
        "tt gap",
        "Track-jet UE",
        "tt jet shapes",
        "Jet UE"
    ]
    names_fn = [
        "TrackJetProperties",
        "JetShapes",
        "DijetDecorr",
        "Multijets",
        "p_T-z",
        "Substructure",
        "ttGap",
        "Track-jetUE",
        "ttJetShapes",
        "JetUE"
    ]
    rng = []
    start=0
    for no, n in enumerate(nobs):
        rng.append(np.arange(start,start+n).tolist())
        start+=n

    return rng,names_lab,names_fn

def getA14categories23():
    names_lab = [
        "Track Jet properties 1",
        "Track Jet properties 2",
        "Track Jet properties 3",
        "Track Jet properties 4",
        "Jet shapes",
        "Dijet decorr",
        "Multijets",
        "p_T^z",
        "Substructure",
        "tt gap 1",
        "tt gap 2",
        "tt gap 3",
        "tt gap 4",
        "Track-jet UE 1",
        "Track-jet UE 2",
        "tt jet shapes",
        "Jet UE 1",
        "Jet UE 2",
        "Jet UE 3",
        "Jet UE 4",
        "Jet UE 5",
        "Jet UE 6",
        "Jet UE 7"
    ]
    nobsarr = np.arange(406)
    rng = []
    rng.append(nobsarr[0:50])
    rng.append(nobsarr[50:100])
    rng.append(nobsarr[100:150])
    rng.append(nobsarr[150:200])
    rng.append(nobsarr[200:259])
    rng.append(nobsarr[259:268])
    rng.append(nobsarr[268:276])
    rng.append(nobsarr[276:296])
    rng.append(nobsarr[296:332])
    rng.append(nobsarr[332:333])
    rng.append(nobsarr[333:334])
    rng.append(nobsarr[334:335])
    rng.append(nobsarr[335:336])
    rng.append(nobsarr[336:341])
    rng.append(nobsarr[341:344])
    rng.append(nobsarr[344:364])
    rng.append(nobsarr[364:367])
    rng.append(nobsarr[367:370])
    rng.append(nobsarr[370:372])
    rng.append(nobsarr[372:374])
    rng.append(nobsarr[374:376])
    rng.append(nobsarr[376:391])
    rng.append(nobsarr[391:406])

    return rng, names_lab, []

def readcategoryfile(categoryfile,hnames):
    if categoryfile is None:
        rng, names_lab, names_fn = [np.arange(len(hnames))], ["all"], ["all"]
    else:
        rng, names_lab, names_fn = ([],[],[])
        import json
        import re
        with open(categoryfile,'r') as f:
            catds = json.load(f)
        for key in catds:
            names_lab.append(key)
            names_fn.append(re.sub(r"\s+", "", key))
            currrng = []
            for obs in catds[key]:
                currrng.append(hnames.index(obs))
            rng.append(currrng)
    return rng, names_lab, names_fn
def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            y_win = _y
        elif _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def checkexists(args):
    k = 0
    type1 = os.path.basename(args.TESTDIR)
    for t in args.FILES:
        if t == 'u' or t == 'n':
            e = os.path.exists(os.path.join("../../log","optParamsPerObs",type1))
            print(e, os.path.join("../../log","optParamsPerObs",type1))
            if not e: k = 1
        elif t=='w1':
            print(True, t)
            continue
        else:
            e = os.path.exists(t)
            print(e, t)
            if not e: k = 1
    if k: sys.exit(1)


def buildidealparams(indir):
    import json
    approxfile = args.INDIR + "/approximation.json"
    expdatafile = args.INDIR + "/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = args.INDIR + "/experimental_data.json"
    weightfile = args.INDIR + "/weights"
    type1 = os.path.basename(args.INDIR)
    if args.CALIBERROR:
        expdatafile = args.INDIR + "/experimental_data_cenoise.json"
        type1 += "_ce"
    if args.NOISECOV:
        type1 += "_ncov"
    if args.MSE:
        type1 = "_mse"

    outdir = os.path.join("../../log","optParamsPerObs",type1)
    os.makedirs(outdir,exist_ok=True)
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
    IO.setWeights = None
    IO.hnames = sorted(list(set(IO._hnames)))
    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]
    IO.obsBins = obsBins
    if args.MSE:
        IO._E2 = np.array([1.]* len(IO._E2))
    for hname in IO.hnames:
        sel = IO.obsBins(hname)
        if args.NOISECOV:
            type1 = os.path.basename(args.INDIR)
            covfile = os.path.join("../../log/Covariance", type1, "caliberrorcovariance.json")
            with open(covfile, 'r') as f:
                covdata = json.load(f)
            noisecov = np.linalg.inv(np.transpose(np.array(covdata[hname])))

            def obsobjectiveMAT(x, sel=sel):
                print("Please make corrections for code to be compatible with Tuning Objective 2 fiest")
                exit(1)
                if not IO.use_cache:
                    if isinstance(sel, list) or type(sel).__module__ == np.__name__:
                        RR = [IO._RA[i] for i in sel]
                        vals = [r(x) for r in RR]
                    else:
                        RR = IO._RA[sel]
                        vals = [f(x) for f in RR]
                else:
                    IO.setCache(x)
                    vals = np.sum(IO._maxrec * IO._PC[sel], axis=1)
                    if IO._hasRationals:
                        den = np.sum(IO._maxrec * IO._QC[sel], axis=1)
                        vals[IO._mask[sel]] /= den[IO._mask[sel]]
                vals = np.array(vals)

                diff = vals - IO._Y[sel]
                D = np.matmul(noisecov, diff)
                return np.matmul(np.transpose(D), D)
            IO.objective = obsobjectiveMAT

        res = IO.minimize(nstart=100,nrestart=20, sel=sel)
        x = res['x']
        fx = res['fun']
        w = [1 for i in range(len(IO.hnames))]
        reports = {}
        RR = {
            "X_outer": [w],  # parameters of outer objective per iteration
            "X_inner": [x.tolist()],
            # parameters of inner objective per iteration (i.e. tuned parameters given weights)
            "Y_outer": None,  # Outer objective
            "Y_inner": [fx],  # Inner objective
            "OOstartpoint": [None],
            "OOstartpointcomment": [None],
            "pnames_outer": IO.hnames,  # The parameter names of the oo -- i.e. histogram names
            "pnames_inner": IO.pnames,
            "binids": IO._binids
        }
        reports['chain-0'] = RR
        fn = hname.replace("/", "_")+".json"
        outfile = os.path.join(outdir, fn)
        with open(outfile, 'w') as f:
            json.dump(reports, f, indent=4)
def checkTypes(args):
    return checkTypes2(args.FILES,args.REGPARAMNAME)

def checkTypes2(files, regparamname):
    import re
    typearrlab = []
    # typearr = []
    # typecleanarr = []
    # typename = []
    hparam = []
    iteration = []
    for fnum,r in enumerate(files):
        if r == 'u':
            # typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('ideal')
            # typename.append('d')
            hparam.append(-1)
            iteration.append(-1)
        elif r == "n":
            # typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('nadir')
            # typename.append('d')
            hparam.append(-1)
            iteration.append(-1)
        elif r == "w1":
            # typearr.append("\\multicolumn{1}{|c|}{True P}")
            typearrlab.append('Equal weights')
            # typename.append('d')
            hparam.append(-1)
            iteration.append(-1)
        elif "gradO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(regparamname[fnum]), r).group(1))
            # typename.append('FIM')
            hparam.append(mu)
            iteration.append(-1)
            if mu == 0:
                # typearr.append('\\multicolumn{1}{|c|}{FIM \\eqref{eq:bilevel_FIM}}')
                typearrlab.append('FIM')
                # typecleanarr.append('FIM without bound')
            else:
                # typearr.append("\\multicolumn{1}{|c|}{\makecell{FIM w/\\\\bound\\eqref{eq:bilevel_FIM_withbound}\\\\$\\mu=%d$}}"%(mu))
                typearrlab.append('bilevel optimization (FIM)')
                # typecleanarr.append('FIM with bound, $\{}={}$'.format(args.REGPARAMNAME[fnum],mu))
        elif "robO" in r:
            mu = float(re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'.format(regparamname[fnum]), r).group(1))
            # typename.append('robustOpt')
            hparam.append(mu)
            # hparam.append(-1)
            iteration.append((-1))
            # typearr.append("\multicolumn{1}{|c|}{\makecell{Robust\\\\Opt\\eqref{eq:robo_form_final}\\\\$\\mu=%d$}}"%(mu))
            # typearrlab.append('Robust optimization')
            typearrlab.append('Robust optimization')
            # typecleanarr.append('Robust Opt with bound, $\{}={}$'.format(args.REGPARAMNAME[fnum], mu))
        elif "bayesian" in r:
            matcher = re.match(r'.*?_{}(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?_iter(\d+)'.format(regparamname[fnum]), r)
            l = float(matcher.group(1)+matcher.group(3))
            iteration.append(int(matcher.group(4))+1)
            hparam.append(l)
            typearrlab.append('bayesian')
        else:
            findex = -1
            otherapproaches = ['equalweights','portfolio', 'medscore', 'meanscore', # Juli/Wenjing's approaches
                               'CTEQ', 'MSTW', 'NNPDF', 'HERA', 'Monash'] # Approaches in A14 paper
            # otherapproachelab = ['Equal weights','Bilevel-portfolio', 'Bilevel-medianscore', 'Bilevel-meanscore', # Juli/Wenjing's approaches
            #                    'CTEQ', 'MSTW', 'Expert', 'HERA', 'Monash'] # Approaches in A14 paper
            otherapproachelab = ['Equal weights','Portfolio', 'Medianscore', 'Meanscore', # Juli/Wenjing's approaches
                               'CTEQ', 'MSTW', 'Expert', 'HERA', 'Monash'] # Approaches in A14 paper
            for othernum,other in enumerate(otherapproaches):
                if other in r:
                    findex= othernum
                    break
            if findex == -1: raise Exception("Unknown approach {}".format(r))
            # typename.append(otherapproaches[findex])
            hparam.append(-1)
            iteration.append(-1)
            # typearr.append(
            #     "\multicolumn{1}{|c|}{%s}" % (otherapproaches[findex]))
            typearrlab.append('{}'.format(otherapproachelab[findex]))
            # typecleanarr.append('{}'.format(otherapproaches[findex]))

    # return typearr,typearrlab, typecleanarr, typename, hparam
    return typearrlab,hparam,iteration

def plotperformance(args):
    import json
    def obsobjectiveMAT(x):
        print("Please make corrections for code to be compatible with Tuning Objective 2 fiest")
        exit(1)
        if not IO.use_cache:
            if isinstance(sel, list) or type(sel).__module__ == np.__name__:
                RR = [IO._RA[i] for i in sel]
                vals = [r(x) for r in RR]
            else:
                RR = IO._RA[sel]
                vals = [f(x) for f in RR]
        else:
            IO.setCache(x)
            vals = np.sum(IO._maxrec * IO._PC[sel], axis=1)
            if IO._hasRationals:
                den = np.sum(IO._maxrec * IO._QC[sel], axis=1)
                vals[IO._mask[sel]] /= den[IO._mask[sel]]
        vals = np.array(vals)

        diff = vals - IO._Y[sel]
        D = np.matmul(noisecov, diff)
        return np.matmul(np.transpose(D), D)

    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data_ro.json"
    if not os.path.exists(expdatafile):
        expdatafile = args.TESTDIR + "/experimental_data.json"

    type1 = os.path.basename(args.TESTDIR)
    if args.CALIBERROR:
        expdatafile = args.TESTDIR + "/experimental_data_cenoise.json"
        type1 += "_ce"
    if args.NOISECOV:
        type1 += "_ncov"

    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
    IO.setWeights = None
    IO.hnames = sorted(list(set(IO._hnames)))
    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]
    IO.obsBins = obsBins
    IO._W2 = np.array([1.]*len(IO._W2))
    if args.MSE:
        IO._E2 = np.array([1.]* len(IO._E2))
    if args.NOISECOV:
        type2 = os.path.basename(args.TESTDIR)
        covfile = os.path.join("../../log/Covariance", type2, "caliberrorcovariance.json")
        with open(covfile, 'r') as f:
            covdata = json.load(f)
        GAMMANOISEINVMAT_e={}
        for hname in covdata:
            GAMMANOISEINVMAT_e[hname] = np.linalg.inv(np.transpose(np.array(covdata[hname])))

    allchi2 = {}
    allxinner = {}
    for fno,file in enumerate(args.FILES):
        chi2perobs = []
        if file == 'u':
            import json
            for hname in IO.hnames:
                sel = IO.obsBins(hname)
                if len(sel)==0:
                    continue
                ufn = hname.replace("/", "_") + ".json"
                ufname = os.path.join("../../log","optParamsPerObs",type1,ufn)
                data = readReport(ufname)
                if args.NOISECOV:
                    noisecov = GAMMANOISEINVMAT_e[hname]
                    chi2perobs.append(obsobjectiveMAT(data["x"]) / len(sel))
                else:
                    chi2perobs.append(IO.objective(data["x"],sel=sel,unbiased=True)/len(sel))
        elif file == 'w1':
            import json
            nadirfolder = os.path.join("../../log","ConstWeights_W1")
            os.makedirs(nadirfolder,exist_ok=True)
            constwtfile = os.path.join(nadirfolder,"chi2perobs{}.json".format(type1))
            if os.path.exists(constwtfile):
                import json
                with open(constwtfile,'r') as f:
                    ds = json.load(f)
                xinner = ds['Xinner']
            else:
                IO2 = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                                       filter_hypothesis=False,filter_envelope=False)
                IO2.setWeights = None
                IO2._W2 = np.array([1.] * len(IO2._W2))
                res = IO2.minimize(100)
                ds = {}
                xinner = res["x"]
                ds['Xinner'] = res["x"].tolist()
                with open(constwtfile,'w') as f:
                    json.dump(ds,f,indent=4)
            for hname in IO.hnames:
                sel = IO.obsBins(hname)
                if len(sel)==0:
                    continue
                if args.NOISECOV:
                    noisecov = GAMMANOISEINVMAT_e[hname]
                    chi2perobs.append(obsobjectiveMAT(xinner) / len(sel))
                else:
                    chi2perobs.append(IO.objective(xinner, sel=sel, unbiased=True) / len(sel))

        elif file == 'n':
            import json
            nadirfolder = os.path.join("../../log","Nadir")
            os.makedirs(nadirfolder,exist_ok=True)
            nadirfile = os.path.join(nadirfolder,"chi2perobs_{}.json".format(os.path.basename(args.TESTDIR)))
            if os.path.exists(nadirfile):
                import json
                with open(nadirfile,'r') as f:
                    ds = json.load(f)
                chi2perobs = ds['chi2perobs']
            else:
                for hname in IO.hnames:
                    ufn = hname.replace("/", "_") + ".json"
                    ufname = os.path.join("../../log","optParamsPerObs",type1,ufn)
                    data = readReport(ufname)
                    remainchi2arr = []
                    for hname2 in IO.hnames:
                        sel2 = IO.obsBins(hname2)
                        if args.NOISECOV:
                            noisecov = GAMMANOISEINVMAT_e[hname2]
                            remainchi2arr.append(obsobjectiveMAT(data["x"]) / len(sel))
                        else:
                            remainchi2arr.append(IO.objective(data["x"],sel=sel2, unbiased=True)/len(sel2))
                    chi2perobs.append(np.amax(remainchi2arr))
                ds = {}
                ds['chi2perobs'] = chi2perobs
                with open(nadirfile,'w') as f:
                    json.dump(ds,f,indent=4)
        else:
            data = readReport(file)
            for hname in IO.hnames:
                sel = IO.obsBins(hname)
                if len(sel)==0:
                    continue
                allxinner[file] = data['x']
                if args.NOISECOV:
                    noisecov = GAMMANOISEINVMAT_e[hname]
                    chi2perobs.append(obsobjectiveMAT(data["x"]) / len(sel))
                else:
                    chi2perobs.append(IO.objective(data["x"],sel=sel,unbiased=True)/len(sel))

        allchi2[file] = chi2perobs

    #######################################
    if args.PRINTPDIFF:
        prev = None
        for no in range(100):
            if no ==0:
                keyp = ""
                for key in allxinner:
                    if 'iter{}'.format(no) in key:
                        keyp=key
                        break
                if keyp!="":
                    prev = np.array(allxinner[keyp])
            else:
                keyp = ""
                next = ""
                for key in allxinner:
                    if 'iter{}'.format(no) in key:
                        keyp = key
                        break
                if keyp!="":
                    print(keyp)
                    next = np.array(allxinner[keyp])
                    print(sum((next-prev)**2))
                prev = next
    ########################################


    Xaxis = [0.]
    Yaxis = {}

    max = 0
    for key in allchi2:
        if key == 'n':
            continue
        max = np.amax([np.amax(allchi2[key]),max])

    for key in allchi2:
        chi2arr = allchi2[key]
        roundedchi2arr = [np.round(i, 3) for i in chi2arr]
        for c2r in roundedchi2arr:
            Xaxis.append(c2r)
    Xaxis.append(max+1)

    Xaxis = np.unique(Xaxis)
    Xaxis = np.sort(Xaxis)

    for key in allchi2:
        Yaxis[key] = np.zeros(len(Xaxis))
        chi2arr = allchi2[key]
        roundedchi2arr = [np.round(i, 3) for i in chi2arr]
        for cno, chi2 in enumerate(roundedchi2arr):
            for xno, x in enumerate(Xaxis):
                if chi2 <= x:
                    Yaxis[key][xno] += 1

    # for key in allchi2:
    #     Y = []
    #     index = 0
    #     total = 0
    #     for cno, chi2 in enumerate(Xaxis[key]):
    #         if cno<index:
    #             continue
    #         index = cno
    #         for i in range(cno,len(Xaxis[key])):
    #             if Xaxis[key][cno] == Xaxis[key][i]:
    #                 index+=1
    #                 total+=1
    #             else:
    #                 break
    #         for i in range(cno, index):
    #             Y.append(total)
    #     Yaxis[key] = np.array(Y)
    #
    # print(Xaxis)
    # print(Yaxis)
    # sys.exit(1)

    typearrlab,hparam,iteration = checkTypes(args)
    # COL = ["r", "b", "g", "m", "c", "y", "k"]
    marker = ['-','--','-.',':']

    if not args.TABLE:
        import matplotlib.pyplot as plt
        plt.figure(figsize=(12,10))
        for num,key in enumerate(Yaxis):
            label = typearrlab[num]
            if hparam[num] != -1 and iteration[num] != -1:
                label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[num],str(hparam[num]),iteration[num])
            elif hparam[num] != -1:
                label += " $\{} = ".format(args.REGPARAMNAME[num])+str(hparam[num]*100)+"$"
            # 0.2*num+1.5
            plt.step(Xaxis, Yaxis[key], where='mid', label=label,linewidth=1.5,linestyle="%s"%(marker[num%len(marker)]))
            # plt.plot(Xaxis, Yaxis[key], 'C'+str(num)+'o', alpha=0.5)

        size = 24
        # plt.xlim(0.2**10**21)
        plt.ylim(350)
        # plt.xlim(0,1250)
        plt.xticks(fontsize=size-6)
        plt.yticks(fontsize=size - 6)
        plt.yscale('log')
        plt.legend(fontsize=size-4)

        # plt.xscale('log',basex=2)
        plt.xscale('log')

        if len(args.TITLE) > 0:
            title = ' '.join(args.TITLE)
            plt.title(title)
        plt.xlabel('$\\tau$', fontsize=size)
        # if args.MSE:
        #     ylabel = '$\\left|\\left\\{o| \\frac{||R(p) - D||_2^2}{N_o} \\leq \\tau\\right\\}\\right|$'
        # else:
        ylabel = '$\\left|\\left\\{\\mathcal{O}| \\frac{\\chi^2_\\mathcal{O}(\\mathbf{p^*},\\mathbf{w})}{n_{\\rm{bins}}(\\mathcal{O})} \\leq \\tau\\right\\}\\right|$'
        plt.ylabel(ylabel,fontsize=size)
        os.makedirs(args.OUTDIR,exist_ok=True)
        if args.MSE:
            type1+="_mse"
        plt.tight_layout()
        plt.savefig(os.path.join(args.OUTDIR, "{}_{}_performance.pdf".format(args.OFILEPREFIX,type1)))
        # plt.savefig(os.path.join(args.OUTDIR, "{}_{}_performance.png".format(args.OFILEPREFIX, type1)))

#   Comparing the cdf curves
#   K-L Divergence
#     from scipy.stats import entropy
#     KLdata = {}
#     # print("KL ==============================")
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1==num2: continue
#             e = entropy(Yaxis[key1], Yaxis[key2])
#             KLdata[key1 + "_" + key2] = e
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{}\t\t{}\t\t{}".format(e, label1, label2))
#     # print("\n\n###########################\n\n")
#
# #     Jensen-Shannon distance
# #     print("JS ==============================")
#     from scipy.spatial.distance import jensenshannon
#     JSdata = {}
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1==num2: continue
#             d = jensenshannon(Yaxis[key1], Yaxis[key2])
#             JSdata[key1 + "_" + key2] = d
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{}\t\t{}\t\t{}".format(d, label1, label2))
#     # print("\n\n###########################\n\n")
#
# #   L1 distance
# #     print("L1 ==============================")
#     L1data = {}
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1 == num2: continue
#             e = np.sum(np.absolute(Yaxis[key1] - Yaxis[key2]))
#             L1data[key1 + "_" + key2] = e
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{}\t\t{}\t\t{}".format(e, label1, label2))
#     # print("\n \n###########################\n\n")
#
# #   L2 distance
# #     print("L2 ==============================")
#     L2data = {}
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1 == num2: continue
#             e = np.sum((Yaxis[key1] - Yaxis[key2])**2)
#             L2data[key1 + "_" + key2] = e
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{}\t\t{}\t\t{}".format(e, label1, label2))
#     # print("\n \n###########################\n\n")
#
# #   \chi2 test
# #     print("chi2test ==============================")
#     chi2testdata = {}
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1 == num2: continue
#             e = np.sum([((Yaxis[key1][i] - Yaxis[key2][i]) ** 2)/Yaxis[key1][i] for i in range(1,len(Yaxis[key2]))])
#             chi2testdata[key1 + "_" + key2] = e
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{}\t\t{}\t\t{}".format(e, label1, label2))
#     # print("\n \n###########################\n\n")

#   Area between curve
#     print("Area between curve ==============================")
    abc = {}
    for num1, key1 in enumerate(Yaxis):
        for num2, key2 in enumerate(Yaxis):
            if num1 == num2: continue
            Xaxis = [0.]
            Yaxis = {}

            max = 0
            for key in [key1,key2]:
                if key == 'n':
                    continue
                max = np.amax([np.amax(allchi2[key]), max])

            for key in [key1,key2]:
                chi2arr = allchi2[key]
                roundedchi2arr = [np.round(i, 3) for i in chi2arr]
                for c2r in roundedchi2arr:
                    Xaxis.append(c2r)
            Xaxis.append(max + 1)

            Xaxis = np.unique(Xaxis)
            Xaxis = np.sort(Xaxis)

            for key in [key1,key2]:
                Yaxis[key] = np.zeros(len(Xaxis))
                chi2arr = allchi2[key]
                roundedchi2arr = [np.round(i, 3) for i in chi2arr]
                for cno, chi2 in enumerate(roundedchi2arr):
                    for xno, x in enumerate(Xaxis):
                        if chi2 <= x:
                            Yaxis[key][xno] += 1
            # Using area of Trapezoid to find individual areas and summing them up
            e = sum([(((Yaxis[key1][i] - Yaxis[key2][i]) + (Yaxis[key1][i + 1] - Yaxis[key2][i + 1])) / 2)
                     * (Xaxis[i + 1] - Xaxis[i])
                     for i in range(len(Yaxis[key1]) - 1)])
            abc[key1 + "_" + key2] = e
            label1 = typearrlab[num1]
            if hparam[num1] != -1:
                label1 += " ($\mu = " + str(hparam[num1]) + "$)"

            label2 = typearrlab[num2]
            if hparam[num2] != -1:
                label2 += " ($\mu = " + str(hparam[num2]) + "$)"
            # print("{}\t\t{}\t\t{}".format(e, label1, label2))
    # print("\n \n###########################\n\n")
    import json
    os.makedirs(args.OUTDIR,exist_ok=True)
    with open(os.path.join(args.OUTDIR,'abc.json'),'w') as f:
        json.dump(abc,f, indent=4)


# #   KS Test
#     from scipy.stats import ks_2samp
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1==num2 or num1>num2: continue
#             k = ks_2samp(Yaxis[key1], Yaxis[key2])
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{}\t\t{}\t\t{}".format(k, label1, label2))
#     # print("\n\n###########################\n\n")
#
# # Anderson-Darling test for k-samples
#     from scipy.stats import anderson_ksamp
#     for num1, key1 in enumerate(Yaxis):
#         for num2, key2 in enumerate(Yaxis):
#             if num1 == num2: continue
#             ks, kcv, ksl = anderson_ksamp([Yaxis[key1], Yaxis[key2]])
#             label1 = typearrlab[num1]
#             if hparam[num1] != -1:
#                 label1 += " ($\mu = " + str(hparam[num1]) + "$)"
#
#             label2 = typearrlab[num2]
#             if hparam[num2] != -1:
#                 label2 += " ($\mu = " + str(hparam[num2]) + "$)"
#             # print("{} \t\t{}\t\t{}".format(ks, label1, label2))

    # print("\n\n###########################\n\n")
# PRINT TABLES
#     KL 0 and -1
#     printtable(args,"KL divergence", KLdata, typearrlab,hparam,iteration)

#   JS 0 and -1
#     printtable(args, "JS distance", JSdata, typearrlab, hparam,iteration)

# #   l1test 0 and -1
#     printtable(args, "$||.||_1$", L1data, typearrlab, hparam,iteration)

# #   l2test 0 and -1
#     printtable(args, "$||.||_2^2$", L2data, typearrlab, hparam,iteration)

#   chi2test 0 and -1
#     printtable(args, "Chi-Square statistic", chi2testdata, typearrlab, hparam,iteration)

#   area under curve 0 and -1
    printtable(args, "Area between curve", abc, typearrlab, hparam,iteration)

def plotperformancePareto(args):
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    # weightfile = args.TESTDIR + "/weights"
    weightfile = args.TESTDIR + "/weights_wfitrange_ww1"

    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)

    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    def setWeights(wts,hnames):
        resetWeights()
        for num, b in enumerate(IO._binids):
            found = False
            for hn, w in zip(hnames,wts):
                if hn in b:
                    IO._W2[num] = w ** 2
                    found = True
                    break
            if not found:
                IO._W2[num] = 0

    def resetWeights():
        IO._W2 = np.array([1.] * len(IO._W2))

    # https://github.com/QUVA-Lab/artemis/tree/peter/artemis/general
    def is_pareto_efficient(costs, return_mask=True):
        """
        Find the pareto-efficient points
        :param costs: An (n_points, n_costs) array
        :param return_mask: True to return a mask
        :return: An array of indices of pareto-efficient points.
            If return_mask is True, this will be an (n_points, ) boolean array
            Otherwise it will be a (n_efficient_points, ) integer array of indices.
        """
        is_efficient = np.arange(costs.shape[0])
        n_points = costs.shape[0]
        next_point_index = 0  # Next index in the is_efficient array to search for
        while next_point_index < len(costs):
            nondominated_point_mask = np.any(costs < costs[next_point_index], axis=1)
            nondominated_point_mask[next_point_index] = True
            is_efficient = is_efficient[nondominated_point_mask]  # Remove dominated points
            costs = costs[nondominated_point_mask]
            next_point_index = np.sum(nondominated_point_mask[:next_point_index]) + 1
        if return_mask:
            is_efficient_mask = np.zeros(n_points, dtype=bool)
            is_efficient_mask[is_efficient] = True
            return is_efficient_mask
        else:
            return is_efficient

    type1 = os.path.basename(args.TESTDIR)
    os.makedirs(args.OUTDIR,exist_ok=True)
    IO.obsBins = obsBins
    IO.hnames = sorted(list(set(IO._hnames)))

    resetWeights()
    if 'PARETODATA' not in args.FILES[0]:
        results = {
            "Aopt": [],
            "logDopt": [],
            "weighted_chi2": [],
            "filenames": []
        }
        for fno, file in enumerate(args.FILES):
            results['filenames'].append(file)
            data = readReport(file)
            data_hnames = data['hnames']
            data_x = data['x']
            data_w = np.array(data['wmin'][0])
            data_w = data_w/sum(data_w)

            w1 = [1/len(data_hnames) for w in data_hnames]
            setWeights(w1,data_hnames)
            # print(w1)
            # print(IO._W2)
            # exit(1)
            # TO = TuningObjective2("/Users/mkrishnamoorthy/Downloads/weights_wfitrange_ww1", expdatafile,
            #                       approxfile, args.ERROR,
            #                       filter_hypothesis=False, filter_envelope=False)
            # # print(TO._W2)
            # print(len(TO._binids))
            # print(len(IO._binids))
            # exit(1)
            oedo = OEDObjective(TuningObjective2=IO, params=data_x)
            results['Aopt'].append(oedo.calculateAoptimalObjective(data_w))
            results['logDopt'].append(oedo.calculatelogDoptimalObjective(data_w))

            # setWeights(data_w, data_hnames)

            original_stdout = sys.stdout
            wts_fn = "/tmp/wts"
            with open(wts_fn, 'w') as f:
                sys.stdout = f
                for hno,hn in enumerate(data_hnames):
                    print("{} {}".format(hn,data_w[hno]))
                sys.stdout = original_stdout


            # print(IO._W2)
            TO = TuningObjective2(wts_fn, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
            # print(len(TO._binids))
            # for bno, bn in enumerate(TO._binids):
            #     if TO._W2[bno] != IO._W2[bno]:
            #         print("{}\t {}\t {}".format(bn,TO._W2[bno],IO._W2[bno]))
            # exit(1)

            results['weighted_chi2'].append(TO.objective(data_x))

            sys.stdout.write(
                "done with {} out of {} files      \r".format(fno + 1, len(args.FILES)))
            sys.stdout.flush()
        print("\n")
        import json
        with open(os.path.join(args.OUTDIR, "{}_PARETODATA.jsons".format(type1)), 'w') as f:
            json.dump(results, f, indent=4)
    else:
        import json
        with open(args.FILES[0],'r') as f:
            results = json.load(f)

    paretodata = [[a, ld, ch2] for a, ld, ch2 in
                  zip(results['Aopt'], results['logDopt'], results['weighted_chi2'])]
    paretodata = np.array(paretodata)
    result_aopt = np.array(results['Aopt'])
    result_ldopt = np.array(results['logDopt'])
    result_wchi2 = np.array(results['weighted_chi2'])
    result_fn = np.array(results['filenames'])

    paretopointmask = is_pareto_efficient(paretodata)
    print("###################")
    print(results)
    print(paretopointmask)
    print(result_fn[paretopointmask])
    print("###################")
    if len(args.REGPARAMNAME) == 0:
        regparamname = ['mu'] * len(results['filenames'])
    elif len(args.REGPARAMNAME) == 1:
        regparamname = [args.REGPARAMNAME[0]] * len(results['filenames'])
    else:
        regparamname = args.REGPARAMNAME
    typearrlab, hparam, iteration = checkTypes2(results['filenames'],regparamname)

    result_hparam = np.array(hparam)
    from mpl_toolkits import mplot3d
    import matplotlib.pyplot as plt
    ax = plt.axes(projection='3d')
    ax.scatter3D(result_aopt[paretopointmask],result_ldopt[paretopointmask],
                 result_wchi2[paretopointmask],label="pareto runs")
    ax.scatter3D(result_aopt[~paretopointmask], result_ldopt[~paretopointmask],
                 result_wchi2[~paretopointmask], label="other runs")

    plt.legend()
    ax.set_xlabel("A optimal criteria")
    ax.set_ylabel("log D optimal criteria")
    ax.set_zlabel("Weighted $\chi^2$")
    print(np.argmin(result_aopt[paretopointmask]), np.min(result_aopt[paretopointmask]),
          result_hparam[paretopointmask][np.argmin(result_aopt[paretopointmask])])
    print(np.argmin(result_ldopt[paretopointmask]),np.min(result_ldopt[paretopointmask]),
          result_hparam[paretopointmask][np.argmin(result_ldopt[paretopointmask])])
    print(np.argmin(result_wchi2[paretopointmask]),np.min(result_wchi2[paretopointmask]),
          result_hparam[paretopointmask][np.argmin(result_wchi2[paretopointmask])])



    print("mu Wchi2 Aopt LDopt")
    for x, y, z,lab in zip(
        result_aopt[paretopointmask],
        result_ldopt[paretopointmask],
        result_wchi2[paretopointmask],
        result_hparam[paretopointmask]):
        print("%s %.4f %.4f %.4f"%(lab, z,x,y))
        ax.text(x, y, z, lab,fontsize=6)
    plt.show()

def plotperformance2(args):
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]
    IO.obsBins = obsBins
    IO._W2 = np.array([1.]*len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))
    allerror1sdaway = {}
    allsdawayobs = {}
    allxinner = {}
    maxfactor = 200
    lowestexponent = 6
    for fno, file in enumerate(args.FILES):
        data = readReport(file)
        allsdawayobs[file] = [0]
        factor = 1
        nobsachieved = 0
        while nobsachieved < len(IO.hnames):
            err2perobs = []
            for hname in IO.hnames:
                sel = IO.obsBins(hname)
                if len(sel) == 0:
                    continue
                x = data['x']
                allxinner[file] = x
                vals = IO._AS.vals(x, sel=sel)
                if IO._EAS is not None:
                    E2 = IO._EAS.vals(x, sel=sel)
                else:
                    E2 = np.zeros_like(vals)
                E = IO._E[sel]
                Y = IO._Y[sel]
                err2perbin = []
                TE = factor * (E + E2)
                for i in range(len(sel)):
                    err = (max(
                            0,
                               Y[i]-TE[i]-vals[i],
                               vals[i]-TE[i]-Y[i]
                           ))**2
                    err2perbin.append(err)
                err2perobs.append(sum(err2perbin)/len(err2perbin))
            if factor ==1:
                allerror1sdaway[file] = err2perobs
            nz = 0
            for i in err2perobs:
                if i < 10**-lowestexponent:
                    nz+=1
            allsdawayobs[file].append(nz)
            nobsachieved = nz
            factor+=1
            if factor == maxfactor:
                break

    # print(allerror)
    # print(allsdawayobs[file])
    # print(np.count_nonzero(err2perobs))
    # print(err2perobs)

    Xaxis = [0.]
    Yaxis = {}
    for key in allerror1sdaway:
        errorarr = allerror1sdaway[key]
        roundederr2arr = [np.round(i, lowestexponent) for i in errorarr]
        for c2r in roundederr2arr:
            Xaxis.append(c2r)
    Xaxis = np.unique(Xaxis)
    Xaxis = np.sort(Xaxis)

    for key in allerror1sdaway:
        Yaxis[key] = np.zeros(len(Xaxis))
        errorarr = allerror1sdaway[key]
        roundederr2arr = [np.round(i, lowestexponent) for i in errorarr]
        for eno, err2 in enumerate(roundederr2arr):
            for xno, x in enumerate(Xaxis):
                if err2 <= x:
                    Yaxis[key][xno] += 1
    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['-', '--', '-.', ':']

    import matplotlib.pyplot as plt
    plt.figure(figsize=(12, 10))
    for num, key in enumerate(Yaxis):
        label = typearrlab[num]
        if hparam[num] != -1 and iteration[num] != -1:
            label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[num], str(hparam[num]), iteration[num])
        elif hparam[num] != -1:
            label += " ($\{} = ".format(args.REGPARAMNAME[num]) + str(hparam[num]) + "$)"

        plt.step(Xaxis, Yaxis[key], where='mid', label=label, linewidth=1, linestyle="%s" % (marker[num % len(marker)]))
    size = 20
    # plt.ylim(0)
    # plt.xlim(0,1250)
    plt.xticks(fontsize=size - 6)
    plt.yticks(fontsize=size - 6)
    # plt.yscale('log')
    plt.legend(fontsize=size - 4)

    if len(args.TITLE) > 0:
        title = ' '.join(args.TITLE)
        plt.title(title)
    plt.xlabel('$\\tau$', fontsize=size)
    ylabel = '$\\left|\\left\\{o| \\frac{error}{ndf} \\leq \\tau\\right\\}\\right|$'
    plt.ylabel(ylabel, fontsize=size)
    plt.xscale('log')
    os.makedirs(args.OUTDIR, exist_ok=True)
    type1 = os.path.basename(args.TESTDIR)
    plt.savefig(os.path.join(args.OUTDIR, "{}_{}_performance.pdf".format(args.OFILEPREFIX, type1)))

    maxl = 0
    for key in allsdawayobs:
        maxl = max(maxl,len(allsdawayobs[key]))

    import matplotlib.pyplot as plt
    plt.figure(figsize=(12, 10))
    Xaxis = np.arange(maxl)
    for num,key in enumerate(allsdawayobs):
        label = typearrlab[num]
        if hparam[num] != -1 and iteration[num] != -1:
            label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[num], str(hparam[num]), iteration[num])
        elif hparam[num] != -1:
            label += " ($\{} = ".format(args.REGPARAMNAME[num]) + str(hparam[num]) + "$)"

        arr = allsdawayobs[key]
        if len(arr) < maxl:
            fval = arr[len(arr)-1]
            for i in range(len(arr),maxl):
                arr.append(fval)
        plt.step(Xaxis, arr, where='mid', label=label, linewidth=1, linestyle="%s" % (marker[num % len(marker)]))

    size = 20
    # plt.ylim(0)
    # plt.xlim(0,1250)
    plt.xticks(fontsize=size - 6)
    plt.yticks(fontsize=size - 6)
    # plt.yscale('log')
    plt.legend(fontsize=size - 4)

    if len(args.TITLE) > 0:
        title = ' '.join(args.TITLE)
        plt.title(title)
    plt.xlabel('Factor', fontsize=size)
    ylabel = 'Number of observables within the Factor X SD band'
    plt.ylabel(ylabel, fontsize=size)
    os.makedirs(args.OUTDIR, exist_ok=True)
    type1 = os.path.basename(args.TESTDIR)
    plt.savefig(os.path.join(args.OUTDIR, "{}_{}_sdaway.pdf".format(args.OFILEPREFIX, type1)))

"""
ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14 -e ../data/A14/errapproximation.json -p u ../../log/robO/RobustOptResults/A14-robustOptimization/$ftype/*.json ../../log/juliwenjing/A14_30/$ftype/*.json ../../log/orig/A14/out_NNPDF.json -l mu -o ../../log/cdfperf2/param/not_filtered
ftype="HypithesisFiltered"; python cdfPerformance.py --buildwitherror -t ../data/A14 -e ../data/A14/errapproximation.json -p u ../../log/robO/RobustOptResults/A14-robustOptimization/$ftype/*.json ../../log/juliwenjing/A14_30/$ftype/*.json ../../log/orig/A14/out_NNPDF.json -l mu -o ../../log/cdfperf2/param/hypothesis_filtered
ftype="OutlierFiltered"; python cdfPerformance.py --buildwitherror -t ../data/A14 -e ../data/A14/errapproximation.json -p u ../../log/robO/RobustOptResults/A14-robustOptimization/$ftype/*.json ../../log/juliwenjing/A14_30/$ftype/*.json ../../log/orig/A14/out_NNPDF.json -l mu -o ../../log/cdfperf2/param/outlier_filtered

ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14-RA -e ../data/A14-RA/errapproximation.json -p ../../log/robO/RobustOptResults/A14_RA-robustOptimization/$ftype/*.json ../../log/juliwenjing/A14_31/$ftype/*.json ../../log/orig/A14/out_NNPDF.json -l mu -o ../../log/cdfperf2/param/not_filtered/31

NEW Dec 2020
ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14-RA -e ../data/A14-RA/errapproximation.json -p ../../log/robO/RobustOptResults/A14_RA-robustOptimization/$ftype/*.json ../../log/juliwenjing/A14_31/406/*.json ../../log/orig/A14/out_NNPDF.json -l mu -o /tmp/tmp

"""
def plotperformanceBins(args,istopcorner,fileteredKey):
    # fileteredKey = "hypothesis_filtered"
    # fileteredKey = "outlier_filtered"
    topcorner = istopcorner
    if topcorner:
        fn_add = "topcorner"
    else:
        fn_add = "full"

    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)

    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    IO.obsBins = obsBins
    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))
    colors = [
        (0.1216, 0.4667, 0.7059),
        (0.9961, 0.4980, 0.0588),
        (0.1804, 0.6235, 0.1686),
        (0.8392, 0.1490, 0.1569),
        (0.5804, 0.4039, 0.7412),
        (0.5490, 0.3373, 0.2980),
        'yellow'
    ]
    type1 = os.path.basename(args.TESTDIR)
    # Poly IO
    if "-RA" in type1:
        polytestdir = args.TESTDIR[:-3]
        polyexpdatafile = polytestdir + "/experimental_data.json"
        polyweightfile = polytestdir + "/weights"
        polyapproxfile = polytestdir + "/approximation.json"
        polyerrapproxfile = polytestdir + "/errapproximation.json"
        IOpoly = TuningObjective2(polyweightfile, polyexpdatafile, polyapproxfile, polyerrapproxfile,
                                  filter_hypothesis=False, filter_envelope=False)
    else:
        IOpoly = IO

    # Filtered IO
    if "hypothesis_filtered" not in type1 and "outlier_filtered" not in type1:
        buildfilteredplots = True
        if 'RA' in type1:
            ftype1 = "{}_{}-RA".format(type1[:-3],fileteredKey)
        else:
            ftype1 = "{}_{}".format(type1,fileteredKey)
        fdir = os.path.join(os.path.dirname(args.TESTDIR),ftype1)
        if not os.path.exists(fdir):
            raise Exception("Filtered input directory {} does not exist".format(fdir))

        fapproxfile = fdir + "/approximation.json"
        fexpdatafile = fdir + "/experimental_data.json"
        fweightfile = fdir + "/weights"
        ferrapproxfile = None
        if args.ERROR is not None:
            ferrapproxfile = fdir + "/errapproximation.json"
        fIO = TuningObjective2(fweightfile, fexpdatafile, fapproxfile, ferrapproxfile,
                              filter_hypothesis=False, filter_envelope=False)
    else: buildfilteredplots = False
    # print(len(IO._binids),len(fIO._binids))

    resultdata = {}
    allcombmetric = {}
    # allweights = {}
    allcombBins = {}
    catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY,IO.hnames)
    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['-', '--', '-.', ':']
    binresultdata = {}
    for fno, file in enumerate(args.FILES):
        binresultdata[typearrlab[fno]] = {}
    for ano,arr in enumerate(catrng):
        resultdata[names_lab[ano]] = {"Yaxis":None,"Xaxis":{}}
        import matplotlib.pyplot as plt
        import matplotlib as mpl
        mpl.rc('text', usetex=False)
        mpl.rc('font', family='serif', size=12)
        mpl.style.use("ggplot")
        if args.CATEGORY is not None:
            plt.figure(figsize=(4, 4))
        else:
            plt.figure(figsize=(7, 7))
        # plt.figure(figsize=(10, 12))
        for fno, file in enumerate(args.FILES):
            allcombmetric[file] = []
            # allweights[file] = []
            allcombBins[file] = []
            # data = readReport(file)
            # if len(data['wmin'][0]) == len(IO.hnames) - 1:
            #     data['wmin'][0].append(1.-sum(data['wmin'][0]))
            # weightsFromData = data['wmin'][0]
            # if "robO" in file:
            #     weightarr = weightsFromData
            #     sumw = sum(weightarr)
            #     weightsFromData = [w/sumw for w in weightarr]
            # # print(file,sum(weightsFromData))
            for hno in arr:
                hname = IO.hnames[hno]
                if file == 'u':
                    data = readReport(os.path.join(
                        "../../log/optParamsPerObs",
                        type1,
                        hname.replace('/','_') + ".json"
                    ))
                else:
                    data = readReport(file)
                sel = IO.obsBins(hname)
                if len(sel) == 0:
                    continue
                x = data['x']
            #     for i in range(len(sel)):
            #         allweights[file].append(weightsFromData[hno])

                if 'NNPDF' in file:
                    vals = IOpoly._AS.vals(x, sel=sel)
                    E = IOpoly._E[sel]
                    Y = IOpoly._Y[sel]
                    TE2 = (E ** 2)
                else:
                    vals = IO._AS.vals(x, sel=sel)
                    E = IO._E[sel]
                    Y = IO._Y[sel]
                    TE2 = (E ** 2)
                # vals = IO._AS.vals(x, sel=sel)
                # if IO._EAS is not None:
                #     E2 = IO._EAS.vals(x, sel=sel)
                # else:
                #     E2 = np.zeros_like(vals)
                # E = IO._E[sel]
                # Y = IO._Y[sel]
                # TE2 = (E**2)
                for num,n in enumerate(sel):
                    val = (vals[num] - Y[num])**2/TE2[num]
                    allcombmetric[file].append(val)
                    allcombBins[file].append(IO._binids[n])
                    binresultdata[typearrlab[fno]][IO._binids[n]] = val

            Yaxis = np.arange(1 / len(allcombmetric[file]), 1, 1 / len(allcombmetric[file]))
            if len(Yaxis) !=len(allcombmetric[file]):
                Yaxis = np.append(Yaxis, 1)
            ylabel = 'Cumulative distribution of bins'
                     # '(total bins = {})'.format(len(Yaxis))
            resultdata[names_lab[ano]]["Yaxis"] = Yaxis.tolist()

            # Yaxis = np.arange(len(allcombmetric[file]))
            # ylabel = 'Number of bins'

            label = typearrlab[fno]
            if hparam[fno] != -1 and iteration[fno] != -1:
                label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
            elif hparam[fno] != -1:
                label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"

            Xaxis, binnames = zip(*sorted(zip(allcombmetric[file], allcombBins[file])))
            resultdata[names_lab[ano]]["Xaxis"][label] = Xaxis
            allcombBins[file] = binnames
            allcombmetric[file] = Xaxis

            # Xaxis, wt = zip(*sorted(zip(allcombmetric[file], allweights[file])))
            # allcombmetric[file] = Xaxis
            # allweights[file] = wt

            # Xaxis = np.sort(allcombmetric[file])
            # allcombmetric[file] = Xaxis

            plt.step(Xaxis, Yaxis, where='mid', label=label, linewidth=1.2,
                     c=colors[fno % len(colors)],
                     linestyle="%s" % (marker[fno % len(marker)]))
        size = 20
        # plt.ylim(0)
        plt.xlim(10 ** -4, 10 ** 2.8)  # Normal
        ###########
        # Top right corner
        if topcorner:
            plt.xlim(10 ** -2.5, 10**2.8)
            plt.ylim(Yaxis[int(0.35*len(Yaxis))])
        ###########
        plt.vlines(1,ymin=10**-3,ymax = 10**0, colors = 'black',label="Variance boundary")
        plt.xticks(fontsize=size - 6)
        plt.yticks(fontsize=size - 6)
        plt.yscale('log')
        if args.CATEGORY is None:
            plt.legend(fontsize=size - 4)
        # else:
        plt.ylim(10 ** -3)

        # if len(args.TITLE) > 0:
        #     title = ' '.join(args.TITLE)
        #     plt.title(title)
        # plt.title(names_lab[ano],fontsize=size - 4)
        props = dict(boxstyle='square', facecolor='wheat', alpha=1)
        if args.CATEGORY is not None:
            plt.annotate('%s' % names_lab[ano], xy=(0, 1), xycoords='axes fraction', fontsize=16,
                         horizontalalignment='left', verticalalignment='top', bbox=props)
        if args.CATEGORY is None:
            plt.xlabel('Band of variance levels', fontsize=size)
        if args.CATEGORY is None:
            plt.ylabel(ylabel, fontsize=size)
        plt.tight_layout()
        plt.xscale('log')
        dir = os.path.join(args.OUTDIR,'bins','chi2_perf')
        os.makedirs(dir, exist_ok=True)
        type1 = os.path.basename(args.TESTDIR)
        plt.savefig(os.path.join(dir, "{}_{}_{}_{}_comb_chi2_performance.pdf".format(args.OFILEPREFIX, fn_add, type1,names_fn[ano])))
        plt.close("all")

        if buildfilteredplots:
            import matplotlib.pyplot as plt
            if args.CATEGORY is not None:
                plt.figure(figsize=(4, 4))
            else:
                plt.figure(figsize=(7, 7))

            for fno, file in enumerate(args.FILES):
                label = typearrlab[fno]
                if hparam[fno] != -1 and iteration[fno] != -1:
                    label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
                elif hparam[fno] != -1:
                    label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"

                Xaxis = allcombmetric[file]
                Yaxis = []
                fbinnos = 1
                for bno,bin in enumerate(allcombBins[file]):
                    if bin not in fIO._binids:
                        fbinnos += 1
                    # if Xaxis[bno-1]<1 and Xaxis[bno] >=1:
                    #      print(file, bno,fbinnos)
                    Yaxis.append(fbinnos/(bno+1))
                ylabel = 'Number of bins filtered out'

                maxy = max(Yaxis)
                # Yaxis = [y/maxy for y in Yaxis]
                ylabel = ' Filtered bins (# {}) / No. of bins (# {})'.format(fbinnos,len(allcombBins[file]))


                plt.step(Xaxis, Yaxis, where='mid', label=label, linewidth=1.2,
                         c=colors[fno % len(colors)],
                         linestyle="%s" % (marker[fno % len(marker)]))
            size = 20
            plt.xlim(10 ** -9, 10 ** 2.8)  # Normal
            plt.vlines(1,ymin=min(Yaxis),ymax = max(Yaxis), colors = 'black',label="Variance boundary")
            plt.xticks(fontsize=size - 6)
            plt.yticks(fontsize=size - 6)
            plt.yscale('log')
            ###########
            # Top right corner
            if topcorner:
                plt.xlim(10 ** -2.5, 10 ** 2.8)
                plt.ylim(Yaxis[int(0.35*len(Yaxis))])
            ###########
            if args.CATEGORY is None:
                plt.legend(fontsize=size - 4)
            props = dict(boxstyle='square', facecolor='wheat', alpha=1)
            if args.CATEGORY is not None:
                plt.annotate('%s' % names_lab[ano], xy=(0, 1), xycoords='axes fraction', fontsize=16,
                             horizontalalignment='left', verticalalignment='top', bbox=props)
            if args.CATEGORY is None:
                plt.xlabel('Multiple of Variance', fontsize=size)
            if args.CATEGORY is None:
                plt.ylabel(ylabel, fontsize=size)
            plt.tight_layout()
            plt.xscale('log')
            dir = os.path.join(args.OUTDIR, 'bins', 'filteredBins')
            os.makedirs(dir, exist_ok=True)
            type1 = os.path.basename(args.TESTDIR)
            plt.savefig(
                os.path.join(dir, "{}_{}_{}_{}_{}_bins.pdf".format(args.OFILEPREFIX, fn_add, type1, names_fn[ano],fileteredKey)))
            plt.close("all")


        import matplotlib.pyplot as plt
        nrow = 2
        ncol = 2
        fig, ax = plt.subplots(nrows=nrow, ncols=ncol, figsize=(15, 8))
        for i in range(4):
            for fno, file in enumerate(args.FILES):
                label = typearrlab[fno]
                if hparam[fno] != -1 and iteration[fno] != -1:
                    label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
                elif hparam[fno] != -1:
                    label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"
                Xaxis = allcombmetric[file]
                Yaxis = np.arange(len(Xaxis))
                n = len(Xaxis)

                r = int(i / 2)
                c = (i % 2)
                if i==0:
                    ax[r][c].set_xlim(10**-9)
                thisxaxis = Xaxis[i*int(n/4):(i+1)*int(n/4)]
                thisyaxis = Yaxis[i*int(n/4):(i+1)*int(n/4)]
                ax[r][c].step(thisxaxis, thisyaxis, where='mid', linewidth=0.5)
                ax[r][c].step(thisxaxis, thisyaxis, where='mid', label=label, linewidth=1,
                              c=colors[fno % len(colors)],
                         linestyle="%s" % (marker[fno % len(marker)]))
                ax[r][c].set_title("Q{}".format(i + 1), fontsize=size - 4)
                ax[r][c].set_xscale('log')
                ax[r][c].set_yscale('log')
                ax[r][c].legend()
                if r == 1:
                    ax[r][c].set_xlabel('Multiple of Variance', fontsize=size)
                ylabel = 'Number of bins'
                if c == 0:
                    ax[r][c].set_ylabel(ylabel, fontsize=size)
        plt.suptitle(names_lab[ano], fontsize=size)
        plt.tight_layout()
        dir = os.path.join(args.OUTDIR, 'bins','quartile_chi2_perf')
        os.makedirs(dir, exist_ok=True)
        plt.savefig(os.path.join(dir, "{}_{}_{}_quartile_chi2_performance.pdf".format(args.OFILEPREFIX, type1,names_fn[ano])))
        plt.close("all")



        # import matplotlib.pyplot as plt
        # nrow = 2
        # ncol = 3
        # fig, ax = plt.subplots(nrows=nrow, ncols=ncol,figsize=(25,8))
        # plt.rc('xtick', labelsize=size - 6)
        # plt.rc('ytick', labelsize=size - 6)
        # for fno, file in enumerate(args.FILES):
        #     Yaxis = np.array(allweights[file])
        #     for ynum,y in enumerate(Yaxis):
        #         if y == 0:
        #             Yaxis[ynum] = 10**-20
        #
        #     label = typearrlab[fno]
        #     if hparam[fno] != -1 and iteration[fno] != -1:
        #         label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
        #     elif hparam[fno] != -1:
        #         label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"
        #
        #     Xaxis = np.arange(1,len(allcombmetric[file])+1)
        #     r = int(fno/ncol)
        #     c = (fno % ncol)
        #     # print(fno,r,c)
        #     ax[r][c].step(Xaxis, Yaxis, where='mid',
        #                   c=colors[0],
        #                   linewidth=0.5)
        #     # ax[r][c].scatter(Xaxis, Yaxis,s=0.5)
        #     # ax[r][c].plot(Xaxis, Yaxis,  linewidth=0.3)
        #     ax[r][c].set_title(label,fontsize=size - 4)
        #     # ax[r][c].set_yscale('log')
        #     if c!=0:
        #         ax[r][c].get_yaxis().set_visible(False)
        #     if r==0:
        #
        #         ax[r][c].get_xaxis().set_visible(False)
        #     ax[r][c].set_yscale('log')
        #     ax[r][c].set_ylim(10**-9, 1)
        #     ax[r][c].set_xscale('log')
        #     ax[r][c].set_xlim(1,len(Xaxis))
        #     ax[r][c].set_xlabel("Bins",fontsize=size - 4)
        #     ax[r][c].set_ylabel("Weights",fontsize=size - 4)
        # plt.suptitle(names_lab[ano],fontsize=size)
        # plt.tight_layout()
        #
        # dir = os.path.join(args.OUTDIR, 'bins','weights')
        # os.makedirs(dir, exist_ok=True)
        # plt.savefig(os.path.join(dir, "{}_{}_{}_comb_weights.pdf".format(args.OFILEPREFIX, type1,names_fn[ano])))
        # plt.close("all")
    import json
    dir = os.path.join(args.OUTDIR, 'bins', 'chi2_perf')
    os.makedirs(dir, exist_ok=True)
    with open(os.path.join(dir,"{}_{}_{}_comb_chi2_performance.json".format(
            args.OFILEPREFIX, fn_add,type1)),'w') as f:
        json.dump(resultdata, f, indent=4)
    with open(os.path.join(dir, "{}_{}_{}_comb_chi2_performance_binresult.json".format(
            args.OFILEPREFIX, fn_add, type1)), 'w') as f:
        json.dump(binresultdata, f, indent=4)

"""
ftype="HypithesisFiltered"; atype="meanscore.json"; python cdfPerformance.py --buildwitherror -t ../data/A14-RA -e ../data/A14-RA/errapproximation.json -p ../../log/juliwenjing/A14_31/406/$atype ../../log/juliwenjing/A14_31/$ftype/$atype -l mu -o /tmp/filteredresults

ftype="HypithesisFiltered"; python cdfPerformance.py --buildwitherror -t ../data/A14-RA -e ../data/A14-RA/errapproximation.json -p ../../log/robO/RobustOptResults/A14_RA-robustOptimization/406/*.json ../../log/robO/RobustOptResults/A14_RA-robustOptimization/$ftype/*.json -l mu -o /tmp/filteredresults
ftype="HypithesisFiltered"; python cdfPerformance.py --buildwitherror -t ../data/Sherpa-RA -e ../data/Sherpa-RA/errapproximation.json -p ../../log/robO/RobustOptResults/Sherpa_RA-robustOptimization/All/*.json ../../log/robO/RobustOptResults/Sherpa_RA-robustOptimization/$ftype/*.json -l mu -o /tmp/filteredresults
"""
def plotperformanceFilteredBins(args):
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
    def obsBinInIO(hname,binids):
        binnames = [item for i, item in enumerate(binids) if item.startswith(hname)]
        ret = []
        for tno,testbn in enumerate(binnames):
            for index, bn in enumerate(IO._binids):
                if bn==testbn:
                    ret.append(index)
                    break
        return ret


    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))
    colors = [
        (0.1216, 0.4667, 0.7059),
        'red',
        (0.9961, 0.4980, 0.0588),
        (0.1804, 0.6235, 0.1686),
        (0.8392, 0.1490, 0.1569),
        (0.5804, 0.4039, 0.7412),
        (0.5490, 0.3373, 0.2980),
        'yellow'
    ]
    # Filtered IO
    if "HypithesisFiltered" in args.FILES[1]:
        fileteredKey = "hypothesis_filtered"
    elif "OutlierFiltered" in args.FILES[1]:
        fileteredKey = "outlier_filtered"
    else:
        raise Exception("Failed to understand the filter type")
    type1 = os.path.basename(args.TESTDIR)
    if 'RA' in type1:
        ftype1 = "{}_{}-RA".format(type1[:-3], fileteredKey)
    else:
        ftype1 = "{}_{}".format(type1, fileteredKey)
    fdir = os.path.join(os.path.dirname(args.TESTDIR), ftype1)
    if not os.path.exists(fdir):
        raise Exception("Filtered input directory {} does not exist".format(fdir))

    fapproxfile = fdir + "/approximation.json"
    fexpdatafile = fdir + "/experimental_data.json"
    fweightfile = fdir + "/weights"
    ferrapproxfile = None
    if args.ERROR is not None:
        ferrapproxfile = fdir + "/errapproximation.json"
    fIO = TuningObjective2(fweightfile, fexpdatafile, fapproxfile, ferrapproxfile,
                           filter_hypothesis=False, filter_envelope=False)
    fIO.hnames = sorted(list(set(fIO._hnames)))

    combmetric_data = {}

    testkeys = ['alldatatest','not filtereddatatest','filtereddatatest']
    for key in testkeys:
        combmetric_data[key] = {}
    testbinids = [IO._binids,fIO._binids,
                  [bin for bin in IO._binids if bin not in fIO._binids]]
    nfilttesthnames = np.array([b.split("#")[0]  for b in testbinids[2]])
    testhnames = [IO.hnames, fIO.hnames,
                  sorted(list(set(nfilttesthnames)))]
    # arr = [len(i) for i in testhnames]
    # print(arr)
    # arr = [len(i) for i in testbinids]
    # print(arr)

    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['-', '--', '-.', ':']

    for testdkey,testhname,testbinid in zip(testkeys,testhnames,testbinids):
        catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY, testhname)
        combmetric_data[testdkey]['allp'] = {}
        combmetric_data[testdkey]['fp'] = {}
        for ano, arr in enumerate(catrng):
            combmetric_data[testdkey]['allp'][names_fn[ano]] = {}
            combmetric_data[testdkey]['fp'][names_fn[ano]] = {}
            for fno, file in enumerate(args.FILES):
                combmetric_data[testdkey]['allp'][names_fn[ano]][file] = []
                combmetric_data[testdkey]['fp'][names_fn[ano]][file] = []
                data = readReport(file)
                x = data['x']
                for hno in arr:
                    hname = testhname[hno]
                    if file == 'u':
                        raise Exception('ideal/single tune (u) not supported')
                    sel = obsBinInIO(hname,testbinid)
                    if len(sel) == 0:
                        continue
                    vals = IO._AS.vals(x, sel=sel)
                    E = IO._E[sel]
                    Y = IO._Y[sel]
                    TE2 = (E ** 2)

                    for num, n in enumerate(sel):
                        val = (vals[num] - Y[num]) ** 2 / TE2[num]
                        if fno % 2 == 0:
                            combmetric_data[testdkey]['allp'][names_fn[ano]][file].append(val)
                        elif fno % 2 == 1:
                            combmetric_data[testdkey]['fp'][names_fn[ano]][file].append(val)

    size = 20
    cross1str = ""
    odir = None
    props = dict(boxstyle='square', facecolor='wheat', alpha=1)
    for testdkey, testhname, testbinid in zip(testkeys, testhnames, testbinids):
        catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY, testhname)
        for ano, arr in enumerate(catrng):
            fno = 0
            while fno < len(args.FILES)-1:
                import matplotlib.pyplot as plt
                import matplotlib as mpl
                mpl.rc('text', usetex=False)
                mpl.rc('font', family='serif', size=12)
                mpl.style.use("ggplot")
                ##############################
                plt.figure(figsize=(4, 3))
                # plt.figure(figsize=(10, 3))
                ##############################


                Xaxisall = np.sort(combmetric_data[testdkey]['allp'][names_fn[ano]][args.FILES[fno]])
                Xaxisfilt = np.sort(combmetric_data[testdkey]['fp'][names_fn[ano]][args.FILES[fno+1]])
                Yaxis = np.arange(1 / len(Xaxisall), 1, 1 / len(Xaxisall))
                if len(Yaxis) != len(Xaxisall):
                    Yaxis = np.append(Yaxis, 1)
                plt.ylim(10**-3,5)
                # Yaxis = np.arange(len(Xaxisall))
                # print(len(Xaxisall))
                ylabel = 'Cumulative distribution of bins (total bins = {})'.format(len(Yaxis))

                labelAll = typearrlab[fno]
                labelFilt = typearrlab[fno+1]
                if hparam[fno] != -1 and iteration[fno] != -1:
                    labelAll += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
                elif hparam[fno] != -1:
                    labelAll += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"

                if hparam[fno+1] != -1 and iteration[fno+1] != -1:
                    labelFilt += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno+1], str(hparam[fno+1]), iteration[fno+1])
                elif hparam[fno+1] != -1:
                    labelFilt += " ($\{} = ".format(args.REGPARAMNAME[fno+1]) + str(hparam[fno+1]) + "$)"

                labelAll = "$p^*_a$"
                labelBinFilt = "$p^*_b$"
                labelObsFilt = "$p^*_o$"
                if fileteredKey == "outlier_filtered":
                    labelFilt = labelObsFilt
                    filtcol = colors[1]
                else:
                    labelFilt = labelBinFilt
                    filtcol = colors[2]
                binindices= np.arange(len(Xaxisall))
                if testdkey == testkeys[0] and fno ==0:
                    cross1str += "\\multicolumn{1}{|c|}{%s}"%typearrlab[fno]
                for xno in range(1,len(Xaxisall)):
                    if Xaxisall[xno-1]<1 and Xaxisall[xno]>=1:
                        cross1str += " & {}".format(binindices[xno])
                for xno in range(1,len(Xaxisfilt)):
                    if Xaxisfilt[xno-1]<1 and Xaxisfilt[xno]>=1:
                        cross1str += " & {}".format(binindices[xno])

                plt.annotate('{}\n{}'.format(typearrlab[fno],testdkey[:-8]), xy=(0, 1), xycoords='axes fraction', fontsize=13,
                             horizontalalignment='left', verticalalignment='top', bbox=props)
                plt.step(Xaxisall, Yaxis, where='mid', label=labelAll, linewidth=1.2,
                         c=colors[0],
                         linestyle="%s" % (marker[0]))
                #######################################
                plt.step(Xaxisfilt, Yaxis, where='mid', label=labelFilt, linewidth=1.2,
                         c=filtcol,
                         linestyle="%s" % (marker[1]))

                # OR

                # plt.step(Xaxisfilt, Yaxis, where='mid', label=labelObsFilt, linewidth=1.2,
                #          c=colors[1],
                #          linestyle="%s" % (marker[1]))
                # plt.step(Xaxisfilt, Yaxis, where='mid', label=labelBinFilt, linewidth=1.2,
                #          c=colors[2],
                #          linestyle="%s" % (marker[1]))
                #######################################
                plt.xlim(10 ** -4, 10 ** 3.2)
                plt.vlines(1, ymin=min(Yaxis), ymax=max(Yaxis), colors='black', label="Variance boundary")

                #######################################
                # plt.legend(loc='upper center', ncol=3, bbox_to_anchor=(0.5, 1.2), borderaxespad=0., shadow=False)
                #######################################
                plt.xticks(fontsize=size - 6)
                plt.yticks(fontsize=size - 6)
                # plt.legend(fontsize=size - 4)
                plt.yscale('log')
                plt.tight_layout()
                plt.xscale('log')
                # plt.title("{} / {}".format(testdkey,typearrlab[fno]))
                odir = os.path.join(args.OUTDIR, 'bins', 'filteredcdfperf',fileteredKey,typearrlab[fno].replace(' ',''))
                os.makedirs(odir, exist_ok=True)
                type1 = os.path.basename(args.TESTDIR)
                plt.savefig(
                    os.path.join(odir, "{}_{}-{}-{}.pdf".format(args.OFILEPREFIX, type1, names_fn[ano].replace(' ',''),testdkey.replace(' ',''))))
                plt.close("all")
                fno += 2
    cross1str += "\\\\\\hline\n"
    with open (os.path.join(odir,"{}_cross1.txt".format(type1)),'w') as f:
        print(cross1str, file=f)


def organizePlots(args,fileteredKey):
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)

    def obsBinNames(TunObj,hname):
        return [item for i, item in enumerate(TunObj._binids) if item.startswith(hname)]

    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))

    # Filtered IO
    type1 = os.path.basename(args.TESTDIR)
    # if "hypothesis_filtered" not in type1 and "outlier_filtered" not in type1:
    if 'RA' in type1:
        ftype1 = "{}_{}-RA".format(type1[:-3], fileteredKey)
    else:
        ftype1 = "{}_{}".format(type1, fileteredKey)
    fdir = os.path.join(os.path.dirname(args.TESTDIR), ftype1)
    if not os.path.exists(fdir):
        raise Exception("Filtered input directory {} does not exist".format(fdir))

    fapproxfile = fdir + "/approximation.json"
    fexpdatafile = fdir + "/experimental_data.json"
    fweightfile = fdir + "/weights"
    ferrapproxfile = None
    if args.ERROR is not None:
        ferrapproxfile = fdir + "/errapproximation.json"
    fIO = TuningObjective2(fweightfile, fexpdatafile, fapproxfile, ferrapproxfile,
                           filter_hypothesis=False, filter_envelope=False)

    fIO._W2 = np.array([1.] * len(fIO._W2))
    fIO.hnames = sorted(list(set(fIO._hnames)))

    plotfolder = args.INDIR

    import shutil
    for hno, hname in enumerate(IO.hnames):
        allselbins = obsBinNames(IO,hname)
        fselbins = obsBinNames(fIO,hname)
        if len(allselbins) != len(fselbins):
            diff = len(allselbins) - len(fselbins)
            outdir = os.path.join(plotfolder,'filterOrganizedPlots',fileteredKey,hname.replace('/','_'),"b{}".format(diff))
            l=0
            r=0
            if len(fselbins)==0:
                outdir = os.path.join(outdir, "all")
            else:
                l = allselbins.index(fselbins[0])
                r = len(allselbins) - (allselbins.index(fselbins[-1])+1)
                outdir = os.path.join(outdir, "l{}r{}".format(l,r))
            # if l>0 and r>0:
            #     print(allselbins)
            #     print(fselbins)
            #     print(dir)
            #     exit(1)
            # else:
            #     print(dir)

            os.makedirs(outdir,exist_ok=True)
            infile = os.path.join(plotfolder,hname[1:]+".pdf")
            outfile = os.path.join(outdir,hname.replace('/','_')+".pdf")
            # print(infile)
            # print(outfile)
            # exit(1)
            shutil.copyfile(infile,outfile)

def createYoda(args):
    approxfile = args.TESTDIR + "/approximation.json"
    yodaenvfile = "/Users/mkrishnamoorthy/Research/Code/YODA/YODA-1.7.4/yodaenv.sh"
    apppredictfile = "/Users/mkrishnamoorthy/Research/Code/apprentice/bin/app-predict"
    for fno, file in enumerate(args.FILES):
        if file == 'u':
            continue
        data = readReport(file)
        x = data['x']
        pnames = data['pnames']
        pstr = ""
        for pno,pn in enumerate(pnames):
            pstr += "{}\t{}\n".format(pn,x[pno])
        outfilebase = os.path.join(os.path.dirname(file),os.path.basename(file).split('.')[0])
        outfile = outfilebase+".p0"
        print(outfile)
        print(pstr)
        with open(outfile, 'w') as f:
            print(pstr, file=f)
        cmd = "source {}; {} {} -p {} -o {}.yoda".format(yodaenvfile,apppredictfile,approxfile,outfile,outfilebase)
        # print(cmd)
        os.system(cmd)




# Per observable
def plotperformanceObservables(args):
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)

    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    IO.obsBins = obsBins
    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))

    colors = [
        (0.1216, 0.4667, 0.7059),
        (0.9961, 0.4980, 0.0588),
        (0.1804, 0.6235, 0.1686),
        (0.8392, 0.1490, 0.1569),
        (0.5804, 0.4039, 0.7412),
        (0.5490, 0.3373, 0.2980)
    ]

    allcombmetricWeightedAvgVar = {}
    allcombmetricCombVar = {}
    allcombmetricChi2Obs = {}
    allweights = {}
    catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY,IO.hnames)
    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['-', '--', '-.', ':']
    size = 20
    for ano,arr in enumerate(catrng):
        for fno, file in enumerate(args.FILES):
            data = readReport(file)
            allcombmetricWeightedAvgVar[file] = []
            allcombmetricCombVar[file] = []
            allcombmetricChi2Obs[file] = []

            if len(data['wmin'][0]) == len(IO.hnames) - 1:
                data['wmin'][0].append(1. - sum(data['wmin'][0]))
            weightsFromData = data['wmin'][0]
            if "robO" in file:
                weightarr = weightsFromData
                sumw = sum(weightarr)
                weightsFromData = [w / sumw for w in weightarr]
            # print(file,sum(weightsFromData))
            allweights[file] = [weightsFromData[hno] for hno in arr]

            for hno in arr:
                hname = IO.hnames[hno]
                sel = IO.obsBins(hname)
                if len(sel) == 0:
                    continue
                x = data['x']

                vals = IO._AS.vals(x, sel=sel)

                E = IO._E[sel]
                Y = IO._Y[sel]

                # if IO._EAS is not None:
                #     E2 = IO._EAS.vals(x, sel=sel)
                # else:
                #     E2 = np.zeros_like(vals)
                #     raise Exception("Not implemented with plotperformanceObservables")

                # Combined numerator
                combntr = sum((vals-Y)**2)
                combntr /= len(sel)


                # Combined SD
                # rbmean = np.mean(vals)
                # rbsqres = [(v-rbmean)**2 for v in vals]
                # rbvar = [E2[i]**2 +rbsqres[i] for i in range(len(sel))]
                # rbvar = sum(rbvar)/len(sel)


                expdatamean = np.mean(Y)
                expdatasqres = [(v - expdatamean) ** 2 for v in Y]
                expdatavar = [E[i] ** 2 + expdatasqres[i] for i in range(len(sel))]
                expdatavar = sum(expdatavar) / len(sel)

                val = combntr / (expdatavar)
                allcombmetricCombVar[file].append(val)

                # WeightedAvgVar
                # rbvar = sum(E2**2)/len(sel)
                expdatavar = sum(E**2)/len(sel)

                val = combntr / (expdatavar)
                allcombmetricWeightedAvgVar[file].append(val)

                # Chi2 per Obs
                vals = [(vals[i] - Y[i]) ** 2 / E[i]**2 for i in range(len(sel))]
                allcombmetricChi2Obs[file].append(sum(vals)/len(sel))


        # import matplotlib.pyplot as plt
        # fig, ax = plt.subplots(nrows=1, ncols=2, figsize=(25, 8))
        # plt.rc('xtick', labelsize=size - 6)
        # plt.rc('ytick', labelsize=size - 6)
        vartypes = ['WAvgVar', "AvgChi2", "CombinedVar"]
        varlabels  = ['Weighted Average Variance', "Average $\\chi^2$", "Combined Variance"]
        alldata = [allcombmetricWeightedAvgVar,allcombmetricChi2Obs,allcombmetricCombVar]
        xlb = [10**-2.5,10**-2.5,10**-6]
        xub = [10**2,10**2,10**0.5]
        for vtype,vlabel,vdata,lb,ub in zip(vartypes,varlabels,alldata,xlb,xub):
            import matplotlib.pyplot as plt
            if args.CATEGORY is not None:
                plt.figure(figsize=(4, 4))
            else:
                plt.figure(figsize=(7, 7))
            for fno, file in enumerate(args.FILES):
                Yaxis = np.arange(1,len(vdata[file])+1)

                label = typearrlab[fno]
                if hparam[fno] != -1 and iteration[fno] != -1:
                    label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
                elif hparam[fno] != -1:
                    label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"

                Xaxis = np.sort(vdata[file])
                plt.step(Xaxis, Yaxis, where='mid', label=label, linewidth=1,
                         c=colors[fno % len(colors)],
                         linestyle="%s" % (marker[fno % len(marker)]))

            props = dict(boxstyle='square', facecolor='wheat', alpha=1)
            # plt.title("%s / %s"%(names_lab[ano],vlabel), fontsize=size - 4)
            if args.CATEGORY is not None:
                plt.annotate('%s'%names_lab[ano], xy=(0, 1), xycoords='axes fraction', fontsize=16,
                    horizontalalignment='left', verticalalignment='top',bbox=props)

            plt.vlines(1, ymin=min(Yaxis), ymax=max(Yaxis), colors='black', label="Variance boundary")
            plt.xticks(fontsize=size - 6)
            plt.yticks(fontsize=size - 6)
            plt.xlim(lb, ub)
            plt.yscale('log')
            if args.CATEGORY is None:
                plt.legend(fontsize=size - 4)
            if args.CATEGORY is None:
                plt.xlabel('Multiple of Variance', fontsize=size)
            if args.CATEGORY is None:
                ylabel = 'Number of Observables'
                plt.ylabel(ylabel, fontsize=size)
            plt.tight_layout()
            plt.xscale('log')

            dir = os.path.join(args.OUTDIR, 'observables', vtype, 'perf')
            os.makedirs(dir, exist_ok=True)
            type1 = os.path.basename(args.TESTDIR)
            plt.savefig(
                os.path.join(dir, "{}_{}_{}_comb_chi2_performance.pdf".format(args.OFILEPREFIX, type1, names_fn[ano])))
            plt.close("all")

            import matplotlib.pyplot as plt
            nrow = 2
            ncol = 3
            fig, ax = plt.subplots(nrows=nrow, ncols=ncol, figsize=(25, 8))
            plt.rc('xtick', labelsize=size - 6)
            plt.rc('ytick', labelsize=size - 6)

            for fno, file in enumerate(args.FILES):
                vd, Yaxis = zip(*sorted(zip(vdata[file], allweights[file])))
                Yaxis = np.array(Yaxis)
                for ynum, y in enumerate(Yaxis):
                    if y == 0:
                        Yaxis[ynum] = 10 ** -20
                # xyz = np.sort(vdata[file])
                # print(sum(np.abs(xyz-vd)))

                label = typearrlab[fno]
                if hparam[fno] != -1 and iteration[fno] != -1:
                    label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
                elif hparam[fno] != -1:
                    label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"

                Xaxis = np.arange(1, len(vdata[file]) + 1)

                r = int(fno / ncol)
                c = (fno % ncol)

                ax[r][c].step(Xaxis, Yaxis, where='mid',c=colors[0], linewidth=0.5)
                ax[r][c].set_title("%s"%(label), fontsize=size - 4)

                if c != 0:
                    ax[r][c].get_yaxis().set_visible(False)
                if r == 0:
                    ax[r][c].get_xaxis().set_visible(False)

                ax[r][c].set_yscale('log')
                ax[r][c].set_ylim(10 ** -9, 1)
                ax[r][c].set_xscale('log')
                ax[r][c].set_xlim(1, len(Xaxis)+1)
                ax[r][c].set_xlabel("Observables", fontsize=size - 4)
                ax[r][c].set_ylabel("Weights", fontsize=size - 4)
            plt.suptitle("%s / %s"%(names_lab[ano],vlabel), fontsize=size)
            plt.tight_layout()

            dir = os.path.join(args.OUTDIR, 'observables', vtype, 'weights')
            os.makedirs(dir, exist_ok=True)
            plt.savefig(os.path.join(dir, "{}_{}_{}_comb_weights.pdf".format(args.OFILEPREFIX, type1, names_fn[ano])))

# Per observable
def plotresidualhistogram(args):
    import scipy.stats as stats
    def ks_critical_value(n_trials, alpha):
        from scipy.stats import ksone
        return ksone.ppf(1 - alpha / 2, n_trials)
    def computeKS(data, mu, sd):
        from skgof import ks_test
        from scipy.stats import norm
        res = ks_test(data, norm(loc=mu, scale=sd))
        return [res.statistic, res.pvalue]

    # Mahalanobis distance / standardized Euclidean distance for diagonal covariance matrix
    def computeMahalanobisDist(data, mu, sd):
        # np.random.seed(982363)
        # sampledata = np.random.normal(loc=mu,scale=sd,size=len(data))
        # resarr = [(d-s)**2/sd**2 for (d,s) in zip(data,sampledata)]
        resarr = [(d - mu) ** 2 / sd ** 2 for d in data]
        return np.sqrt(sum(resarr))

    def areabetweencurve(X,Y1,Y2):
        area = sum([(((Y1[i] - Y2[i]) + (Y1[i + 1] - Y2[i + 1])) / 2)
             * (X[i + 1] - X[i])
             for i in range(len(Y1) - 1)])

        return area


    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)

    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]
    size = 20
    IO.obsBins = obsBins
    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))

    allcombmetric = {}
    catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY,IO.hnames)
    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['-', '--', '-.', ':']
    for ano, arr in enumerate(catrng):
        for fno, file in enumerate(args.FILES):
            data = readReport(file)
            allcombmetric[file] = []
            if len(data['wmin'][0]) == len(IO.hnames) - 1:
                data['wmin'][0].append(1. - sum(data['wmin'][0]))
            weightsFromData = data['wmin'][0]
            if "robO" in file:
                weightarr = weightsFromData
                sumw = sum(weightarr)
                weightsFromData = [w / sumw for w in weightarr]
            for hno in arr:
                hname = IO.hnames[hno]
                sel = IO.obsBins(hname)
                if len(sel) == 0:
                    continue
                x = data['x']

                vals = IO._AS.vals(x, sel=sel)
                Y = IO._Y[sel]
                E = IO._E[sel]
                for i in range(len(sel)):
                    val = (Y[i] - vals[i])/E[i]
                    if args.BWEIGHTS:
                        val *= weightsFromData[hno]
                    allcombmetric[file].append(val)

        for fno, file in enumerate(args.FILES):
            label = typearrlab[fno]
            if hparam[fno] != -1 and iteration[fno] != -1:
                label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
            elif hparam[fno] != -1:
                label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"
            import matplotlib.pyplot as plt
            plt.figure(figsize=(10, 12))
            # print(np.sort(allcombmetric[file])[:10])
            # print(np.sort(allcombmetric[file])[-10:])
            if args.BWEIGHTS:
                nhistobins = np.arange(-0.1, 0.11,0.02)
            else:
                nhistobins = np.arange(-4, 4.5,0.5)
            histoY, bins, patches = plt.hist(allcombmetric[file],
                                        bins = nhistobins,
                                        density=True
                                        )
            # print(file)
            # print(n,bins,patches)
            # exit(1)
            plt.xticks(fontsize=size - 6)
            plt.yticks(fontsize=size - 6)
            # plt.yscale('log')
            # plt.legend(fontsize=size - 4)

            taublabel = "$T = [\\tau_1,\\dots,\\tau_{N_b}]$ where $\\tau_b = "
            if args.BWEIGHTS:
                taublabel+="w_b"
            taublabel+="\\frac{R_b - f_b(\\mathbf{p^*})}{\Delta R_b}, b \\in \\{1,\\dots,N_b\\}$"
            mdistlabel = "$d_M(T,\\mathcal{N}(0,1)) = " \
                    "\\sqrt{\\sum_{b=1}^{N_b}\\frac{(\\tau_b-0)^2}{1^2}} = \\sqrt{\\sum_{b=1}^{N_b}\\tau_b^2}$"
            mdist = computeMahalanobisDist(allcombmetric[file], 0, 1)
            title = "%s / %s\n%s\n%s = %.2f"%(names_lab[ano],label,taublabel,mdistlabel,mdist)
            if not args.BWEIGHTS:
                abclabel = "Area between curve"
                nhistobinspp5 = [(nhistobins[i] + nhistobins[i+1])/2 for i in range(len(nhistobins)-1)]
                stdnormY = stats.norm.pdf(nhistobinspp5, 0,1)
                abc = areabetweencurve(nhistobinspp5,stdnormY,histoY)
                title += "\n%s = %f"%(abclabel,abc)
            plt.title(title, fontsize = size - 4)
            plt.xlabel('T', fontsize=size)
            ylabel = 'Probability Density'

            if not args.BWEIGHTS:
                mu = 0
                sigma = 1
                SDNX = np.linspace(-4, 4, 100)
                plt.plot(SDNX, stats.norm.pdf(SDNX, mu, sigma),color='black')
            plt.ylabel(ylabel, fontsize=size)
            plt.tight_layout()
            # print(file)
            # print(computeKS(allcombmetric[file],0,1))
            # print(computeMahalanobisDist(allcombmetric[file], 0, 1))
            # print(ks_critical_value(len(allcombmetric[file]), 0.01))
            # print(ks_critical_value(len(allcombmetric[file]), 0.1))
            # print(ks_critical_value(len(allcombmetric[file]), 0.2))
            # print(np.sqrt(sum(np.array(allcombmetric[file])**2)))
            # print("\n")

            # plt.show()
            dir = os.path.join(args.OUTDIR, 'bins', 'residual_perf')
            if args.BWEIGHTS:
                dir = os.path.join(dir, 'with_weights')
            else:
                dir = os.path.join(dir, 'without_weights')
            os.makedirs(dir, exist_ok=True)
            type1 = os.path.basename(args.TESTDIR)
            plt.savefig(
                os.path.join(dir, "{}_{}_{}_{}_comb_residual_performance.pdf".format(args.OFILEPREFIX, type1, names_fn[ano],typearrlab[fno])))
            plt.close("all")

    # print(ks_critical_value(1000, 0.05))


def printtable(args,metric, data, typearrlab,hparam,iteration):
    # nums = [0, len(args.FILES) - 1]
    nums = [0]
    add = ""
    # if args.MSE:
    #     add = " (MSE)"
    s = "%s =============================\n\n"%(metric)
    for num1 in nums:
        s += "\\begin{tabular}{|c|c|}\\hline\n\\textbf{Model} & \\textbf{%s%s} \\\\\\hline\n"%(metric,add)
        # s += "\\begin{tabular}{|c|c|}\\hline\n$\\mathbf{\\mu}$ & \\textbf{%s%s} \\\\\\hline\n" % (metric, add)
        key1 = args.FILES[num1]
        repvals =[]
        for num2, key2 in enumerate(args.FILES):
            if num1 == num2: continue
            repvals.append(data[key1 + "_" + key2])
        repvals = np.sort(repvals)
        repindex = []
        for val in repvals:
            for num2, key2 in enumerate(args.FILES):
                if num1 == num2: continue
                if data[key1 + "_" + key2] == val:
                    repindex.append(num2)
        for num2 in repindex:
            key2 = args.FILES[num2]
            if num1 == num2: continue
            label2 = typearrlab[num2]
            if hparam[num2] != -1 and iteration[num2] != -1:
                label2 += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[num2],str(hparam[num2]),iteration[num2])
            elif hparam[num2] != -1:
                label2 += " ($\{} = ".format(args.REGPARAMNAME[num2]) + str(hparam[num2]) + "$)"
                # label2 += str(hparam[num2]*100)
            s += "{} & {:.6e} \\\\\\hline\n".format(label2, data[key1 + "_" + key2])
            # s += "{} & {:.2e} \\\\\\hline\n".format(int(round(float(label2))), data[key1 + "_" + key2])
        s+="\\end{tabular}"
        s+="\n\n###########################\n\n"
    print(s)


"""
ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14 -e ../data/A14/errapproximation.json -p ../../log/SimulationData/A14_chi2/Jets/full31_*  -l mu -o /tmp/temp -c ../data/A14Categories/A14Cat_Steve_MC.json 
"""
def plotperformanceBins_MC(args):
    import pandas as pd
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    IO.obsBins = obsBins
    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))
    colors = [
        (0.1216, 0.4667, 0.7059),
        (0.9961, 0.4980, 0.0588),
        (0.1804, 0.6235, 0.1686),
        (0.8392, 0.1490, 0.1569),
        (0.5804, 0.4039, 0.7412),
        (0.5490, 0.3373, 0.2980),
        'yellow'
    ]
    # Filtered IO
    type1 = os.path.basename(args.TESTDIR)

    allcombmetric = {}
    catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY, IO.hnames)
    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['-', '--', '-.', ':']
    for ano, arr in enumerate(catrng):
        import matplotlib.pyplot as plt
        import matplotlib as mpl
        mpl.rc('text', usetex=False)
        mpl.rc('font', family='serif', size=12)
        mpl.style.use("ggplot")
        if args.CATEGORY is not None:
            plt.figure(figsize=(4, 4))
        else:
            plt.figure(figsize=(7, 7))
        for fno, file in enumerate(args.FILES):
            allcombmetric[file] = []
            data = pd.read_csv(file, sep=' ',header=None)
            D = data.values
            data = {}
            for binno,binname in enumerate(D[:,0]):
                data[binname] = D[binno,1]
            for hno in arr:
                hname = IO.hnames[hno]
                sel = IO.obsBins(hname)
                if len(sel) == 0:
                    continue
                for num, n in enumerate(sel):
                    val = data[IO._binids[n]]
                    allcombmetric[file].append(val)
            Yaxis = np.arange(1 / len(allcombmetric[file]), 1, 1 / len(allcombmetric[file]))
            if len(Yaxis) !=len(allcombmetric[file]):
                Yaxis = np.append(Yaxis, 1)
            ylabel = 'Fraction of bins (# {})'.format(len(Yaxis))
            label = typearrlab[fno]
            if hparam[fno] != -1 and iteration[fno] != -1:
                label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
            elif hparam[fno] != -1:
                label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"

            # Xaxis, binnames = zip(*sorted(zip(allcombmetric[file], allcombBins[file])))
            Xaxis = np.sort(allcombmetric[file])
            allcombmetric[file] = Xaxis

            plt.step(Xaxis, Yaxis, where='mid', label=label, linewidth=1.2,
                     c=colors[fno % len(colors)],
                     linestyle="%s" % (marker[fno % len(marker)]))
        size = 20
        # plt.ylim(0)
        plt.xlim(10 ** -4, 10 ** 2.8)  # Normal

        plt.vlines(1, ymin=10**-3, ymax=max(Yaxis), colors='black', label="Variance boundary")
        plt.xticks(fontsize=size - 6)
        plt.yticks(fontsize=size - 6)
        plt.yscale('log')

        if args.CATEGORY is None:
            plt.legend(fontsize=size - 4)
        else:
            plt.ylim(10 ** -3)
        props = dict(boxstyle='square', facecolor='wheat', alpha=1)
        if args.CATEGORY is not None:
            plt.annotate('%s' % names_lab[ano], xy=(0, 1), xycoords='axes fraction', fontsize=16,
                         horizontalalignment='left', verticalalignment='top', bbox=props)
        if args.CATEGORY is None:
            plt.xlabel('Multiple of Variance', fontsize=size)
        if args.CATEGORY is None:
            plt.ylabel(ylabel, fontsize=size)
        plt.tight_layout()
        plt.xscale('log')
        dir = os.path.join(args.OUTDIR, 'bins', 'chi2_perf')
        os.makedirs(dir, exist_ok=True)
        type1 = os.path.basename(args.TESTDIR)
        plt.savefig(os.path.join(dir,
                                 "{}_{}_{}_comb_MCchi2_performance.pdf".format(args.OFILEPREFIX, type1,
                                                                               names_fn[ano])))
        plt.close("all")

"""
Jets

ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14 -e ../data/A14/errapproximation.json -p 
../../log/robO/RobustOptResults/A14_RA-robustOptimization/406/out_mu0.8_ms0_l1.0.json ../../log/SimulationData/A14_chi2/Jets/full31_robO_data_chi2.dat  
../../log/juliwenjing/A14_31/406/meanscore.json ../../log/SimulationData/A14_chi2/Jets/full31_meanscore_data_chi2.dat 
../../log/orig/A14/out_NNPDF.json ../../log/SimulationData/A14_chi2/Jets/full31_NNPDF_data_chi2.dat 
../../log/juliwenjing/A14_31/406/equalweights.json ../../log/SimulationData/A14_chi2/Jets/full31_equalweights_data_chi2.dat  
../../log/juliwenjing/A14_31/406/medscore.json ../../log/SimulationData/A14_chi2/Jets/full31_medscore_data_chi2.dat 
../../log/juliwenjing/A14_31/406/portfolio.json ../../log/SimulationData/A14_chi2/Jets/full31_portfolio_data_chi2.dat 
-l mu -o /tmp/ravsmc -c ../data/A14Categories/A14Cat_Steve_MC.json


################
All

ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14-RA -e ../data/A14-RA/errapproximation.json -p
../../log/robO/RobustOptResults/A14_RA-robustOptimization/406/out_mu0.8_ms0_l1.0.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_robO_data_chi2.dat
../../log/juliwenjing/A14_31/406/meanscore.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_meanscore_data_chi2.dat
../../log/orig/A14/out_NNPDF.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_NNPDF_data_chi2.dat  
../../log/juliwenjing/A14_31/406/medscore.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_medscore_data_chi2.dat 
../../log/juliwenjing/A14_31/406/portfolio.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_portfolio_data_chi2.dat
../../log/juliwenjing/A14_31/406/equalweights.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_equalweights_data_chi2.dat
-l mu -o /tmp/allravsmc -c ../data/A14Categories/A14Cat_all_qcd_ttbar_z.json

ftype="406"; python cdfPerformance.py --buildwitherror -t ../data/A14-RA -e ../data/A14-RA/errapproximation.json -p ../../log/robO/RobustOptResults/A14_RA-robustOptimization/406/out_mu0.8_ms0_l1.0.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_robO_data_chi2.dat ../../log/juliwenjing/A14_31/406/meanscore.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_meanscore_data_chi2.dat ../../log/orig/A14/out_NNPDF.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_NNPDF_data_chi2.dat  ../../log/juliwenjing/A14_31/406/medscore.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_medscore_data_chi2.dat ../../log/juliwenjing/A14_31/406/portfolio.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_portfolio_data_chi2.dat ../../log/juliwenjing/A14_31/406/equalweights.json ../../log/SimulationData/A14_chi2/all_qcd_ttbar_z/full31_equalweights_data_chi2.dat -l mu -o /tmp/allravsmc -c ../data/A14Categories/A14Cat_all_qcd_ttbar_z.json

"""
def plotperformanceBins_RAvsMC(args):
    import pandas as pd
    approxfile = args.TESTDIR + "/approximation.json"
    expdatafile = args.TESTDIR + "/experimental_data.json"
    weightfile = args.TESTDIR + "/weights"
    from apprentice.appset import TuningObjective2
    IO = TuningObjective2(weightfile, expdatafile, approxfile, args.ERROR,
                          filter_hypothesis=False, filter_envelope=False)
    ##### PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
    if "-RA" not in args.TESTDIR:
        raise Exception('Test directory must be of RA type. Exiting now')

    polytestdir = args.TESTDIR[:-3]
    polyexpdatafile = polytestdir + "/experimental_data.json"
    polyweightfile = polytestdir + "/weights"
    polyapproxfile = polytestdir + "/approximation.json"
    polyerrapproxfile = polytestdir + "/errapproximation.json"
    IOpoly = TuningObjective2(polyweightfile, polyexpdatafile, polyapproxfile, polyerrapproxfile,
                              filter_hypothesis=False, filter_envelope=False)

    # IOpoly = IO
    ##### PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP

    def obsBins(hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    def obsBinsPoly(hname):
        return [i for i, item in enumerate(IOpoly._binids) if item.startswith(hname)]

    IO.obsBins = obsBins
    IO._W2 = np.array([1.] * len(IO._W2))
    IO.hnames = sorted(list(set(IO._hnames)))

    IOpoly.obsBins = obsBinsPoly
    IOpoly._W2 = np.array([1.] * len(IOpoly._W2))
    IOpoly.hnames = sorted(list(set(IOpoly._hnames)))

    colors = [
        (0.1216, 0.4667, 0.7059),
        (0.9961, 0.4980, 0.0588),
        (0.1804, 0.6235, 0.1686),
        (0.1216, 0.4667, 0.7059),
        (0.9961, 0.4980, 0.0588),
        (0.1804, 0.6235, 0.1686),
        (0.8392, 0.1490, 0.1569),
        (0.5804, 0.4039, 0.7412),
        (0.5490, 0.3373, 0.2980),
        'yellow'
    ]

    allcombmetric = {}
    catrng, names_lab, names_fn = readcategoryfile(args.CATEGORY, IO.hnames)
    typearrlab, hparam, iteration = checkTypes(args)
    marker = ['--', '-','-.',':']

    for ano, arr in enumerate(catrng):
        allcombmetric[names_fn[ano]] = {}
        for fno, file in enumerate(args.FILES):
            allcombmetric[names_fn[ano]][file] = []
            if fno % 2 ==0: # RA
                data = readReport(file)
                for hno in arr:
                    x = data['x']
                    if 'NNPDF' in file:
                        hname = IOpoly.hnames[hno]
                        sel = IOpoly.obsBins(hname)
                        if len(sel) == 0:
                            continue
                        vals = IOpoly._AS.vals(x, sel=sel)
                        E = IOpoly._E[sel]
                        Y = IOpoly._Y[sel]
                        TE2 = (E ** 2)
                    else:
                        hname = IO.hnames[hno]
                        sel = IO.obsBins(hname)
                        if len(sel) == 0:
                            continue
                        vals = IO._AS.vals(x, sel=sel)
                        E = IO._E[sel]
                        Y = IO._Y[sel]
                        TE2 = (E ** 2)

                    for num, n in enumerate(sel):
                        val = (vals[num] - Y[num]) ** 2 / TE2[num]
                        allcombmetric[names_fn[ano]][file].append(val)
            else: # MC
                data = pd.read_csv(file, sep=' ', header=None)
                D = data.values
                data = {}

                for binno, binname in enumerate(D[:, 0]):
                    data[binname] = D[binno, 1]
                for hno in arr:
                    if 'NNPDF' in file:
                        hname = IOpoly.hnames[hno]
                        sel = IOpoly.obsBins(hname)
                        if len(sel) == 0:
                            continue
                        binids = IOpoly._binids
                    else:
                        hname = IO.hnames[hno]
                        sel = IO.obsBins(hname)
                        if len(sel) == 0:
                            continue
                        binids = IO._binids
                    for num, n in enumerate(sel):
                        if binids[n] not in data:
                            print("{} not found".format(hname))
                            break
                        val = data[binids[n]]
                        allcombmetric[names_fn[ano]][file].append(val)

        import matplotlib.pyplot as plt
        import matplotlib as mpl
        outindex = 0
        for fno, file in enumerate(args.FILES):
            if fno%6 == 0:
                outindex +=1
                mpl.rc('text', usetex=False)
                mpl.rc('font', family='serif', size=12)
                mpl.style.use("ggplot")
                if args.CATEGORY is not None:
                    #################################################################
                    plt.figure(figsize=(5, 5))
                    # plt.figure(figsize=(9, 9))
                    #################################################################
                else:
                    plt.figure(figsize=(7, 7))
            Yaxis = np.arange(1 / len(allcombmetric[names_fn[ano]][file]), 1, 1 / len(allcombmetric[names_fn[ano]][file]))
            if len(Yaxis) != len(allcombmetric[names_fn[ano]][file]):
                Yaxis = np.append(Yaxis, 1)
            ylabel = 'Fraction of bins (# {})'.format(len(Yaxis))
            label = typearrlab[fno]
            if hparam[fno] != -1 and iteration[fno] != -1:
                label += " ($\{} = {}$, iter = {})".format(args.REGPARAMNAME[fno], str(hparam[fno]), iteration[fno])
            elif hparam[fno] != -1:
                label += " ($\{} = ".format(args.REGPARAMNAME[fno]) + str(hparam[fno]) + "$)"
            if fno % 2 == 0:
                if 'NNPDF' in file:
                    label += " (PA)"
                else:
                    ##### PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
                    label += " (RA)"
                    # label += " (PA)"
                    ##### PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP
            else:
                label += " (MC)"

            Xaxis = np.sort(allcombmetric[names_fn[ano]][file])
            allcombmetric[names_fn[ano]][file] = Xaxis

            cindex = int(fno/2)
            mindex = fno%2
            plt.step(Xaxis, Yaxis, where='mid', label=label, linewidth=1.2,
                     c=colors[cindex],
                     linestyle="%s" % (marker[mindex]))
            if fno % 6 == 5 or fno == len(args.FILES)-1:
                size = 20
                # plt.ylim(0)
                plt.xlim(10 ** -4, 10 ** 2.8)  # Normal

                plt.vlines(1, ymin=10 ** -3, ymax=max(Yaxis), colors='black', label="Variance boundary")
                plt.xticks(fontsize=size - 6)
                plt.yticks(fontsize=size - 6)
                plt.yscale('log')

                if args.CATEGORY is None:
                    plt.legend(fontsize=size - 4)
                else:
                    #################################################################
                    # plt.legend(fontsize=size - 4)
                    #################################################################
                    plt.ylim(10 ** -3)
                props = dict(boxstyle='square', facecolor='wheat', alpha=1)
                if args.CATEGORY is not None:
                    plt.annotate('%s' % names_lab[ano], xy=(0, 1), xycoords='axes fraction', fontsize=16,
                                 horizontalalignment='left', verticalalignment='top', bbox=props)
                if args.CATEGORY is None:
                    plt.xlabel('Multiple of Variance', fontsize=size)
                if args.CATEGORY is None:
                    plt.ylabel(ylabel, fontsize=size)
                plt.tight_layout()
                plt.xscale('log')
                dir = os.path.join(args.OUTDIR, 'bins', 'chi2_perf')
                os.makedirs(dir, exist_ok=True)
                type1 = os.path.basename(args.TESTDIR)
                plt.savefig(os.path.join(dir,
                                         "{}_{}_{}_o{}_comb_MCchi2_performance.pdf".format(args.OFILEPREFIX, type1,
                                                                                       names_fn[ano],outindex)))
                plt.close("all")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='DoE Performance plot',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("-t", "--testdir", dest="TESTDIR", type=str, default=None,
                        help="Directory where testing approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored")
    parser.add_argument("-e", "--error", dest="ERROR", type=str, default=None,
                        help="Filename of the MC error stddev approximations is stored")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir for the plot file")
    parser.add_argument("--title", dest="TITLE", type=str, default=[], nargs='+',
                        help="Title for the plot (Spaces allowed)")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix (No spaces)")
    parser.add_argument("-p","--paramfiles", dest="FILES", type=str, default=[], nargs='+',
                        help="Parameter files in which output is stored and u for including ideal params (seperate files with space). "
                             "Use u for Ideal, n for Nadir and w1 for const. weights. "
                             "If u is used as an option to plot ideal, then please run this script with --onlyideal and -i option first")
    parser.add_argument("--buildwitherror", dest="BERROR", default=False, action="store_true",
                        help="Plot Errors insead of chi2")
    parser.add_argument("--useweights", dest="BWEIGHTS", default=False, action="store_true",
                        help="Use weights (with \"--buildwitherror\") in plot of residual histogram")
    parser.add_argument("-c", "--category", dest="CATEGORY", type=str, default=None,
                        help="Filename of the Category file. Use with \"--buildwitherror\". "
                             "If None, categories ignored.")
    parser.add_argument("--onlycheck", dest="CHECK", default=False, action="store_true",
                        help="Only check for existence of parameter files")
    parser.add_argument("--onlytable", dest="TABLE", default=False, action="store_true",
                        help="Only print data. Dont print plots")
    parser.add_argument("--onlyideal", dest="IDEAL", default=False, action="store_true",
                        help="Only find params for ideal runs. Please provide -i or --indir AND -e or --error")
    parser.add_argument("--domse", dest="MSE", default=False, action="store_true",
                        help="Use MSE as comparison")
    parser.add_argument("--printpdiff", dest="PRINTPDIFF", default=False, action="store_true",
                        help="Print P difference")
    parser.add_argument("--usecaliberrors", dest="CALIBERROR", default=False, action="store_true",
                        help="Use caliberation errors instead of observation errors")
    parser.add_argument("--usenoisecovariance", dest="NOISECOV", default=False, action="store_true",
                        help="Use Calibration Noise Covariance. Use with --usecaliberrors")
    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir where approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored "
                             "for building ideal parameters")
    parser.add_argument("-l", "--regparamname", dest="REGPARAMNAME", default=[], nargs='+',
                        help="Regularization param name per file to find in filenames. Ignore option if no regularization "
                             "param used")
    args = parser.parse_args()

    if args.NOISECOV and not args.CALIBERROR:
        print("Use noise covariance (--usenoisecovariance) with calibration error (--usecaliberrors)")
        sys.exit(1)

    if len(args.REGPARAMNAME) == 0:
        args.REGPARAMNAME = ['mu'] * len(args.FILES)
    elif len(args.REGPARAMNAME) == 1:
        args.REGPARAMNAME = [args.REGPARAMNAME[0]] * len(args.FILES)

    print(args)

    if args.IDEAL:
        if args.INDIR is None:
            print("Indur required for ideal param building. Please provide one using -i or --indir option")
            sys.exit(1)
        buildidealparams(args.INDIR)
    elif not args.IDEAL and args.CHECK: checkexists(args)
    else:
        if args.TESTDIR is None:
            print("Testdir required for performance plots. Please provide one using -t or --testdir option")
        checkexists(args)
        if args.BERROR:
            # plotperformanceBins(args,istopcorner=True,fileteredKey="outlier_filtered")
            # plotperformanceBins(args, istopcorner=True, fileteredKey="hypothesis_filtered")
            plotperformanceBins(args, istopcorner=False, fileteredKey="outlier_filtered")
            # plotperformanceBins(args, istopcorner=False, fileteredKey="hypothesis_filtered")

            # plotperformanceBins_MC(args)

            # plotperformanceBins_RAvsMC(args)

            # plotperformanceFilteredBins(args)
            """
            python cdfPerformance.py --buildwitherror -t ../data/A14 -e ../data/A14/errapproximation.json -i ../../YODA/param/outlier_filtered/A14_30
            """
            # organizePlots(args,fileteredKey='hypothesis_filtered')
            # organizePlots(args, fileteredKey='outlier_filtered')

            # createYoda(args)

            # plotperformanceObservables(args)
            # plotresidualhistogram(args)
        else:
            # plotperformance(args)
            plotperformancePareto(args)
