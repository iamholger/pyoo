#! /usr/bin/env python
import numpy as np
import sys
import datetime
from timeit import default_timer as timer
import argparse
import copy
import json
import apprentice
from mpi4py import MPI

class BaysesianAlgorithm():
    def __init__(self, *args,scargs,**kwargs):
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()
        x = datetime.datetime.now()
        if rank==0:
            print("##############################")
            print(x)
            print("##############################")

        apprfile = os.path.join(scargs.INDIR, "approximation.json")
        datafile = os.path.join(scargs.INDIR, "experimental_data.json")
        wtfile = os.path.join(scargs.INDIR, "weights")

        noiseexp = 2
        if scargs.CALIBERROR:
            datafile = os.path.join(scargs.INDIR, "experimental_data_cenoise.json")

        LAMBDA = scargs.LAMBDA if len(scargs.ADDITIONS) > 0 else 0.
        logdir = os.path.join(scargs.OUTDIR, "log")
        if not os.path.exists(logdir): os.makedirs(logdir, exist_ok=True)
        self.logfp = logdir + "/bayesianlog_dbase{}_dm{}_lambda{:.2e}".format(scargs.DOEBASE, scargs.DOEMETRIC, LAMBDA)
        outdir = os.path.join(scargs.OUTDIR, "out")
        if not os.path.exists(outdir): os.makedirs(outdir, exist_ok=True)
        self.outfp = outdir + "/out_dbase{}_dm{}_lambda{:.2e}".format(scargs.DOEBASE, scargs.DOEMETRIC, LAMBDA)
        start = timer()
        TO = apprentice.appset.TuningObjective2(
                    wtfile, datafile, apprfile,
                    f_error = None,
                    filter_envelope=scargs.FILTERENVELOPE, # not implemented in TO2 4/2/20
                    filter_hypothesis=scargs.FILTERHYPO, # not implemented in TO2 4/3/20
                    # metric=scargs.DOEMETRIC,
                    # metric_base=scargs.DOEBASE,
                    # restart_filter=100,
                    debug=scargs.DEBUG,
                    # OO_uses_sym_cache=True,
                    noise_exponent = noiseexp)
        fimtime = timer() - start
        self._debug = scargs.DEBUG
        self.metric=scargs.DOEMETRIC
        self.metric_base=scargs.DOEBASE
        self.nstart = scargs.NSTART
        self.nrestart = scargs.NRESTART

        self.TO = TO
        self.TOobjective = self.TO.objective
        self.TOgradient = self.TO.gradient
        self.TOhessian = self.TO.hessian

        self.nparam = self.TO._SCLR.dim
        self.hnames = sorted(list(set(self.TO._hnames)))
        self.TO._hnames = None
        self.nweight = len(self.hnames)
        self.nbin = len(self.TO._binids)
        self.LAMBDA = LAMBDA
        self.noiseexp = noiseexp
        self.fimtime = fimtime
        self.oedtimestart = timer()
        self.additions = scargs.ADDITIONS
        self.STOPCOND = scargs.STOPCOND
        self.NOISECOV = scargs.NOISECOV
        self.OOGRAD = 'analytic'
        self.datascale = 1. # Remove as this is uncompatible with Tunining Objective 2
        self.priorinflationfactor = 1.25
        self.noiseinflationfactor = 1.5
        self.MPIMAPOPT = scargs.MPIMAPOPT

        def obsBins(hname):
            return [i for i, item in enumerate(self.TO._binids) if item.startswith(hname)]
        self.OBSBINS = [obsBins(hname) for hname in self.hnames]
        self.PRIORPARAM = self.readReport(scargs.PRIORPARAM)['x']

        if not scargs.PERFORMANCE:self.TO._E *= self.noiseinflationfactor

        maxE = max(self.TO._E ** self.noiseexp)
        GAMMAPRIOR = [scargs.PRIORFACTOR * maxE] * self.nparam
        GAMMAPRIORINV = [1 / p for p in GAMMAPRIOR]
        self.GAMMAPRIORINVMAT = np.zeros((self.nparam, self.nparam))
        np.fill_diagonal(self.GAMMAPRIORINVMAT, GAMMAPRIORINV)

        # Scale data and error
        if not scargs.PERFORMANCE: self.TO._Y *= self.datascale
        if not scargs.PERFORMANCE:
            self.TO._E *= self.datascale
            self.TO._E2 = np.array([1. / e ** noiseexp for e in self.TO._E], dtype=np.float64)

        self.GAMMANOISEINVMAT_e = {}
        if scargs.NOISECOV:
            type = os.path.basename(scargs.INDIR)
            covfile = os.path.join("../../log/Covariance", type,"caliberrorcovariance.json")
            with open(covfile,'r') as f:
                covdata = json.load(f)
            for hno, hname in enumerate(self.hnames):
                self.GAMMANOISEINVMAT_e[hname] = np.linalg.inv(np.transpose(np.array(covdata[hname])
                                        * self.datascale * self.noiseinflationfactor))
        else:
            for hno, hname in enumerate(self.hnames):
                obsbins = self.OBSBINS[hno]
                err = np.array([e for e in self.TO._E[obsbins]])
                self.GAMMANOISEINVMAT_e[hname] = np.zeros((len(err), len(err)))
                np.fill_diagonal(self.GAMMANOISEINVMAT_e[hname], err)
                self.GAMMANOISEINVMAT_e[hname] = np.linalg.inv(np.transpose(self.GAMMANOISEINVMAT_e[hname]))
        if not scargs.PERFORMANCE:
            if self.NOISECOV:
                self.TO.objective = self.mapobjectiveMAT
                self.TO._E = None
                self.TO._W2 = None
                self.TO.setWeights = None
                self.TO.gradient = self.mapgradientMAT
                self.TO.hessian = None
            else:
                self.TO.setWeights = None
                self.TO.objective = self.mapobjective
                self.TO.gradient = self.mapgradient
                self.TO.hessian = self.maphessian

    def readReport(self,fname):
        """
        Read a pyoo report and extract data for plotting
        """

        import json
        with open(fname) as f:
            D = json.load(f)

        import numpy as np
        # First, find the chain with the best objective
        c_win = ""
        y_win = np.inf
        for chain, rep in D.items():
            _y = np.min(rep["Y_outer"])
            if _y is None:
                c_win = chain
                break
            elif _y < y_win:
                c_win = chain
                y_win = _y

        i_win = np.argmin(D[c_win]["Y_outer"])
        wmin = D[c_win]["X_outer"]
        binids = D[c_win]["binids"]
        pmin = D[c_win]["X_inner"][i_win]
        pmap = D[c_win]["PMAP"][i_win] if "PMAP" in D[c_win] and D[c_win]["PMAP"] is not None else 0.
        chi2win = D[c_win]["Y_inner"][i_win] if D[c_win]["Y_inner"] is not None else 0.
        pnames = D[c_win]["pnames_inner"]
        hnames = D[c_win]["pnames_outer"]
        ll = D[c_win]["lambda"] if "lambda" in D[c_win] and D[c_win]["lambda"] is not None else 0.
        sn = D[c_win]["solutionnorm"] if "solutionnorm" in D[c_win] and D[c_win]["solutionnorm"] is not None else 0.
        rn = D[c_win]["residualnorm"] if "residualnorm" in D[c_win] and D[c_win]["residualnorm"] is not None else 0.

        return {"x": pmin, "y": chi2win, 'pmap': pmap, "binids": binids, "pnames": pnames, "hnames": hnames,
                'wmin': wmin,'youter':D[c_win]["Y_outer"],'lambda':ll,'sn':sn, "rn":rn}

    def mapobjectiveMAT(self,x, sel=slice(None, None, None)):
        vals = self.TO._AS.vals(x,sel)

        P = x - self.PRIORPARAM
        Q = np.matmul(self.GAMMAPRIORINVMAT, P)
        obj = np.matmul(np.transpose(Q), P)
        diff = vals - self.TO._Y
        for hno, hname in enumerate(self.hnames):
            obsbins = self.OBSBINS[hno]
            D = np.matmul(self.GAMMANOISEINVMAT_e[hname],diff[obsbins])
            obj += np.matmul(np.transpose(D), diff[obsbins])

        return obj

    def mapgradientMAT(self, x):
        gradf = self.TO._AS.grads(x, set_cache=False)
        valsf = self.TO._AS.vals(x)

        P = x - self.PRIORPARAM
        diff = valsf - self.TO._Y

        G = np.matmul(self.GAMMAPRIORINVMAT,P)
        for hno, hname in enumerate(self.hnames):
            obsbins = self.OBSBINS[hno]
            v = np.matmul(self.GAMMANOISEINVMAT_e[hname],diff[obsbins])
            v = np.matmul(np.transpose(gradf[obsbins]),v)
            G += v
        return 2*G


    def mapobjective(self,x, sel=slice(None, None, None), unbiased=False):
        import autograd.numpy as np
        lsqobj = self.TOobjective(x,sel,unbiased)

        P = x - self.PRIORPARAM
        Q = np.matmul(self.GAMMAPRIORINVMAT, P)
        priorobj = np.matmul(np.transpose(Q), P)

        return lsqobj + priorobj

    def mapgradient(self,x, sel=slice(None, None, None)):
        lsqgrad = self.TOgradient(x,sel)

        P = x - self.PRIORPARAM
        Q = np.matmul(self.GAMMAPRIORINVMAT, P)

        return lsqgrad + (2 * Q)

    def maphessian(self,x, sel=slice(None, None, None)):
        lsqhess = self.TOhessian(x,sel)

        Q = self.GAMMAPRIORINVMAT

        return lsqhess + (2 * Q)

    def setWeights(self,w,wexp):
        for hno,hname in enumerate(self.hnames):
            obsbins = self.OBSBINS[hno]
            self.TO._W2[obsbins] = w[hno] ** wexp

    def mapminimize(self, useMPI=False):
        saddlePointCheck = False if self.NOISECOV else True

        if useMPI:
            return self.TO.minimizeMPI(
                nstart=self.nstart,
                nrestart=self.nrestart,
                method='tnc',
                saddlePointCheck=saddlePointCheck
            )
        else:
            return self.TO.minimize(
                nstart=self.nstart,
                nrestart=self.nrestart,
                method='tnc',
                saddlePointCheck=saddlePointCheck
            )

    def runOED(self,BOED, iter):
        import ipopt

        lb = np.zeros(self.nweight, dtype=np.float)
        # ub = np.ones(self.nweight, dtype=np.float)
        ub = [None] * self.nweight
        cl = []
        cu = []
        # ub *= 10.
        np.random.seed(23456744)
        x0 = np.random.uniform(low=0., high=1., size=(self.nweight ,))
        # x0 = np.append(x0, 1 - np.sum(x0))
        nlp = ipopt.problem(
            n=len(x0),
            m=len(cl),
            problem_obj=BOED,
            lb=lb,
            ub=ub,
            cl=cl,
            cu=cu
        )

        # IPOPT Options from https://github.com/syarra/pyOpt-pyIPOPT/blob/master/pyIPOPT/pyIPOPT.py
        nlp.addOption('mu_strategy', 'adaptive')
        nlp.addOption('max_iter', 1000)
        # nlp.addOption('recalc_y', 'no')
        # nlp.addOption('recalc_y_feas_tol', 1000.1)
        nlp.addOption('bound_relax_factor', 0.)
        # nlp.addOption('constr_viol_tol',10**-12)
        # nlp.setProblemScaling(
        #     obj_scaling=1,
        #     x_scaling=np.ones(self.nweight, dtype=np.float)
        # )
        # nlp.addOption('nlp_scaling_method', 'user-scaling')
        # nlp.addOption('max_iter', 20)
        # nlp.addOption('nlp_scaling_method', 'none')
        nlp.addOption('tol', 1e-4)
        # nlp.addOption('derivative_test', 'first-order')
        # nlp.addOption('derivative_test_print_all', 'yes')

        nlp.addOption('output_file', self.logfp + "_iter{}.log".format(iter))
        nlp.addOption('print_level',1)
        nlp.addOption('file_print_level', 5)
        # Acceptable convergence options
        # nlp.addOption('acceptable_tol', 1e-2)
        # nlp.addOption('acceptable_iter', 5)
        # nlp.addOption('acceptable_constr_viol_tol', 1e-1)
        # nlp.addOption('acceptable_dual_inf_tol', 1e+12)

        x, info = nlp.solve(x0)
        # print("Solution of the primal variables: x=%s\n" % repr(x))
        # print("Solution of the dual variables: lambda=%s\n" % repr(info['mult_g']))
        # print("Objective=%s\n" % repr(info['obj_val']))

        ret = {
            'xoutermin': x,
            'youtermin': info['obj_val'],
            'status': repr(info['status']),
            'status_msg': repr(info['status_msg']),
            'x0': x0,
            'log': {
                'fimtime': self.fimtime,
                'ootime': timer() - self.oedtimestart,
                'oostatus': [repr(info['status'])],
                'oostatus_msg': [repr(info['status_msg'])]
            }
        }
        return ret

    def run(self,startP = None,startW = None):
        comm = MPI.COMM_WORLD
        rank = comm.Get_rank()

        if not self.NOISECOV:
            self.setWeights([1.0] * self.nbin,wexp=1)
        errortype = 'calibration' if args.CALIBERROR else 'observed'
        self.PMAP = np.array([np.Infinity]*len(self.PRIORPARAM))
        i=0
        while(sum((self.PMAP-self.PRIORPARAM)**2)>self.STOPCOND):
            if i==0:
                if startP is None:
                    mapres = self.mapminimize(useMPI=self.MPIMAPOPT)
                    self.PMAP = mapres['x']
                    # self.PMAP = self.PRIORPARAM
                else:
                    self.PMAP = startP
                    self.setWeights(startW,1)
                    for hno, hname in enumerate(self.hnames):
                        BA.GAMMANOISEINVMAT_e[hname] = np.sqrt(startW[hno]) * BA.GAMMANOISEINVMAT_e[hname]

            BOED = BayesianOED(BA=self)
            if i == 0:
                self.firstBOED = BOED
            oedres = self.runOED(BOED,i)

            wnew = oedres['xoutermin']
            wnew = (1/np.max(wnew))*wnew

            solutionnorm = 0.
            solutionnorm_unscaled = 0.
            for a in self.additions:
                if a == "L1":
                    solutionnorm += sum(wnew)
                    solutionnorm_unscaled += sum(oedres['xoutermin'])
                if a == "L2":
                    solutionnorm += sum(wnew ** 2)
                    solutionnorm_unscaled += sum(oedres['xoutermin']**2)
            residualnorm = self.firstBOED.doe_metric(
                self.firstBOED.getposteriorcovmatrix(wnew))

            if not self.NOISECOV:
                self.setWeights(wnew,wexp=1)
            self.GAMMAPRIORINVMAT = np.linalg.inv(self.priorinflationfactor * BOED.getposteriorcovmatrix(wnew))
            self.PRIORPARAM = self.PMAP
            for hno, hname in enumerate(self.hnames):
                self.GAMMANOISEINVMAT_e[hname] = np.sqrt(wnew[hno])*self.GAMMANOISEINVMAT_e[hname]

            mapres = self.mapminimize(useMPI=self.MPIMAPOPT)
            self.PMAP = mapres['x']
            if rank == 0:
                print('\nStopping condition metric is {}\n'.format(sum((self.PMAP-self.PRIORPARAM)**2)))
                sys.stdout.flush()

            if rank==0:
                reports = {}
                RR = {}
                RR['PMAP'] = [self.PRIORPARAM.tolist()]
                RR["X_outer"] = [oedres['xoutermin'].tolist()]
                RR["Y_outer"] = oedres['youtermin']
                RR['log'] = oedres['log']
                RR["X_inner"] = [mapres['x'].tolist()]
                RR["Y_inner"] = [mapres["fun"]]  # Inner objective
                RR["pnames_outer"] = self.hnames
                RR["pnames_inner"] = self.TO.pnames
                RR["binids"] = self.TO._binids
                RR["DOE_METRIC"] = self.metric
                RR["DOE_BASE"] = self.metric_base
                RR['additions'] = self.additions
                RR['lambda'] = self.LAMBDA
                RR['errortype'] = errortype
                RR['solutionnorm'] = solutionnorm
                RR['solutionnorm_unscaled'] = solutionnorm_unscaled
                RR['residualnorm'] = residualnorm

                reports['chain-0'] = RR
                # print(reports)

                import json
                with open(self.outfp+"_iter{}.json".format(i), 'w') as f:
                    json.dump(reports, f, indent=4)
            i+=1


            # break

class BayesianOED():
    def __init__(self, *args, BA,**kwargs):
        self.BA = BA

        if self.BA.OOGRAD == 'ad':
            self.gradient = self.gradientAD
        elif self.BA.OOGRAD == 'analytic':
            self.gradient = self.gradientANA
        else:
            raise Exception('{grad} grad type not supported'.format(grad=self.BA.OOGRAD))
        self.EMG = self.BA.TO._AS.grads(self.BA.PMAP)*self.BA.datascale
        self.GAMMAPRIORINVMATCOPY = copy.deepcopy(self.BA.GAMMAPRIORINVMAT)
        self.F_e = {}
        self.B_e = {}
        for hno, h in enumerate(BA.hnames):
            obsbins = BA.OBSBINS[hno]
            self.F_e[h] = [self.EMG[bnum].tolist() for bnum in obsbins]
            # err = np.array([e for e in BA.TO._E[obsbins]])
            # self.C_e[h] = np.zeros((len(err),len(err)))
            # np.fill_diagonal(self.C_e[h], err)
            B = np.matmul(self.BA.GAMMANOISEINVMAT_e[h],self.F_e[h])
            self.B_e[h] = np.matmul(np.transpose(B),B)

    def doe_metric(self,M):
        import autograd.numpy as np
        doe_type = self.BA.metric
        if doe_type == "A":  # --> equivalent to Eavg as trace is sum_of_eigenvals
            return np.trace(M)
        elif doe_type == "D":
            return np.linalg.det(M)
        else:
            raise Exception("Specified metric type not known.")

    def objective(self,x):
        w = np.array(x)
        obj = self.doe_metric(self.getposteriorcovmatrix(w))
        for a in self.BA.additions:
            if a == 'L1':
                obj += self.BA.LAMBDA * sum(w)
            if a == 'L2':
                obj += self.BA.LAMBDA * sum(w**2)
        return obj

    def getposteriorcovmatrix(self,x):
        import autograd.numpy as np
        w = np.array(x)
        A = copy.deepcopy(self.GAMMAPRIORINVMATCOPY)
        for hno,hname in enumerate(self.BA.hnames):
            A += w[hno]*self.B_e[hname]
        return np.linalg.inv(A)

    def gradientANA(self, x):
        grad = np.zeros(self.BA.nweight, dtype=np.float)
        if self.BA.metric == 'A':
            w = np.array(x)
            A = self.getposteriorcovmatrix(x)
            for pnum,p in enumerate(range(self.BA.nparam)):
                e_i = np.eye(self.BA.nparam)[pnum]
                x = np.matmul(A,e_i)
                for hno, hname in enumerate(self.BA.hnames):
                    y = np.matmul(self.F_e[hname],x)
                    y = np.matmul(self.BA.GAMMANOISEINVMAT_e[hname],y)
                    grad[hno] += np.matmul(np.transpose(y),y)
            grad = -grad
        else:
            raise Exception('Analytical gradient not available for {M}. Try ad or fd grad option'.format(M=self.BA.OO._metric))
        for a in self.BA.additions:
            if a == 'L1':
                for i in range(len(grad)):
                    grad[i] += self.BA.LAMBDA
            if a == 'L2':
                for i in range(len(grad)):
                    grad[i] += self.BA.LAMBDA * 2 * w[i]
        return grad

    def gradientAD(self, x):
        import autograd.numpy as np
        w = np.array(x)
        from autograd import elementwise_grad
        egrad = elementwise_grad(self.objective)
        vegrad = egrad(w)
        return np.array(vegrad)

    def constraints(self, x):
        import autograd.numpy as np
        return np.array([])

    def jacobian(self, x):
        from autograd import jacobian
        jac = jacobian(self.constraints)
        jacv = jac(x)
        return np.array(jacv)

class SaneFormatter(argparse.RawTextHelpFormatter,
                    argparse.ArgumentDefaultsHelpFormatter):
    pass

def buildPerformanceTable(args):
    import json
    def obsobjectiveMAT(x):
        vals = np.array(BA.TO._AS.vals(x, sel))

        diff = vals - BA.TO._Y[sel]
        D = np.matmul(noisecov, diff)
        return np.matmul(np.transpose(D), D)

    if args.NOISECOV:
        type2 = os.path.basename(args.INDIR)
        covfile = os.path.join("../../log/Covariance", type2, "caliberrorcovariance.json")
        with open(covfile, 'r') as f:
            covdata = json.load(f)
        GAMMANOISEINVMAT_e={}
        for hname in covdata:
            GAMMANOISEINVMAT_e[hname] = np.linalg.inv(np.transpose(np.array(covdata[hname])))

    BA = BaysesianAlgorithm(scargs=args)
    if args.MSE:
        BA.TO._E2 = np.array([1.] * len(BA.TO._E2))
    chi2w1 = []
    chi2wnot1 = []
    Jp = []
    minw = []
    maxw = []
    avgw = []
    pmapdiff = [None,0]
    # etw = []
    area = []
    type = os.path.basename(args.INDIR)
    abcfile = "../../log/orig/{}/perfplot/abc.json".format(type)
    with open(abcfile,'r') as f:
        abc = json.load(f)
    trace = []
    prev = None
    for i in range(100):
        if i==0:
            file = args.PRIORPARAM
        else:
            iterindex = i-1
            file = BA.outfp+"_iter{}.json".format(iterindex)
            if not os.path.exists(file):
                break

        data = BA.readReport(file)
        if args.NOISECOV:
            e2w1 = []
            for hno,hname in BA.hnames:
                sel = BA.OBSBINS[hno]
                noisecov = GAMMANOISEINVMAT_e[hname]
                e2w1.append(obsobjectiveMAT(data["x"]))
        else:
            BA.setWeights([1.] * BA.nweight,1)
            e2w1 = np.array([BA.TO.objective(data['x'],BA.OBSBINS[hno])
                             for hno, hname in enumerate(BA.hnames)])

        for hno,hname in enumerate(BA.hnames):
            e2w1[hno] /= len(BA.OBSBINS[hno])
        chi2w1.append(sum(e2w1))

        w = data['wmin'][0]
        if len(w) < len(BA.hnames):
            w.append(1-sum(w))
        w = np.array(w)
        if args.NOISECOV:
            e2notw1= []
            for hno, hname in enumerate(BA.hnames):
                noisecov = np.sqrt(w[hno]) * GAMMANOISEINVMAT_e[hname]
                sel = BA.OBSBINS[hno]
                e2notw1.append(obsobjectiveMAT(data["x"]))
        else:
            ww = copy.deepcopy(w)
            w = 1/np.max(w)*w
            BA.setWeights(np.array(w),1)
            e2notw1 = np.array([BA.TO.objective(data['x'], BA.OBSBINS[hno])
                        for hno, hname in enumerate(BA.hnames)])

        for hno, hname in enumerate(BA.hnames):
            e2notw1[hno] /= len(BA.OBSBINS[hno])
        chi2wnot1.append(sum(e2notw1))

        # etw.append(data['sn'])
        # etw.append(sum(w)
        Jp.append(data['y'])
        minw.append(min(ww))
        maxw.append(max(ww))
        avgw.append(np.average(ww))
        if i > 1 :
            next = np.array(data["x"])
            pmapdiff.append(sum((next - prev) ** 2))
        prev = np.array(data["x"])

        if i==0:
            trace.append(-1)
        else:
            trace.append(data['rn'])

        area.append(abc['u_'+file])


    str = ""
    for i in range(len(chi2w1)):
        if i==0:
            str += "%d & $\priorparam$ & %.2e & %.2e & N/A & N/A & %.1e & %.1e & %.1e & N/A & %.2e" % \
                   (i,chi2w1[i], chi2wnot1[i], minw[i],maxw[i],avgw[i],area[i])
        else:
            str+="%d & $\\param_{%d}^{\\rm MAP}$ & %.2e & %.2e & %.2e & %.2e & %.1e & %.1e & %.1e & %.1e & %.5e"%\
                 (i,i,chi2w1[i],chi2wnot1[i],Jp[i],trace[i],minw[i],maxw[i],avgw[i],pmapdiff[i],area[i])
        str += "\\\\\\hline\n"


    print(str)

def mkTrainingData(args, WDIM,PDIM,nsamples, fout, base_seed):
    """
    """
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    # WDIM = len(bobj.hnames)
    # PDIM = bobj.nparam
    # if nsamples <= WDIM: nsamples=WDIM+1

    import sys
    if rank == 0:
        print("Training data generation")
        print("LHS design in {} dimensions".format(WDIM))
        print("Inner problem dimension: {}".format(PDIM))
        print("Will produce {} samples".format(nsamples))
        if size > 1:
            print("Distributed amongst {} ranks".format(size))
        sys.stdout.flush()

    import pyDOE2
    if rank == 0:
        S = pyDOE2.lhs(WDIM, samples=nsamples, criterion="maximin", random_state=base_seed)
        for i in range(len(S)):
            S[i] = (1 / np.max(S[i])) * S[i]
        print("Sample Generation completed")
        sys.stdout.flush()
    else:
        S = None
    S = comm.bcast(S, root=0)

    _Y = np.zeros((nsamples, PDIM))
    import apprentice
    allWork = apprentice.tools.chunkIt([i for i in range(nsamples)], size)
    rankWork = comm.scatter(allWork, root=0)

    import time
    import sys
    import datetime
    from collections import OrderedDict
    t0 = time.time()

    for ii in rankWork:
        BA = BaysesianAlgorithm(scargs=args)
        currPoint = S[ii].ravel()
        np.random.seed(base_seed + ii)

        # wd = OrderedDict()
        # for num, hn in enumerate(sorted(list(set(bobj.hnames)))):
        #     wd[hn] = currPoint[num]
        BA.setWeights(currPoint, 1)
        for hno, hname in enumerate(BA.hnames):
            BA.GAMMANOISEINVMAT_e[hname] = np.sqrt(currPoint[hno]) * BA.GAMMANOISEINVMAT_e[hname]
        res = BA.mapminimize(useMPI=False)
        _Y[ii] = res["x"]

        if rank == 0:
            print("[{}] {}/{}".format(rank, ii, len(rankWork)))
            now = time.time()
            tel = now - t0
            ttg = tel * (len(rankWork) - ii) / (ii + 1)
            eta = now + ttg
            eta = datetime.datetime.fromtimestamp(now + ttg)
            sys.stdout.write(
                "[{}] {}/{} (elapsed: {:.1f}s, to go: {:.1f}s, ETA: {})\r".format(rank, ii + 1, len(rankWork), tel,
                                                                                  ttg, eta.strftime(
                        '%Y-%m-%d %H:%M:%S')))
            sys.stdout.flush()
    a = comm.gather(_Y[rankWork])

    if rank == 0:
        allWork = apprentice.tools.chunkIt([i for i in range(nsamples)], size)
        for r in range(size): _Y[allWork[r]] = a[r]

        T = np.zeros((nsamples, WDIM + PDIM))
        T[:, range(WDIM)] = S
        T[:, range(WDIM, PDIM + WDIM)] = _Y
        np.savetxt(fout, T)

    comm.barrier()

def findBestparam(args,WDIM,PDIM, Y,W,fout):
    """
    """
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    # WDIM = len(bobj.hnames)
    # PDIM = bobj.nparam
    nsamples = len(Y)
    # if nsamples <= WDIM: nsamples=WDIM+1

    import sys
    if rank == 0:
        print("Training data generation")
        print("Weight design in {} dimensions".format(WDIM))
        print("Inner problem dimension: {}".format(PDIM))
        print("Will compare {} samples".format(nsamples))
        if size > 1:
            print("Distributed amongst {} ranks".format(size))
        sys.stdout.flush()

    _W = np.zeros((nsamples, WDIM))
    _F = np.zeros(nsamples)
    allWork = apprentice.tools.chunkIt([i for i in range(nsamples)], size)
    rankWork = comm.scatter(allWork, root=0)

    import time
    import sys
    import datetime
    t0 = time.time()

    for ii in rankWork:
        BA = BaysesianAlgorithm(scargs=args)
        currPointP = Y[ii].ravel()
        currPointW = W[ii].ravel()
        BA.PMAP = np.array(currPointP)
        BA.setWeights(currPointW, wexp=1)
        for hno, hname in enumerate(BA.hnames):
            BA.GAMMANOISEINVMAT_e[hname] = np.sqrt(W[ii][hno]) * BA.GAMMANOISEINVMAT_e[hname]
        BOED = BayesianOED(BA=BA)
        oedres = BA.runOED(BOED, 0)
        _W[ii] = currPointW
        _F[ii] = BOED.doe_metric(BOED.getposteriorcovmatrix(oedres['xoutermin']))

        if rank == 0:
            print("[{}] {}/{}".format(rank, ii, len(rankWork)))
            now = time.time()
            tel = now - t0
            ttg = tel * (len(rankWork) - ii) / (ii + 1)
            eta = now + ttg
            eta = datetime.datetime.fromtimestamp(now + ttg)
            sys.stdout.write(
                "[{}] {}/{} (elapsed: {:.1f}s, to go: {:.1f}s, ETA: {})\r".format(rank, ii + 1, len(rankWork), tel,
                                                                                  ttg, eta.strftime(
                        '%Y-%m-%d %H:%M:%S')))
            sys.stdout.flush()
    b = comm.gather(_W[rankWork])
    c = comm.gather(_F[rankWork])

    if rank == 0:
        allWork = apprentice.tools.chunkIt([i for i in range(nsamples)], size)
        for r in range(size): _W[allWork[r]] = b[r]
        for r in range(size): _F[allWork[r]] = c[r]

        T = np.zeros((nsamples, WDIM + PDIM + 1))
        T[:, range(PDIM)] = Y
        T[:, range(PDIM, PDIM + WDIM)] = _W
        T[:, range(PDIM + WDIM, PDIM + WDIM + 1)] = np.vstack(_F)
        np.savetxt(fout, T)

    comm.barrier()

if __name__ == "__main__":
    from timeit import default_timer as timer
    import os
    import pyoo.outer_objective as oobj

    parser = argparse.ArgumentParser(description='Baysian Optimal Experimental Design for Model Fitting',
                                     formatter_class=SaneFormatter)
    parser.add_argument("-i", "--indir", dest="INDIR", type=str, default=None,
                        help="In dir where approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored "
                             "for building ideal parameters")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir")

    parser.add_argument("-d", "--doemetric", dest="DOEMETRIC", type=str, default="A",
                        help="DOE Metric. Options are A, D logD")
    parser.add_argument("-b", "--doebase", dest="DOEBASE", type=str, default="WFIM",
                        help="DOE Metric Base. Options are FIM, WFIM, H")

    parser.add_argument("--filterhypo", dest="FILTERHYPO", default=False, action="store_true",
                        help="Do hypothesis filtering")
    parser.add_argument("--filterenvelope", dest="FILTERENVELOPE", default=False, action="store_true",
                        help="Do Envelope filtering")
    parser.add_argument("--usenoisecovariance", dest="NOISECOV", default=False, action="store_true",
                        help="Use Calibration Noise Covariance. Use with --usecaliberrors")
    parser.add_argument("--usecaliberrors", dest="CALIBERROR", default=False, action="store_true",
                        help="Use caliberation errors instead of observation errors")
    parser.add_argument('--stopcond', dest="STOPCOND", default=10**-6, type=float,
                        help="Stoping condition metric")
    parser.add_argument('-f', '--priorfactor', dest="PRIORFACTOR", default=8., type=float,
                        help="Factor by which the max error in \gamma_{noise} will be"
                             "multiplied by to get the \gamma_{prior} diagonal elements "
                             "(all diagonal elements of \gamma_{prior} will be factor * maxE")

    parser.add_argument("-a", "--additions", dest="ADDITIONS", type=str, default=[], nargs='+',
                        help="Additions to OED formulation. Options include:\n"
                             "1. L1: Add sum of weights to objective. as penalty\n"
                             "2. L2: Add L2 norm of the weight vector to objective as penalty")

    parser.add_argument("-p","--prior", dest="PRIORPARAM", type=str, default=None,
                        help="Prior parameter file path")

    parser.add_argument('--nstart', dest="NSTART", type=int, default=50,
                        help="Number of MAP objective function evaluations")
    parser.add_argument('--nrestart', dest="NRESTART", type=int, default=20,
                        help="Number of restarts of MAP optimization")

    parser.add_argument('-l','--lambda', dest="LAMBDA", type=float, default=0.,
                        help="Lambda parameter for regularization")

    parser.add_argument("--onlyshowperformance", dest="PERFORMANCE", default=False, action="store_true",
                        help="Only display performance (for a precious run)")
    parser.add_argument("--domse", dest="MSE", default=False, action="store_true",
                        help="Use MSE in performance")

    parser.add_argument("--dotraining", dest="BTRAIN", default=False, action="store_true",
                        help="Run Bayesian Training")

    parser.add_argument("--usempimapopt", dest="MPIMAPOPT", default=False, action="store_true",
                        help="Use MPI for MAP Objective Minimization")

    parser.add_argument("-v", "--debug", dest="DEBUG", action="store_true", default=False,
                        help="Turn on some debug messages")

    args = parser.parse_args()
    print(args)

    if args.NOISECOV and not args.CALIBERROR:
        print("Use noise covariance (--usenoisecovariance) with calibration error (--usecaliberrors)")
        sys.exit(1)

    for a in args.ADDITIONS:
        if a not in ["L1", "L2"]:
            print("additions unknown: {}".format(a))
            sys.exit(1)

    if args.PERFORMANCE:
        buildPerformanceTable(args)
        exit(0)

    bestP = None
    bestW = None
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    if args.BTRAIN:
        traindir = os.path.join(args.OUTDIR, "train")
        if not os.path.exists(traindir): os.makedirs(traindir, exist_ok=True)
        nsamples = 1000
        BA = BaysesianAlgorithm(scargs=args)
        fout = traindir + "/trainingdata_WP_n{}.txt".format(nsamples)
        if not os.path.exists(fout):
            mkTrainingData(args=args,PDIM=BA.nparam,WDIM=BA.nweight,
                        nsamples=nsamples,fout=fout,base_seed=67568954)

        D = np.loadtxt(fout, delimiter=' ')
        W = D[:, 0:BA.nweight]
        P = D[:, BA.nweight:]
        fout = traindir + "/trainingdata_PWF_dbase{}_dm{}_lambda{:.2e}_n{}.txt".format(BA.metric_base, BA.metric,
                                                                                   BA.LAMBDA,nsamples)
        if not os.path.exists(fout):
            findBestparam(args=args,WDIM=BA.nweight,PDIM=BA.nparam,
                          Y=P,W=W,fout=fout)

        D = np.loadtxt(fout, delimiter=' ')
        P = D[:, 0:BA.nparam]
        W = D[:, BA.nparam:BA.nparam + BA.nweight]
        F = D[:, BA.nparam + BA.nweight:]
        bestP = P[np.argmin(F)]
        bestW = W[np.argmin(F)]

    BA = BaysesianAlgorithm(scargs=args)
    BA.run(startP=bestP,startW=bestW)

    x = datetime.datetime.now()

    if rank==0:
        print("##############################")
        print(x)
        print("##############################")
