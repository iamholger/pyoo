from setuptools import setup, find_packages
setup(
  name = 'pyoo',
  version = '0.3.0',
  description = 'Outer optimisation',
  url = '',
  author = 'Holger Schulz',
  author_email = 'hschulz@fnal.gov',
  packages = find_packages(),
  include_package_data = True,
  install_requires = [
    'pyDOE',
    'numpy',
    'scipy',
    'matplotlib'
  ],
  scripts=['bin/pyoo-app', 'bin/pyoo-plot', 'bin/pyoo-analyse'],
  extras_require = {
  },
  entry_points = {
  },
  dependency_links = [
  ]
)
