#!/usr/bin/env python

import numpy as np
import os, sys


def run(indir,oos,m,im,outdir, sp = None, spc = None):
    cmd = "python `which pyoo-app` -w {IN}/weights -d {IN}/experimental_data.json {IN}/approximation.json -M {M} --IM {IM} --OOstrategy {OOS} -o {OUT}".format(IN=indir, M=m,IM=im,OOS=oos,OUT=outdir)
    if sp is not None:
        startpointstr  = ','.join(map(str, sp))
        cmd+=" --OOstartpoint {sp}".format(sp=startpointstr)
    if spc is not None:
        cmd+=" --OOstartpointcomment {spc}".format(spc=spc)
    print(cmd)
    os.system(cmd)

if __name__ == "__main__":

    if len(sys.argv)!=10:
        print("Usage: {} infolder fromoos fromdoem fromdoeb fromoutfolder tooos todoem todeob tooutfolder".format(sys.argv[0]))
        sys.exit(1)

    if not os.path.exists(sys.argv[1]):
        print("Input folder '{}' not found.".format(sys.argv[1]))
        sys.exit(1)

    infolder = sys.argv[1]

    fromoos = sys.argv[2]
    fromdoem = sys.argv[3]
    fromdoeb = sys.argv[4]
    fromoutfolder = sys.argv[5]

    tooos = sys.argv[6]
    todoem = sys.argv[7]
    todeob = sys.argv[8]
    tooutfolder = sys.argv[9]

    # run from part
    run(infolder,fromoos,fromdoem,fromdoeb,fromoutfolder)

    # getstart startpoints
    import json
    fname = "results/"+fromoutfolder+"/report.json"
    if fname:
        with open(fname, 'r') as fn:
            datastore = json.load(fn)
    sp = datastore['chain-0']['X_outer'][-1]

    # build spcomment
    spc = "spfrom_{OS}_{M}_{IM}".format(OS=fromoos,M=fromdoem,IM=fromdoeb)

    # run to part
    run(infolder,tooos,todoem,todeob,tooutfolder,sp,spc)

    sys.exit(0)
