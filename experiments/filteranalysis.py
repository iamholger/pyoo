import os,sys
import numpy as np
from apprentice.appset import TuningObjective2
import matplotlib.pyplot as plt

def readReport(fname):
    """
    Read a pyoo report and extract data for plotting
    """

    import json
    with open(fname) as f:
        D=json.load(f)

    import numpy as np
    # First, find the chain with the best objective
    c_win =""
    y_win = np.inf
    for chain, rep in D.items():
        _y = np.min(rep["Y_outer"])
        if _y is None:
            c_win = chain
            y_win = _y
        elif _y < y_win:
            c_win=chain
            y_win = _y

    i_win = np.argmin(D[c_win]["Y_outer"])
    wmin = D[c_win]["X_outer"]
    binids = D[c_win]["binids"]
    pmin = D[c_win]["X_inner"][i_win]
    pnames = D[c_win]["pnames_inner"]
    hnames = D[c_win]["pnames_outer"]

    return {"x":pmin, "binids":binids, "pnames":pnames, "hnames":hnames,'wmin':wmin}

def getA14categories():
    nobs = [200, 59, 9, 8, 20, 36, 4, 8, 20, 42]
    names_lab = [
        "Track jet properties",
        "Jet shapes",
        "Dijet decorr",
        "Multijets",
        "$p_T^z$",
        "Substructure",
        "tt gap",
        "Track-jet UE",
        "tt jet shapes",
        "Jet UE"
    ]
    names_fn = [
        "TrackJetProperties",
        "JetShapes",
        "DijetDecorr",
        "Multijets",
        "p_T-z",
        "Substructure",
        "ttGap",
        "Track-jetUE",
        "ttJetShapes",
        "JetUE"
    ]
    rng = []
    start=0
    for no, n in enumerate(nobs):
        rng.append(np.arange(start,start+n).tolist())
        start+=n

    return rng,names_lab,names_fn

def buildplots(args):
    """
    Data
    """
    allapproxfile = args.ALLINDIR + "/approximation.json"
    allexpdatafile = args.ALLINDIR + "/experimental_data.json"
    allweightfile = args.ALLINDIR + "/weights"
    allerrorapproxfile = None
    if os.path.exists(args.ALLINDIR + "/errapproximation.json"):
        allerrorapproxfile = args.ALLINDIR + "/errapproximation.json"

    allIO = TuningObjective2(allweightfile, allexpdatafile, allapproxfile, allerrorapproxfile,
                          filter_hypothesis=False, filter_envelope=False)

    obsfweightfile = args.ONSFWEIGHTF
    obsfIO = TuningObjective2(obsfweightfile, allexpdatafile, allapproxfile, allerrorapproxfile,
                          filter_hypothesis=False, filter_envelope=False)

    binfapproxfile = args.BINFINDIR + "/approximation.json"
    binfexpdatafile = args.BINFINDIR + "/experimental_data.json"
    binfweightfile = args.BINFINDIR + "/weights"
    binferrorapproxfile = None
    if os.path.exists(args.BINFINDIR + "/errapproximation.json"):
        binferrorapproxfile = args.BINFINDIR + "/errapproximation.json"


    binfIO = TuningObjective2(binfweightfile, binfexpdatafile, binfapproxfile, binferrorapproxfile,
                             filter_hypothesis=False, filter_envelope=False)

    def obsBins(IO, hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    hnames = sorted(list(set(allIO._hnames)))
    # A14categoryrng, names_lab, names_fn = getA14categories()
    A14categoryrng, names_lab, names_fn = [np.arange(len(hnames))], [""], ["all"]
    """
    Plot 1
    """
    os.makedirs(os.path.join(args.OUTDIR,"binf_kept_removed"), exist_ok=True)
    props = dict(boxstyle='square', facecolor='wheat', alpha=0.5)
    data = {}
    for ano,arr in enumerate(A14categoryrng):
        data[names_lab[ano]] = {}
        data[names_lab[ano]]['hnames'] = []
        nbins_kept = []
        nbins_removed = []
        for i in arr:
            hname = hnames[i]
            data[names_lab[ano]]['hnames'].append(hname)
            """
            Sherpa
            /DELPHI_1996_S3430090/d10-x01-y01 & 2 & 11.07 & 15.90 & 10.59\\\hline
            /DELPHI_1996_S3430090/d33-x01-y01 & 5 & 52.19 & 75.18 & 50.31\\\hline
            """
            nk = len(obsBins(binfIO,hname))
            nt = len(obsBins(allIO,hname))
            nr = nt - nk
            if nt>0:
                pass
            nbins_kept.append(nk)
            nbins_removed.append(nr)
        print("{} : {} (bin nr) {} (bin nk)".format(names_lab[ano],sum(nbins_removed),sum(nbins_kept)))
        X = np.arange(len(nbins_kept))
        fig, ax = plt.subplots(figsize=(30, 8))
        width = 0.55


        ax.bar(X, nbins_kept, width, label='Bins Kept',color='blue')
        ax.bar(X, nbins_removed, width, bottom=nbins_kept, color='red',label='Bins removed')
        data[names_lab[ano]]["Bins Kept"] = nbins_kept
        data[names_lab[ano]]["Bins removed"] = nbins_removed
        size = 40
        ax.set_xlabel('Observables',fontsize=size)
        ax.set_ylabel('Number of bins',fontsize=size)
        ax.set_title(names_lab[ano])
        ax.legend(loc='best',fontsize=size-4)
        xlab = []
        for i in range(len(X)):
            j=i+1
            if j==1:
                xlab.append("1")
                continue
            if j % 20 == 0:
                xlab.append(str(j))
            else:
                xlab.append("")
        plt.xticks(X,xlab,fontsize=size)

        #############################
        tenp1, temp2, temp3 = getA14categories()
        color = ['white','gray','white','gray','white','gray',
                 'white','gray','white','gray']
        for aaaaano,aaaa in enumerate(tenp1):
            ax.axvspan(aaaa[0], aaaa[len(aaaa)-1]+1, alpha=0.2,color=color[aaaaano])
        # secax = ax.secondary_xaxis('top')
        # secax.set_xlabel('A14 categories',fontsize = 24)
        # secax.set_xticklabels([str(i) for i in X])
        chars = ['A','B','C','D','E','F','G','H','I','J']
        for aaaaano, aaaa in enumerate(tenp1):
            if aaaaano == 6:
                x = aaaa[0]
            else:
                x = (aaaa[0]+aaaa[len(aaaa)-1])/2
            ax.text(x, 34, chars[aaaaano], fontsize=27, weight='bold', bbox=props)
        ################################
        plt.legend(fontsize = size-4)
        plt.yticks(fontsize=size-4)


        plt.tight_layout()
        # plt.setp(ax.get_xticklabels(), visible=False)
        # plt.show()
        plt.savefig(os.path.join(args.OUTDIR, "binf_kept_removed","{}.pdf".format(names_fn[ano])))
        plt.close('all')

        import json
        with open(os.path.join(args.OUTDIR, "filterdata.json"),'w') as f:
            json.dump(data,f,indent=4)


    """
    Plot 2
    """
    X = []
    nbins_kept = []
    nbins_removed = []

    obsfobs = sorted(list(set(obsfIO._hnames)))
    for hno, hname in enumerate(sorted(list(set(allIO._hnames)))):
        if hname not in obsfobs:
            nk = len(obsBins(binfIO, hname))
            nt = len(obsBins(allIO, hname))
            nr = nt - nk
            X.append(str(hno + 1))
            nbins_kept.append(nk)
            nbins_removed.append(nr)
    print("{} : {} (bin nr) {} (obs nk)".format("all", sum(nbins_removed), sum(nbins_kept)))

    fig, ax = plt.subplots(figsize=(20, 8))
    width = 0.55

    ax.bar(X, nbins_kept, width, label='Bins Kept',color='blue')
    ax.bar(X, nbins_removed, width, bottom=nbins_kept, color='red',label='Bins removed')

    ax.set_xlabel('Observables that were filtered by observable filter')
    ax.set_ylabel('Number of bins')
    ax.set_title('')
    ax.legend(loc='best')
    plt.tight_layout()
    plt.setp(ax.get_xticklabels(), visible=False)
    # plt.show()
    plt.savefig(os.path.join(args.OUTDIR, "obsf_kept_removed.pdf"))
    plt.close('all')

    import json
    with open(args.RESULT[0], 'r') as f:
        ds = json.load(f)
    X_outer = ds['chain-0']['X_outer'][0]
    pnames_outer = ds['chain-0']['pnames_outer']
    X_outer_dict = dict(zip(pnames_outer, X_outer))

    """
    Plot 3
    """
    os.makedirs(os.path.join(args.OUTDIR, "binf_weights"), exist_ok=True)

    width = 0.45

    for ano,arr in enumerate(A14categoryrng):
        nbins_removed = []
        weights = []
        for i in arr:
            hname = hnames[i]
            nk = len(obsBins(binfIO,hname))
            nt = len(obsBins(allIO,hname))
            nr = nt - nk
            nbins_removed.append(nr/nt)
            weights.append(X_outer_dict[hname])

        X = np.arange(len(weights))
        X1 =[x + width for x in X]
        fig, ax = plt.subplots(figsize=(20, 8))
        ax.bar(X, nbins_removed, width, label='Bins Removed',color='red')
        ax.bar(X1, weights, width, label='Weights',color='lime')
        ax.set_xlabel('Observables')
        ax.set_title(names_lab[ano])
        ax.legend(loc='best')
        plt.tight_layout()
        plt.setp(ax.get_xticklabels(), visible=False)
        # plt.show()
        plt.savefig(os.path.join(args.OUTDIR, "binf_weights","{}.pdf".format(names_fn[ano])))
        plt.close('all')

    """
    Plot 4
    """
    weights = []
    obsfobs = sorted(list(set(obsfIO._hnames)))
    for hno, hname in enumerate(sorted(list(set(allIO._hnames)))):
        if hname not in obsfobs:
            weights.append(X_outer_dict[hname])
    width = 0.45
    X = np.arange(len(weights))
    fig, ax = plt.subplots(figsize=(20, 8))
    ax.bar(X, weights, width, label='Weights',color='lime')
    ax.set_xlabel('Observables that were filtered by observable filter')
    ax.set_title('')
    ax.legend(loc='best')
    plt.tight_layout()
    plt.setp(ax.get_xticklabels(), visible=False)
    # plt.show()
    plt.savefig(os.path.join(args.OUTDIR, "obsf_weights.pdf"))
    plt.close('all')

def comparefiltercdfperf(args):
    """
    Data
    """
    allapproxfile = args.ALLINDIR + "/approximation.json"
    allexpdatafile = args.ALLINDIR + "/experimental_data.json"
    allweightfile = args.ALLINDIR + "/weights"
    allerrorapproxfile = None
    if os.path.exists(args.ALLINDIR + "/errapproximation.json"):
        allerrorapproxfile = args.ALLINDIR + "/errapproximation.json"

    allIO = TuningObjective2(allweightfile, allexpdatafile, allapproxfile, allerrorapproxfile,
                             filter_hypothesis=False, filter_envelope=False)

    obsfweightfile = args.ONSFWEIGHTF
    obsfIO = TuningObjective2(obsfweightfile, allexpdatafile, allapproxfile, allerrorapproxfile,
                              filter_hypothesis=False, filter_envelope=False)

    binfapproxfile = args.BINFINDIR + "/approximation.json"
    binfexpdatafile = args.BINFINDIR + "/experimental_data.json"
    binfweightfile = args.BINFINDIR + "/weights"
    binferrorapproxfile = None
    if os.path.exists(args.BINFINDIR + "/errapproximation.json"):
        binferrorapproxfile = args.BINFINDIR + "/errapproximation.json"

    binfIO = TuningObjective2(binfweightfile, binfexpdatafile, binfapproxfile, binferrorapproxfile,
                              filter_hypothesis=False, filter_envelope=False)

    def obsBins(IO, hname):
        return [i for i, item in enumerate(IO._binids) if item.startswith(hname)]

    hnames = sorted(list(set(allIO._hnames)))
    # A14categoryrng, names_lab, names_fn = getA14categories()
    # A14categoryrng, names_lab, names_fn = [np.arange(len(hnames))], [""], ["all"]
    marker = ['-', '--', '-.', ':']
    # for ano, arr in enumerate(A14categoryrng):
    import matplotlib.pyplot as plt
    plt.figure(figsize=(10, 12))
    YaxisAll = None
    XaxisAll = None
    title = args.OFILEPREFIX
    for fno, file in enumerate(args.RESULT):
        data = readReport(file)
        if "hypothesis_filtered" in file:
            IO = allIO
            legendkey = "Bin Filtered"
        elif "outlier_filtered" in file:
            IO = allIO
            legendkey = "Outlier Filtered"
        else:
            IO = allIO
            legendkey = "All"
        IO._W2 = np.array([1.] * len(IO._W2))
        IO.hnames = sorted(list(set(IO._hnames)))
        allcombmetric = []
        for hno,hname in enumerate(IO.hnames):
            # hname = IO.hnames[hno]
            sel = obsBins(IO, hname)
            if len(sel) == 0:
                continue
            x = data['x']
            if len(data['wmin'][0]) == len(IO.hnames) - 1:
                data['wmin'][0].append(1.)
            vals = IO._AS.vals(x, sel=sel)
            if IO._EAS is not None:
                E2 = IO._EAS.vals(x, sel=sel)
            else:
                E2 = np.zeros_like(vals)
            E = IO._E[sel]
            Y = IO._Y[sel]
            TE2 = (E ** 2 + E2 ** 2)
            for i in range(len(sel)):
                val = (vals[i] - Y[i]) ** 2 / TE2[i]
                allcombmetric.append(val)
        thisXaxis = np.sort(allcombmetric)
        if legendkey == 'All':
            # Yaxis = np.arange(1 / len(allcombmetric), 1, 1 / len(allcombmetric))
            # Yaxis = np.append(Yaxis, 1)
            YaxisAll = np.arange(len(allcombmetric))
            XaxisAll = thisXaxis
        if YaxisAll is None:
            raise Exception("Y axis unavailable. Set \"all\" result first")
        thisYaxis = YaxisAll[:len(allcombmetric)]

        if legendkey != 'All':
            e = sum([(((thisXaxis[i] - XaxisAll[i]) + (thisXaxis[i + 1] - XaxisAll[i + 1])) / 2)
                                      * (YaxisAll[i + 1] - YaxisAll[i])
                                      for i in range(len(thisXaxis) - 1)])
            title += "\n Area between %s and All: %.2E"%(legendkey,e)
        plt.step(thisXaxis, thisYaxis, where='mid', label=legendkey, linewidth=1,
                 linestyle="%s" % (marker[fno % len(marker)]))
    plt.vlines(1, ymin=min(thisYaxis), ymax=max(thisYaxis), colors='black', label="Variance boundary")
    size = 20
    # plt.ylim(0)
    # plt.xlim(10 ** -13)
    plt.xticks(fontsize=size - 6)
    plt.yticks(fontsize=size - 6)
    plt.yscale('log')
    plt.legend(fontsize=size - 4)
    # plt.title(names_lab[ano], fontsize=size - 4)
    plt.title(title, fontsize=size - 4)
    plt.xlabel('Multiple of Variance', fontsize=size)
    ylabel = 'Number of bins'
    plt.ylabel(ylabel, fontsize=size)
    ###########
    # Top right corner
    # plt.xlim(10 ** -3)
    # plt.ylim(10 ** 2.5)
    ###########
    plt.tight_layout()
    plt.xscale('log')
    os.makedirs(args.OUTDIR, exist_ok=True)
    type1 = os.path.basename(args.ALLINDIR)
    # plt.savefig(os.path.join(args.OUTDIR,
    #                          "{}_{}_comparefiltercdfperf.pdf".format(type1, names_fn[ano])))
    plt.savefig(os.path.join(args.OUTDIR,
                             "{}_{}_comparefiltercdfperf.pdf".format(type1,args.OFILEPREFIX)))
    plt.close("all")

    # Area  between curve
    # for num1, key1 in enumerate(XaxisAll):
    #     for num2, key2 in enumerate(XaxisAll):
    #         if num1 == num2: continue
    #         e = sum([(((XaxisAll[key1][i] - XaxisAll[key2][i]) + (XaxisAll[key1][i + 1] - XaxisAll[key2][i + 1])) / 2)
    #                  * (YaxisAll[key1][i + 1] - YaxisAll[key1][i])
    #                  for i in range(len(XaxisAll[key1]) - 1)])
    #         print(key1,key2,e)





if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Filtering analysis',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument("--all", dest="ALLINDIR", type=str, default=None,
                        help="Directory where all bins' approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored")
    parser.add_argument("--binf", dest="BINFINDIR", type=str, default=None,
                        help="Directory where bin filtered approximations (as approximation.json), data "
                             "(as experimental_data.json) and weight file (as weights) are stored")
    parser.add_argument("--obsfweight", dest="ONSFWEIGHTF", type=str, default=None,
                        help="Observable filtered weights file path")
    parser.add_argument("-r", "--result", dest="RESULT", type=str, default=[], nargs='+',
                        help="Result JSON files path from a DoE approach")
    parser.add_argument("-o", "--outtdir", dest="OUTDIR", type=str, default=None,
                        help="Output Dir for the plot file")
    parser.add_argument("-x", "--ofileprefix", dest="OFILEPREFIX", type=str, default="",
                        help="Output file prefix (No spaces)")

    args = parser.parse_args()

    """
    python filteranalysis.py --all ../data/A14 --binf ../data/A14_hypothesis_filtered --obsfweight ../data/A14_outlier_filtered/weights -r ../../log/robO/A14/new/out/out_mu0.755_ms0_l1.0.json -o /tmp/binf_kept_removed
    python filteranalysis.py --all ../data/Sherpa --binf ../data/Sherpa_hypothesis_filtered --obsfweight ../data/Sherpa_outlier_filtered/weights -r ../../log/robO_old/Sherpa/new/out/out_mu0.663_ms0_l1.0.json -o /tmp/binf_kept_removed
    
    python filteranalysis.py --all ../data/A14-RA --binf ../data/A14_hypothesis_filtered-RA --obsfweight ../data/A14_outlier_filtered-RA/weights -r ../../log/robO/RobustOptResults/A14_RA-robustOptimization/406/out_mu0.8_ms0_l1.0.json -o /tmp/binf_kept_removed
    python filteranalysis.py --all ../data/Sherpa-RA --binf ../data/Sherpa_hypothesis_filtered-RA --obsfweight ../data/Sherpa_outlier_filtered-RA/weights -r ../../log/robO/RobustOptResults/Sherpa_RA-robustOptimization/All/out_mu0.824_ms0_l1.0.json -o /tmp/binf_kept_removed
    """
    buildplots(args)


    """
    python filteranalysis.py --all ../data/A14 --binf ../data/A14_hypothesis_filtered --obsfweight ../data/A14_outlier_filtered/weights -r ../../log/robO/A14/new/out/out_mu0.751_ms0_l1.0.json ../../log/robO/A14_hypothesis_filtered/new/out/out_mu0.599_ms0_l1.0.json ../../log/robO/A14_outlier_filtered/new/out/out_mu0.758_ms0_l1.0.json -o /tmp/filtercdfperf -x RobustOptimization
    python filteranalysis.py --all ../data/A14 --binf ../data/A14_hypothesis_filtered --obsfweight ../data/A14_outlier_filtered/weights -r ../../log/orig/A14/out_NNPDF.json ../../log/orig/A14/hypothesis_filtered/out_NNPDF.json ../../log/orig/A14/outlier_filtered/out_NNPDF.json -o /tmp/filtercdfperf -x NNPDF
    """
    # comparefiltercdfperf(args)
