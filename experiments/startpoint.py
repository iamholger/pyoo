#!/usr/bin/env python


import os
import sys
# M="D"; OOs="gradoo"; IM="WFIM"; pip install .; dir="test_data_min"; python `which pyoo-app` -w $dir/weights -d $dir/experimental_data.json $dir/approximation.json 0 -M $M --IM $IM --OOstrategy $OOs -o "OO"$OOs"_"$M"_"$IM
datadir = "../test_data_min"
im = "WFIM"
# Marr = ["A","D","D","A","D","A"]
# OOstrategyarr = ["gradoo","gradoo","gradoo","gradoo","gradoo","gradoo"]
# FromMarr = ["A","D","A","D","A","D"]
# FromOOstrategyarr = ["rbf","rbf","rbf","rbf","gradoo","gradoo"]
# # SPcommentarr = ["sp_from_"+a+"_"+b for a,b in zip(FromOOstrategyarr,FromMarr)]
# for fm,foos,m,oos,spc in zip(FromMarr,FromOOstrategyarr,Marr, OOstrategyarr,SPcommentarr):
#     break
oosarr = ['rbf','gradoo']
marr = ['A','D']

def runsp():
    for foos in oosarr:
        for fm in marr:
            for toos in oosarr:
                # if toos == rbf: continue
                for tm in marr:
                    foutdir = "spfrom_{oos}_{m}_{im}".format(oos=foos,m=fm,im=im)
                    toutdir = "spto_{oos}_{m}_{im}".format(oos=toos,m=tm,im=im)
                    log = "../../log/doe_sp_{foos}_{fm}_{im}_{toos}_{tm}_{im}.log".format(foos=foos,fm=fm,im=im,toos=toos,tm=tm)
                    cmd = ('nohup python runstartpoint.py '
                        '{indir} {foos} {fm} {im} {foutdir} '
                        '{toos} {tm} {im} {toutdir} '
                        '>{LOG} 2>&1 &').format(indir=datadir,foos=foos,fm=fm,im=im,toos=toos,tm=tm,foutdir=foutdir,toutdir=toutdir,LOG=log)
                    # from IPython import embed
                    # embed()
                    os.system(cmd)


def analyzesp():
    s = ""
    for foos in oosarr:
        for fm in marr:
            for toos in oosarr:
                # if toos == rbf: continue
                for tm in marr:
                    if(foos == toos and fm == tm):
                        continue
                    foutdir = "spfrom_{oos}_{m}_{im}".format(oos=foos,m=fm,im=im)
                    toutdir = "spto_{oos}_{m}_{im}".format(oos=toos,m=tm,im=im)
                    ofname = "report"
                    ffn = os.path.join("results",foutdir,ofname+".json")

                    spc = "spfrom_{OS}_{M}_{IM}".format(OS=foos,M=fm,IM=im)
                    ofname = ofname+"_"+spc
                    tfn = os.path.join("results",toutdir,ofname+".json")

                    for fnum,fn in enumerate([ffn,tfn]):
                        import json
                        with open(fn,'r') as f:
                            results = json.load(f)
                        if fnum ==0:
                            if foos == 'rbf':
                                oos = 'r'
                            else:
                                oos = 'g'
                            m=fm
                        else:
                            if toos == 'rbf':
                                oos = 'r'
                            else:
                                oos = 'g'
                            m=tm
                            s+="&"
                        s+= "{OS} {M}".format(OS=oos,M=m)
                        s+= "&%.2f %.2f"%(results['chain-0']['X_outer'][-1][0],results['chain-0']['X_outer'][-1][1])
                        s+= "&%.2f %.2f %.2f"%(results['chain-0']['X_inner'][-1][0],results['chain-0']['X_inner'][-1][1],results['chain-0']['X_inner'][-1][2])
                        s+= "&%.2E"%(results['chain-0']['Y_outer'][-1])
                        s+= "&%.2E"%(results['chain-0']['Y_inner'][-1])
                        s+= "&%.2E"%(results['chain-0']['log']['ootime'])
                        if 'oostatus' in results['chain-0']['log']:
                            s+= "&%s"%(results['chain-0']['log']['oostatus'][0])
                        else:
                            s+= "&--"
                    s+= "\n\\\\\\hline\n"
    print(s)

#
if __name__ == "__main__":
    type = int(sys.argv[1])
    if type ==  0: # run
        runsp()
    elif type == 1: # analyze
        analyzesp()

    sys.exit(0)
